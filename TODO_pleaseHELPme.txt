Prop eyescanners are necessary, to replace the func walls and buttons that appear around the mod. Until then, some of the scanners will have the HL2 prop under them, but most will remain as brushes. I do not have enough experience to know how this can be done.

Prop buttons are necessary, since for some reason button and animated textures have seized to work post-VTF replacement. Either this, or a more experienced modder can help alleviate the issue at least for the animated textures (see Office Complex elevator).

Prop ragdoll push and pull is necessary; find out what entity gets Used when the player tries to use a ragdoll (it's not CPropRagdoll, I know that much).

Monster Houndeye chasing after enemies after acquire line of sight is necessary, yet it is not what occurs. My attempts at fixing it have all failed, and I need help.

Monster Barney, Monster Scientist and Monster Scientist Special following and unfollowing the player after a save is loaded is necessary, yet it is not what occurs. My attempts at fixing it have all failed, and I need help.

Monster Osprey creating breakmodels on crash touch is necessary, yet it is not what occurs. The code is the exact same as the Apache's, yet only the latter works. My attempts at fixing it have all failed, and I need help.

Monster Barnacle lifting NPCs by turning them into ragdolls but reverting them to NPCs when it's killed, and having their tongues blocked by breakables and props, are necessary features, yet it is not what occurs. My attempts at fixing it have all failed, and I need help.

Multiplayer Players' dead ragdolls acting like any other server ragdoll is necessary, yet it is not what occurs. My attempts at fixing it have all failed, and I need help.

Bots in multiplayer following the navmesh correctly and firing at players on gaining sight of them is necessary, yet it is not what occurs. My attempts at fixing it have all failed, and I need help.