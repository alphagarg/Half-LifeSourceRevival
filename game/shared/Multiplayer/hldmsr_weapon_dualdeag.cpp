//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		DualDeag - hand gun
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "npcevent.h"
#include "hl1mp_basecombatweapon_shared.h"
//#include "basecombatcharacter.h"
//#include "ai_basenpc.h"
#ifdef CLIENT_DLL
#include "c_baseplayer.h"
#else
#include "player.h"
#endif
#include "gamerules.h"
#include "in_buttons.h"
#ifdef CLIENT_DLL
#else
#include "soundent.h"
#include "game.h"
#endif
#include "vstdlib/random.h"
#include "engine/IEngineSound.h"

#ifdef CLIENT_DLL
#define CWeaponDualDeag C_WeaponDualDeag
#endif

class CWeaponDualDeag : public CBaseHL1MPCombatWeapon
{
	DECLARE_CLASS( CWeaponDualDeag, CBaseHL1MPCombatWeapon );

	DECLARE_NETWORKCLASS(); 
	DECLARE_PREDICTABLE();

public:

	CWeaponDualDeag(void);

	void	Precache( void );
	int m_iShotsFired /*= 0*/; // need to comment this out to compile on linux
	void	PrimaryAttack( void );
	bool	Reload( void );
	void	WeaponIdle( void );
	void	DryFire( void );

private:
	void	DeagFire( float flSpread , float flCycleTime, bool fUseAutoAim );
};

IMPLEMENT_NETWORKCLASS_ALIASED( WeaponDualDeag, DT_WeaponDualDeag );

BEGIN_NETWORK_TABLE( CWeaponDualDeag, DT_WeaponDualDeag )
END_NETWORK_TABLE()

LINK_ENTITY_TO_CLASS( weapon_dualdeag, CWeaponDualDeag );

BEGIN_PREDICTION_DATA( CWeaponDualDeag )
END_PREDICTION_DATA()

PRECACHE_WEAPON_REGISTER( weapon_dualdeag );


CWeaponDualDeag::CWeaponDualDeag( void )
{
	m_bReloadsSingly	= false;
	m_bFiresUnderwater	= true;
	m_iShotsFired		= 0;
}


void CWeaponDualDeag::Precache( void )
{

	BaseClass::Precache();
}


void CWeaponDualDeag::DryFire( void )
{
	WeaponSound( EMPTY );
	SendWeaponAnim( ACT_VM_DRYFIRE );
		
	m_flNextPrimaryAttack = gpGlobals->curtime + 0.2;
}


void CWeaponDualDeag::PrimaryAttack( void )
{
	DeagFire( 0.05, 0.25, FALSE );
}


void CWeaponDualDeag::DeagFire( float flSpread , float flCycleTime, bool fUseAutoAim )
{
	// Only the player fires this way so we can cast
	CBasePlayer *pPlayer = ToBasePlayer( GetOwner() );

	if ( !pPlayer )
	{
		return;
	}

	if ( m_iClip1 <= 0 )
	{
		if ( !m_bFireOnEmpty )
		{
			Reload();
		}
		else
		{
			DryFire();
		}

		return;
	}

	// Semi-auto behaviour
	
	m_iShotsFired++;
	
	if (m_iShotsFired > 1)
		return;
	
	WeaponSound( SINGLE );

	pPlayer->DoMuzzleFlash();

	m_iClip1--;

	if ( m_iClip1 == 0 )
		SendWeaponAnim( ACT_GLOCK_SHOOTEMPTY );
	else
		SendWeaponAnim( ACT_VM_PRIMARYATTACK );

	pPlayer->SetAnimation( PLAYER_ATTACK1 );

	m_flNextPrimaryAttack	= gpGlobals->curtime + flCycleTime;

	Vector vecSrc = pPlayer->Weapon_ShootPosition();
	Vector vecAiming;
	
	if ( fUseAutoAim )
	{
		vecAiming = pPlayer->GetAutoaimVector( AUTOAIM_10DEGREES );	
	}
	else
	{
		vecAiming = pPlayer->GetAutoaimVector( 0 );	
	}

//	pPlayer->FireBullets( 1, vecSrc, vecAiming, Vector( flSpread, flSpread, flSpread ), MAX_TRACE_LENGTH, m_iPrimaryAmmoType, 0 );
	FireBulletsInfo_t info( 1, vecSrc, vecAiming, Vector( flSpread, flSpread, flSpread ), MAX_TRACE_LENGTH, m_iPrimaryAmmoType );
	info.m_pAttacker = pPlayer;
	pPlayer->FireBullets( info );

	EjectShell( pPlayer, 0 );

	float recoil = random->RandomFloat(-1, 1);
	pPlayer->ViewPunch(QAngle(-1.5f, recoil, recoil));
#if !defined(CLIENT_DLL)
	pPlayer->SetMuzzleFlashTime( gpGlobals->curtime + 0.5 );

	CSoundEnt::InsertSound( SOUND_COMBAT, GetAbsOrigin(), 400, 0.2 );
#endif

	if ( !m_iClip1 && pPlayer->GetAmmoCount( m_iPrimaryAmmoType ) <= 0 )
	{
		// HEV suit - indicate out of ammo condition
		pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);
	}

	SetWeaponIdleTime( gpGlobals->curtime + random->RandomFloat( 10, 15 ) );
}


bool CWeaponDualDeag::Reload( void )
{
	bool iResult;

	iResult = DefaultReload( GetMaxClip1(), GetMaxClip2(), ACT_VM_RELOAD );

	return iResult;
}



void CWeaponDualDeag::WeaponIdle( void )
{
	// Semi-auto behaviour
	
	m_iShotsFired = 0;
	
	CBasePlayer *pPlayer = ToBasePlayer( GetOwner() );
	if ( pPlayer )
	{
		pPlayer->GetAutoaimVector( AUTOAIM_10DEGREES );
	}

	// only idle if the slid isn't back
	if ( m_iClip1 != 0 )
	{
		BaseClass::WeaponIdle();
	}
}
