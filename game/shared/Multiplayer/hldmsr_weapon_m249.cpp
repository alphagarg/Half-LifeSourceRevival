//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "hl1mp_basecombatweapon_shared.h"
#include "npcevent.h"
#ifdef CLIENT_DLL
#include "hl1/hl1_c_player.h"
#else
#include "hl1_player.h"
#endif
#include "hldmsr_weapon_m249.h"
#include "gamerules.h"
#ifdef CLIENT_DLL
#else
#include "soundent.h"
#include "game.h"
#endif
#include "in_buttons.h"
#include "engine/IEngineSound.h"

//=========================================================
//=========================================================

IMPLEMENT_NETWORKCLASS_ALIASED( WeaponM249, DT_WeaponM249 );

BEGIN_NETWORK_TABLE( CWeaponM249, DT_WeaponM249 )
END_NETWORK_TABLE()

BEGIN_PREDICTION_DATA( CWeaponM249 )
END_PREDICTION_DATA()

LINK_ENTITY_TO_CLASS( weapon_m249, CWeaponM249 );

PRECACHE_WEAPON_REGISTER(weapon_M249);

//IMPLEMENT_SERVERCLASS_ST( CWeaponM249, DT_WeaponM249 )
//END_SEND_TABLE()

BEGIN_DATADESC( CWeaponM249 )
END_DATADESC()

CWeaponM249::CWeaponM249( )
{
	m_bReloadsSingly	= false;
	m_bFiresUnderwater	= false;
	m_iShotsFired		= 0;
}

void CWeaponM249::Precache( void )
{
	BaseClass::Precache();
}





void CWeaponM249::PrimaryAttack( void )
{
	// Only the player fires this way so we can cast
	CBasePlayer *pPlayer = ToBasePlayer( GetOwner() );

	if ( !pPlayer )	
	{
		return;
	}

	if ( m_iClip1 <= 0 )
	{
		DryFire();
		return;
	}
	
	// Semi-auto behaviour
	
	if (m_iShotsFired != 0)
		return;
	
	m_iShotsFired++;
	
	WeaponSound( SINGLE );

	pPlayer->DoMuzzleFlash();

	m_iClip1--;

	SendWeaponAnim( ACT_VM_PRIMARYATTACK );
	pPlayer->SetAnimation( PLAYER_ATTACK1 );

	m_flNextPrimaryAttack	= gpGlobals->curtime + 0.15;
	m_flNextSecondaryAttack	= gpGlobals->curtime + 0.075;

	Vector vecSrc		= pPlayer->Weapon_ShootPosition();
	Vector vecAiming	= pPlayer->GetAutoaimVector( AUTOAIM_5DEGREES );	

	
	
	FireBulletsInfo_t info( 2, vecSrc, vecAiming, VECTOR_CONE_1DEGREES, MAX_TRACE_LENGTH, m_iPrimaryAmmoType );
	info.m_pAttacker = pPlayer;
	info.m_iTracerFreq = 10;
	pPlayer->FireBullets( info );
	
	

	EjectShell( pPlayer, 0 );

	float yz = random->RandomFloat(-0.1, 0.1);
	pPlayer->ViewPunch(QAngle(random->RandomFloat(-0.1, 0.1), yz, yz));
#ifdef CLIENT_DLL
	pPlayer->DoMuzzleFlash();
#else
	pPlayer->SetMuzzleFlashTime( gpGlobals->curtime + 0.5 );
#endif

#ifdef CLIENT_DLL
#else
	CSoundEnt::InsertSound( SOUND_COMBAT, GetAbsOrigin(), 600, 0.2 );
#endif

	if ( !m_iClip1 && pPlayer->GetAmmoCount( m_iPrimaryAmmoType ) <= 0 )
	{
		// HEV suit - indicate out of ammo condition
		pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);
	}
	
	// Inspired by both the OpFor M249 --GARG
	
	Vector vecNewVel = pPlayer->GetAbsVelocity() - Vector(vecAiming.x * 48, vecAiming.y * 48, vecAiming.z * 16);
	pPlayer->SetAbsVelocity( vecNewVel );
	
	SetWeaponIdleTime( gpGlobals->curtime + 5 );
}

void CWeaponM249::SecondaryAttack( void )
{
	// Only the player fires this way so we can cast
	CBasePlayer *pPlayer = ToBasePlayer( GetOwner() );

	if ( !pPlayer )	
	{
		return;
	}

	if ( m_iClip1 <= 0 )
	{
		DryFire();
		return;
	}

	WeaponSound( SINGLE );

	pPlayer->DoMuzzleFlash();

	m_iClip1--;

	SendWeaponAnim( ACT_VM_PRIMARYATTACK );
	pPlayer->SetAnimation( PLAYER_ATTACK1 );

	m_flNextPrimaryAttack	= gpGlobals->curtime + 0.15;
	m_flNextSecondaryAttack	= gpGlobals->curtime + 0.075;

	Vector vecSrc		= pPlayer->Weapon_ShootPosition();
	Vector vecAiming	= pPlayer->GetAutoaimVector( AUTOAIM_5DEGREES );

	
	
	FireBulletsInfo_t info( 1, vecSrc, vecAiming, VECTOR_CONE_6DEGREES, MAX_TRACE_LENGTH, m_iPrimaryAmmoType );
	info.m_pAttacker = pPlayer;
	info.m_iTracerFreq = 2;
	pPlayer->FireBullets( info );
	
	

	EjectShell( pPlayer, 0 );

	float yz = random->RandomFloat(-0.5, 0.5);
	pPlayer->ViewPunch(QAngle(random->RandomFloat(-0.5, 0.5), yz, yz));
#ifdef CLIENT_DLL
	pPlayer->DoMuzzleFlash();
#else
	pPlayer->SetMuzzleFlashTime( gpGlobals->curtime + 0.5 );
#endif

#ifdef CLIENT_DLL
#else
	CSoundEnt::InsertSound( SOUND_COMBAT, GetAbsOrigin(), 600, 0.2 );
#endif

	if ( !m_iClip1 && pPlayer->GetAmmoCount( m_iPrimaryAmmoType ) <= 0 )
	{
		// HEV suit - indicate out of ammo condition
		pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);
	}
	
	// Inspired by both the OpFor M249 and the Big Cock from [MoD] DeathMatch. --GARG
	
	Vector vecNewVel = pPlayer->GetAbsVelocity() - Vector(vecAiming.x * 24, vecAiming.y * 24, vecAiming.z * 8);
	pPlayer->SetAbsVelocity( vecNewVel );
	
	SetWeaponIdleTime( gpGlobals->curtime + 5 );
}





void CWeaponM249::DryFire( void )
{
	WeaponSound( EMPTY );
	m_flNextPrimaryAttack	= gpGlobals->curtime + 0.15;
}


void CWeaponM249::WeaponIdle( void )
{
	CBasePlayer *pPlayer = ToBasePlayer( GetOwner() );
	if ( pPlayer )
	{
		pPlayer->GetAutoaimVector( AUTOAIM_5DEGREES );
	}

	// Semi-auto behaviour
	
	m_iShotsFired = 0;
		
	bool bElapsed = HasWeaponIdleTimeElapsed();

	BaseClass::WeaponIdle();
	
	if( bElapsed )
		SetWeaponIdleTime( gpGlobals->curtime + 5 );
}
