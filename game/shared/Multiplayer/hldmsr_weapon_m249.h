//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		Projectile shot from the M249 
//
// $Workfile:     $
// $Date:         $
//
//-----------------------------------------------------------------------------
// $Log: $
//
// $NoKeywords: $
//=============================================================================//

#ifndef	WEAPONM249_H
#define	WEAPONM249_H

#include "hl1mp_basecombatweapon_shared.h"

#ifdef CLIENT_DLL
#define CWeaponM249 C_WeaponM249
#endif

class CWeaponM249 : public CBaseHL1MPCombatWeapon
{
	DECLARE_CLASS( CWeaponM249, CBaseHL1MPCombatWeapon );
public:

	DECLARE_NETWORKCLASS(); 
	DECLARE_PREDICTABLE();

	CWeaponM249();

	void	Precache( void );
	
	int m_iShotsFired /*= 0*/; // need to comment this out to compile on linux
	void	PrimaryAttack( void );
	void	SecondaryAttack( void );
	void	DryFire( void );
	void	WeaponIdle( void );

	//DECLARE_SERVERCLASS();
	DECLARE_DATADESC();
};


#endif	//WEAPONM249_H
