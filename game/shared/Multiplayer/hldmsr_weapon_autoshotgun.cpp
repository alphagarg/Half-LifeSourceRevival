//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: This is the AutoShotgun weapon
//
// $Workfile:     $
// $Date:         $
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "npcevent.h"
#include "hl1mp_basecombatweapon_shared.h"
//#include "basecombatcharacter.h"
//#include "ai_basenpc.h"
#ifdef CLIENT_DLL
#include "c_baseplayer.h"
#else
#include "player.h"
#endif
#include "gamerules.h"		// For g_pGameRules

#include "IEffects.h"
#ifdef CLIENT_DLL
#include "c_te_effect_dispatch.h"
#else
#include "te_effect_dispatch.h"
#endif

#include "in_buttons.h"
//#include "soundent.h"
#include "vstdlib/random.h"

#ifdef CLIENT_DLL
#define CWeaponAutoShotgun C_WeaponAutoShotgun
#endif

// special deathmatch autoshotgun spreads
#define VECTOR_CONE_DM_SHOTGUN	Vector( 0.08716, 0.04362, 0.00  )// 10 degrees by 5 degrees
#define VECTOR_CONE_DM_DOUBLESHOTGUN Vector( 0.17365, 0.04362, 0.00 ) // 20 degrees by 5 degrees
#define VECTOR_RANDOM Vector( random->RandomFloat(-16, 16), random->RandomFloat(-16, 16), random->RandomFloat(-16, 16) )

class CWeaponAutoShotgun : public CBaseHL1MPCombatWeapon
{
	DECLARE_CLASS( CWeaponAutoShotgun, CBaseHL1MPCombatWeapon );

	DECLARE_NETWORKCLASS(); 
	DECLARE_PREDICTABLE();

private:
	CNetworkVar( int, m_fInSpecialReload );

public:
	void	Precache( void );

	bool Reload( void );
	void FillClip( void );
	void WeaponIdle( void );
	int m_iShotsFired /*= 0*/; // need to comment that out to compile on linux
	void PrimaryAttack( void );
	//void SecondaryAttack( void );
	void DryFire( void );

//	DECLARE_SERVERCLASS();
//	DECLARE_DATADESC();

	CWeaponAutoShotgun(void);

//#ifndef CLIENT_DLL
//	DECLARE_ACTTABLE();
//#endif
};

IMPLEMENT_NETWORKCLASS_ALIASED( WeaponAutoShotgun, DT_WeaponAutoShotgun );

BEGIN_NETWORK_TABLE( CWeaponAutoShotgun, DT_WeaponAutoShotgun )
#ifdef CLIENT_DLL
	RecvPropInt( RECVINFO( m_fInSpecialReload ) ),
#else
	SendPropInt( SENDINFO( m_fInSpecialReload ) ),
#endif
END_NETWORK_TABLE()

BEGIN_PREDICTION_DATA( CWeaponAutoShotgun )
#ifdef CLIENT_DLL
	DEFINE_PRED_FIELD( m_fInSpecialReload, FIELD_INTEGER, FTYPEDESC_INSENDTABLE ),
#endif
END_PREDICTION_DATA()

LINK_ENTITY_TO_CLASS( weapon_autoshotgun, CWeaponAutoShotgun );
PRECACHE_WEAPON_REGISTER(weapon_autoshotgun);

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CWeaponAutoShotgun::CWeaponAutoShotgun( void )
{
	m_bReloadsSingly	= true;
	m_bFiresUnderwater	= false;
	m_fInSpecialReload	= 0;
	m_iShotsFired		= 0;
}

void CWeaponAutoShotgun::Precache( void )
{
	BaseClass::Precache();
}

extern short	g_sModelIndexFireball;

void CWeaponAutoShotgun::PrimaryAttack( void )
{
	// Only the player fires this way so we can cast
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());

	if (!pPlayer)
	{
		return;
	}

	if (m_iClip1 <= 0)
	{
		Reload();
		DryFire();

		return;
	}

	// MUST call sound before removing a round from the clip of a CMachineGun
	WeaponSound(SINGLE);

	pPlayer->DoMuzzleFlash();

	SendWeaponAnim(ACT_VM_PRIMARYATTACK);
	pPlayer->SetAnimation(PLAYER_ATTACK1);

	m_flNextPrimaryAttack = gpGlobals->curtime + 0.2;
	m_iClip1 -= 1;

	Vector vecSrc = pPlayer->Weapon_ShootPosition();
	Vector vecAiming = pPlayer->GetAutoaimVector(AUTOAIM_5DEGREES);

	if (g_pGameRules->IsMultiplayer())
	{
		FireBulletsInfo_t info(8, vecSrc, vecAiming, VECTOR_CONE_DM_SHOTGUN, MAX_TRACE_LENGTH, m_iPrimaryAmmoType);
		info.m_pAttacker = pPlayer;

		pPlayer->FireBullets(info);
	}
	else
	{
		FireBulletsInfo_t info(8, vecSrc, vecAiming, VECTOR_CONE_10DEGREES, MAX_TRACE_LENGTH, m_iPrimaryAmmoType);
		info.m_pAttacker = pPlayer;

		pPlayer->FireBullets(info);

		//		pPlayer->FireBullets( 6, vecSrc, vecAiming, VECTOR_CONE_10DEGREES, MAX_TRACE_LENGTH, m_iPrimaryAmmoType, 0 );
	}

	EjectShell(pPlayer, 1);

#if !defined(CLIENT_DLL)
	pPlayer->SetMuzzleFlashTime(gpGlobals->curtime + 1.0);
#endif

	float recoil = random->RandomFloat(-4, 4);
	pPlayer->ViewPunch(QAngle(-4, recoil, recoil));

	//	CSoundEnt::InsertSound( SOUND_COMBAT, GetAbsOrigin(), 600, 0.2 );
	WeaponSound(SINGLE);

	if (!m_iClip1 && pPlayer->GetAmmoCount(m_iPrimaryAmmoType) <= 0)
	{
		// HEV suit - indicate out of ammo condition
		pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);
	}

	m_fInSpecialReload = 0;
}

/*
void CWeaponAutoShotgun::SecondaryAttack( void )
{
	
}
*/

bool CWeaponAutoShotgun::Reload( void )
{
	CBaseCombatCharacter *pPlayer  = GetOwner();
	
	if ( pPlayer == NULL )
		return false;

	if ( pPlayer->GetAmmoCount( m_iPrimaryAmmoType ) <= 0 )
		return false;

	if ( m_iClip1 >= GetMaxClip1() )
		return false;

	// don't reload until recoil is done
	if ( m_flNextPrimaryAttack > gpGlobals->curtime )
		return false;

	// check to see if we're ready to reload
	if ( m_fInSpecialReload == 0 )
	{
		SendWeaponAnim( ACT_SHOTGUN_RELOAD_START );
		m_fInSpecialReload = 1;

		pPlayer->m_flNextAttack	= gpGlobals->curtime + 0.3;
		SetWeaponIdleTime( gpGlobals->curtime + 0.25 );
		m_flNextPrimaryAttack	= gpGlobals->curtime + 0.5;
		//m_flNextSecondaryAttack	= gpGlobals->curtime + 1.0;

		return true;
	}
	else if ( m_fInSpecialReload == 1 )
	{
		if ( !HasWeaponIdleTimeElapsed() )
			return false;

		// was waiting for gun to move to side
		m_fInSpecialReload = 2;

		// Play reload on different channel as otherwise steals channel away from fire sound
		WeaponSound( RELOAD );
		SendWeaponAnim( ACT_VM_RELOAD );

		SetWeaponIdleTime( gpGlobals->curtime + 0.25 );
	}
	else
	{
		FillClip();
		m_fInSpecialReload = 1;
	}

	return true;
}


void CWeaponAutoShotgun::FillClip( void )
{
	CBaseCombatCharacter *pPlayer  = GetOwner();
	
	if ( pPlayer == NULL )
		return;

	// Add them to the clip
	m_iClip1++;
	pPlayer->RemoveAmmo( 1, m_iPrimaryAmmoType );
}


void CWeaponAutoShotgun::DryFire( void )
{
	WeaponSound( EMPTY );
	m_flNextPrimaryAttack	= gpGlobals->curtime + 0.75;
	//m_flNextSecondaryAttack	= gpGlobals->curtime + 0.75;
}


void CWeaponAutoShotgun::WeaponIdle( void )
{
	CBasePlayer *pPlayer = ToBasePlayer( GetOwner() );

	if ( pPlayer == NULL )
		return;

	// Semi-auto behaviour
	
	m_iShotsFired = 0;
		
	pPlayer->GetAutoaimVector( AUTOAIM_10DEGREES );

	if ( HasWeaponIdleTimeElapsed() )
	{
		if ( m_iClip1 == 0 && m_fInSpecialReload == 0 && pPlayer->GetAmmoCount( m_iPrimaryAmmoType ) > 0 )
		{
			Reload();
		}
		else if ( m_fInSpecialReload != 0 )
		{
			if ( m_iClip1 != 6 && pPlayer->GetAmmoCount( m_iPrimaryAmmoType ) > 0 )
			{
				Reload( );
			}
			else
			{
				SendWeaponAnim( ACT_SHOTGUN_PUMP );
				m_fInSpecialReload = 0;
				SetWeaponIdleTime( gpGlobals->curtime + 0.25 );
			}
		}
		else
		{
			int		iAnim;
			float	flRand = random->RandomFloat( 0, 1 );

			if ( flRand <= 0.8 )
			{
				iAnim = ACT_SHOTGUN_IDLE_DEEP;
			}
			else if ( flRand <= 0.95 )
			{
				iAnim = ACT_VM_IDLE;
			}
			else
			{
				iAnim = ACT_SHOTGUN_IDLE4;
			}

			SendWeaponAnim( iAnim );
		}
	}
}
