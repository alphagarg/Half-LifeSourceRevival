#ifndef HL1MP_GAMERULES_H
#define HL1MP_GAMERULES_H
#pragma once

#include "gamerules.h"
#include "teamplay_gamerules.h"

#ifdef CLIENT_DLL
	#define CHL1MPRules C_HL1MPRules
	#define CHL1MPGameRulesProxy C_HL1MPGameRulesProxy
#endif


class CHL1MPGameRulesProxy : public CGameRulesProxy
{
public:
	DECLARE_CLASS( CHL1MPGameRulesProxy, CGameRulesProxy );
	DECLARE_NETWORKCLASS();
};


class CHL1MPRules : public CTeamplayRules
{
public:
	DECLARE_CLASS( CHL1MPRules, CTeamplayRules );

#ifdef CLIENT_DLL
	DECLARE_CLIENTCLASS_NOBASE();
#else
	DECLARE_SERVERCLASS_NOBASE();
#endif

	CHL1MPRules();
	virtual ~CHL1MPRules();

	virtual void CreateStandardEntities( void );

	virtual float GetAmmoDamage( CBaseEntity *pAttacker, CBaseEntity *pVictim, int nAmmoType );
    virtual float GetDamageMultiplier( void );    

#ifndef CLIENT_DLL
	virtual void Think ( void );

	virtual void GoToIntermission( void );

	virtual float FlWeaponRespawnTime( CBaseCombatWeapon *pWeapon );
	virtual float FlItemRespawnTime( CItem *pItem );

	virtual void InitHUD( CBasePlayer *pPlayer );
	virtual void ClientSettingsChanged( CBasePlayer *pPlayer );
#endif
};


inline CHL1MPRules* HL1MPRules()
{
	return static_cast<CHL1MPRules*>(g_pGameRules);
}

#ifndef CLIENT_DLL
int UTIL_HumansInGame( bool ignoreSpectators = false );
#endif

#endif
