#include "cbase.h"
#include "hl1mp_gamerules.h"
#include "gamerules_register.h"
#include "viewport_panel_names.h"
#include "gameeventdefs.h"
#include <KeyValues.h>
#include "ammodef.h"

#ifdef CLIENT_DLL

#include "hl1/c_hl1mp_player.h"

#else

#include "eventqueue.h"
#include "player.h"
#include "gamerules.h"
#include "game.h"
#include "items.h"
#include "entitylist.h"
#include "in_buttons.h"
#include <ctype.h>
#include "voice_gamemgr.h"
#include "iscorer.h"
#include "hl1mp_player.h"
#include "voice_gamemgr.h"

ConVar sv_hl1mp_weapon_respawn_time( "sv_hl1mp_weapon_respawn_time", "20", FCVAR_GAMEDLL | FCVAR_NOTIFY );
ConVar sv_hl1mp_item_respawn_time( "sv_hl1mp_item_respawn_time", "30", FCVAR_GAMEDLL | FCVAR_NOTIFY );

#endif

REGISTER_GAMERULES_CLASS( CHL1MPRules );

BEGIN_NETWORK_TABLE_NOBASE( CHL1MPRules, DT_HL1MPRules )

END_NETWORK_TABLE()

LINK_ENTITY_TO_CLASS( hl1mp_gamerules, CHL1MPGameRulesProxy );
IMPLEMENT_NETWORKCLASS_ALIASED( HL1MPGameRulesProxy, DT_HL1MPGameRulesProxy )

#ifdef CLIENT_DLL
	void RecvProxy_HL1MPRules( const RecvProp *pProp, void **pOut, void *pData, int objectID )
	{
		CHL1MPRules *pRules = HL1MPRules();
		Assert( pRules );
		*pOut = pRules;
	}

	BEGIN_RECV_TABLE( CHL1MPGameRulesProxy, DT_HL1MPGameRulesProxy )
		RecvPropDataTable( "hl1mp_gamerules_data", 0, 0, &REFERENCE_RECV_TABLE( DT_HL1MPRules ), RecvProxy_HL1MPRules )
	END_RECV_TABLE()
#else
	void* SendProxy_HL1MPRules( const SendProp *pProp, const void *pStructBase, const void *pData, CSendProxyRecipients *pRecipients, int objectID )
	{
		CHL1MPRules *pRules = HL1MPRules();
		Assert( pRules );
		return pRules;
	}

	BEGIN_SEND_TABLE( CHL1MPGameRulesProxy, DT_HL1MPGameRulesProxy )
		SendPropDataTable( "hl1mp_gamerules_data", 0, &REFERENCE_SEND_TABLE( DT_HL1MPRules ), SendProxy_HL1MPRules )
	END_SEND_TABLE()
#endif

#ifndef CLIENT_DLL

#if 0
	class CVoiceGameMgrHelper : public IVoiceGameMgrHelper
	{
	public:
		virtual bool		CanPlayerHearPlayer( CBasePlayer *pListener, CBasePlayer *pTalker )
		{
			return true;
		}
	};
	CVoiceGameMgrHelper g_VoiceGameMgrHelper;
	IVoiceGameMgrHelper *g_pVoiceGameMgrHelper = &g_VoiceGameMgrHelper;
#endif

#endif

void CHL1MPRules::CreateStandardEntities( void )
{

#ifndef CLIENT_DLL
	// Create the entity that will send our data to the client.

	BaseClass::CreateStandardEntities();

#ifdef _DEBUG
	CBaseEntity *pEnt = 
#endif
	CBaseEntity::Create( "hl1mp_gamerules", vec3_origin, vec3_angle );
	Assert( pEnt );
#endif
}

// CTORS (constructor functions), DON'T REMOVE EITHER OF THESE
CHL1MPRules::CHL1MPRules() {}
CHL1MPRules::~CHL1MPRules( void ) {}

float CHL1MPRules::GetAmmoDamage( CBaseEntity *pAttacker, CBaseEntity *pVictim, int nAmmoType )
{
    return BaseClass::GetAmmoDamage( pAttacker, pVictim, nAmmoType ) * GetDamageMultiplier();
}

float CHL1MPRules::GetDamageMultiplier( void )
{
    if ( IsMultiplayer() )
        return 0.75f;
    else
        return 1.0f;
}



#ifdef CLIENT_DLL
#else


void CHL1MPRules::Think ( void )
{
	CGameRules::Think();

	if ( g_fGameOver )   // someone else quit the game already
	{
		// check to see if we should change levels now
		if ( m_flIntermissionEndTime < gpGlobals->curtime )
		{
			ChangeLevel(); // intermission is over
		}

		return;
	}

	float flTimeLimit = mp_timelimit.GetFloat() * 60;
	float flFragLimit = fraglimit.GetFloat();
	
	if ( flTimeLimit != 0 && gpGlobals->curtime >= flTimeLimit )
	{
		GoToIntermission();
		return;
	}

	if ( flFragLimit )
	{
		// check if any player is over the frag limit
		for ( int i = 1; i <= gpGlobals->maxClients; i++ )
		{
			CBasePlayer *pPlayer = UTIL_PlayerByIndex( i );

			if ( pPlayer && pPlayer->FragCount() >= flFragLimit )
			{
				GoToIntermission();
				return;
			}
		}
	}

//	ManageObjectRelocation();
}


void CHL1MPRules::GoToIntermission()
{
#ifndef CLIENT_DLL
	if ( g_fGameOver )
		return;

	g_fGameOver = true;

	for ( int i = 0; i < MAX_PLAYERS; i++ )
	{
		CBasePlayer *pPlayer = UTIL_PlayerByIndex( i );

		if ( !pPlayer )
			continue;

		pPlayer->ShowViewPortPanel( PANEL_SCOREBOARD );
		pPlayer->AddFlag( FL_FROZEN );
	}
#endif
}

void CHL1MPRules::InitHUD( CBasePlayer *pPlayer )
{
	BaseClass::InitHUD( pPlayer );
}

void CHL1MPRules::ClientSettingsChanged( CBasePlayer *pPlayer )
{
	CHL1MP_Player *pHL1Player = ToHL1MPPlayer( pPlayer );

	if ( pHL1Player == NULL )
		return;

	// strip down to just the name
	char szTempCurrentModel[128];
	Q_FileBase( modelinfo->GetModelName( pPlayer->GetModel() ), szTempCurrentModel, 128 );
	const char *pCurrentModel = szTempCurrentModel;

	char szTempModelName[128];
	Q_FileBase( engine->GetClientConVarValue( engine->IndexOfEdict( pPlayer->edict() ), "cl_playermodel" ), szTempModelName, 128 );	
	const char *szModelName = szTempModelName;

	//If we're different.
	if ( strlen( szModelName ) > 0 && stricmp( szModelName, pCurrentModel ) )
	{
		//Too soon, set the cvar back to what it was.
		//Note: this will make this function be called again
		//but since our models will match it'll just skip this whole dealio.
		if ( pHL1Player->GetNextModelChangeTime() >= gpGlobals->curtime )
		{
			char szReturnString[512];

			Q_snprintf( szReturnString, sizeof (szReturnString ), "cl_playermodel %s\n", pCurrentModel );
			engine->ClientCommand ( pHL1Player->edict(), szReturnString );

			Q_snprintf( szReturnString, sizeof( szReturnString ), "Please wait %d more seconds before trying to switch.\n", (int)(pHL1Player->GetNextModelChangeTime() - gpGlobals->curtime) );
			ClientPrint( pHL1Player, HUD_PRINTTALK, szReturnString );
			return;
		}

		pHL1Player->SetPlayerModel();

		//const char *pszCurrentModelName = modelinfo->GetModelName( pHL1Player->GetModel() );

		//char szReturnString[128];
		//Q_snprintf( szReturnString, sizeof( szReturnString ), "Your player model is: %s\n", pszCurrentModelName );

		//Printing the model path for the average player is way too verbose - ml
		//ClientPrint( pHL1Player, HUD_PRINTTALK, szReturnString );
	}

	BaseClass::ClientSettingsChanged( pPlayer );
}

float CHL1MPRules::FlWeaponRespawnTime( CBaseCombatWeapon *pWeapon )
{
	if ( weaponstay.GetInt() > 0 )
	{
		// make sure it's only certain weapons
		if ( !(pWeapon->GetWeaponFlags() & ITEM_FLAG_LIMITINWORLD) )
		{
			return 0;
		}
	}

	return sv_hl1mp_weapon_respawn_time.GetInt();
}

float CHL1MPRules::FlItemRespawnTime( CItem *pItem )
{
	return sv_hl1mp_item_respawn_time.GetInt();
}

#ifndef CLIENT_DLL
int UTIL_HumansInGame( bool ignoreSpectators )
{
	int iCount = 0;

	for ( int i = 1; i <= gpGlobals->maxClients; i++ )
	{
		CHL1MP_Player *entity = dynamic_cast< CHL1MP_Player* >( CHL1MP_Player::Instance( i ) );

		if ( entity && !FNullEnt( entity->edict() ) )
		{
			if ( FStrEq( entity->GetPlayerName(), "" ) )
				continue;

			if ( FBitSet( entity->GetFlags(), FL_FAKECLIENT ) )
				continue;

			if ( ignoreSpectators )
				continue;

			iCount++;
		}
	}

	return iCount;
}
#endif

#endif
