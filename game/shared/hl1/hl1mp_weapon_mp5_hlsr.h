//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		Projectile shot from the MP5 
//
// $Workfile:     $
// $Date:         $
//
//-----------------------------------------------------------------------------
// $Log: $
//
// $NoKeywords: $
//=============================================================================//

#ifndef	WEAPONMP5HLSR_H
#define	WEAPONMP5HLSR_H

#ifdef CLIENT_DLL
#else
#include "hl1_basegrenade.h"
#endif
#include "hl1mp_basecombatweapon_shared.h"

#ifdef CLIENT_DLL
class CGrenadeMP5HLSR;
#else
#endif

#ifdef CLIENT_DLL
#define CWeaponMP5HLSR C_WeaponMP5HLSR
#endif

class CWeaponMP5HLSR : public CBaseHL1MPCombatWeapon
{
	DECLARE_CLASS( CWeaponMP5HLSR, CBaseHL1MPCombatWeapon );
public:

	DECLARE_NETWORKCLASS(); 
	DECLARE_PREDICTABLE();

	CWeaponMP5HLSR();

	void	Precache( void );
	void	PrimaryAttack( void );
	void	SecondaryAttack( void );
	void	DryFire( void );
	void	WeaponIdle( void );

//	DECLARE_SERVERCLASS();
	DECLARE_DATADESC();
};


#endif	//WEAPONMP5HLSR_H
