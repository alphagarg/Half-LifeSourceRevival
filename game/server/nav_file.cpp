//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
// nav_file.cpp
// Reading and writing nav files
// Author: Michael S. Booth (mike@turtlerockstudios.com), January-September 2003

#include "cbase.h"
#include "nav_mesh.h"

#ifdef CSTRIKE_DLL
#include "hl1_shareddefs.h"
#include "nav_pathfind.h"
#endif


//--------------------------------------------------------------------------------------------------------------
/// The current version of the nav file format
const int NavCurrentVersion = 9;

//--------------------------------------------------------------------------------------------------------------
//
// The 'place directory' is used to save and load places from
// nav files in a size-efficient manner that also allows for the 
// order of the place ID's to change without invalidating the
// nav files.
//
// The place directory is stored in the nav file as a list of 
// place name strings.  Each nav area then contains an index
// into that directory, or zero if no place has been assigned to 
// that area.
//
class PlaceDirectory
{
public:

	typedef unsigned short IndexType;

	void Reset( void )
	{
		m_directory.RemoveAll();
	}

	/// return true if this place is already in the directory
	bool IsKnown( Place place ) const
	{
		return m_directory.HasElement( place );
	}

	/// return the directory index corresponding to this Place (0 = no entry)
	IndexType GetIndex( Place place ) const
	{
		if (place == UNDEFINED_PLACE)
			return 0;

		int i = m_directory.Find( place );

		if (i < 0)
		{
			Assert( false && "PlaceDirectory::GetIndex failure" );
			return 0;
		}

		return (IndexType)(i+1);
	}

	/// add the place to the directory if not already known
	void AddPlace( Place place )
	{
		if (place == UNDEFINED_PLACE)
			return;

		Assert( place < 1000 );

		if (IsKnown( place ))
			return;

		m_directory.AddToTail( place );
	}

	/// given an index, return the Place
	Place IndexToPlace( IndexType entry ) const
	{
		if (entry == 0)
			return UNDEFINED_PLACE;

		int i = entry-1;

		if (i >= m_directory.Count())
		{
			Assert( false && "PlaceDirectory::IndexToPlace: Invalid entry" );
			return UNDEFINED_PLACE;
		}

		return m_directory[ i ];
	}

	/// store the directory
	void Save( FileHandle_t file )
	{
		// store number of entries in directory
		IndexType count = (IndexType)m_directory.Count();
		filesystem->Write( &count, sizeof(IndexType), file );

		// store entries		
		for( int i=0; i<m_directory.Count(); ++i )
		{
			const char *placeName = TheNavMesh->PlaceToName( m_directory[i] );

			// store string length plus the string itself
			unsigned short len = (unsigned short)(strlen( placeName ) + 1);
			filesystem->Write( &len, sizeof(unsigned short), file );
			filesystem->Write( placeName, len, file );
		}
	}

	/// load the directory
	void Load( FileHandle_t file )
	{
		// read number of entries
		IndexType count;
		filesystem->Read( &count, sizeof(IndexType), file );

		m_directory.RemoveAll();

		// read each entry
		char placeName[256];
		unsigned short len;
		for( int i=0; i<count; ++i )
		{
			filesystem->Read( &len, sizeof(unsigned short), file );
			filesystem->Read( placeName, len, file );

			AddPlace( TheNavMesh->NameToPlace( placeName ) );
		}
	}

private:
	CUtlVector< Place > m_directory;
};

static PlaceDirectory placeDirectory;


//--------------------------------------------------------------------------------------------------------------
/**
 * Replace extension with "bsp"
 */
char *GetBspFilename( const char *navFilename )
{
	static char bspFilename[256];

	Q_snprintf( bspFilename, sizeof( bspFilename ), "maps\\%s.bsp", STRING( gpGlobals->mapname ) );

	int len = strlen( bspFilename );
	if (len < 3)
		return NULL;

	bspFilename[ len-3 ] = 'b';
	bspFilename[ len-2 ] = 's';
	bspFilename[ len-1 ] = 'p';

	return bspFilename;
}

//--------------------------------------------------------------------------------------------------------------
/*
void CNavArea::Save( FILE *fp ) const
{
	fprintf( fp, "v  %f %f %f\n", m_extent.lo.x, m_extent.lo.y, m_extent.lo.z );
	fprintf( fp, "v  %f %f %f\n", m_extent.hi.x, m_extent.lo.y, m_neZ );
	fprintf( fp, "v  %f %f %f\n", m_extent.hi.x, m_extent.hi.y, m_extent.hi.z );
	fprintf( fp, "v  %f %f %f\n", m_extent.lo.x, m_extent.hi.y, m_swZ );

	static int base = 1;
	fprintf( fp, "\n\ng %04dArea%s%s%s%s\n", m_id, 
				(GetAttributes() & BOT_NAV_CROUCH) ? "CROUCH" : "",
				(GetAttributes() & BOT_NAV_JUMP) ? "JUMP" : "",
				(GetAttributes() & BOT_NAV_PRECISE) ? "PRECISE" : "",
				(GetAttributes() & BOT_NAV_NO_JUMP) ? "NO_JUMP" : "" );
	fprintf( fp, "f %d %d %d %d\n\n", base, base+1, base+2, base+3 );
	base += 4;
}
*/

//--------------------------------------------------------------------------------------------------------------
/**
 * Save a navigation area to the opened binary stream
 */
void CNavArea::Save( FileHandle_t file, unsigned int version ) const
{
	// save ID
	filesystem->Write( &m_id, sizeof(unsigned int), file );

	// save attribute flags
	filesystem->Write( &m_attributeFlags, sizeof(unsigned short), file );

	// save extent of area
	filesystem->Write( &m_extent, 6*sizeof(float), file );

	// save heights of implicit corners
	filesystem->Write( &m_neZ, sizeof(float), file );
	filesystem->Write( &m_swZ, sizeof(float), file );

	// save connections to adjacent areas
	// in the enum order NORTH, EAST, SOUTH, WEST
	for( int d=0; d<NUM_DIRECTIONS; d++ )
	{
		// save number of connections for this direction
		unsigned int count = m_connect[d].Count();
		filesystem->Write( &count, sizeof(unsigned int), file );

		FOR_EACH_LL( m_connect[d], it )
		{
			NavConnect connect = m_connect[d][ it ];
			filesystem->Write( &connect.area->m_id, sizeof(unsigned int), file );
		}
	}

	// store place dictionary entry
	PlaceDirectory::IndexType entry = placeDirectory.GetIndex( GetPlace() );
	filesystem->Write( &entry, sizeof(entry), file );

	// write out ladder info
	int i;
	for ( i=0; i<CNavLadder::NUM_LADDER_DIRECTIONS; ++i )
	{
		// save number of ladderful paths for this area
		unsigned int count = m_ladder[i].Count();
		filesystem->Write( &count, sizeof(unsigned int), file );

		NavLadderConnect ladder;
		FOR_EACH_LL( m_ladder[i], it )
		{
			ladder = m_ladder[i][it];

			unsigned int id = ladder.ladder->GetID();
			filesystem->Write( &id, sizeof( id ), file );
		}
	}
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Load a navigation area from the file
 */
void CNavArea::Load( FileHandle_t file, unsigned int version )
{
	// load ID
	filesystem->Read( &m_id, sizeof(unsigned int), file );

	// update nextID to avoid collisions
	if (m_id >= m_nextID)
		m_nextID = m_id+1;

	// load attribute flags
	if ( version <= 8 )
	{
		unsigned char flags = 0;
		filesystem->Read( &flags, sizeof(unsigned char), file );
		m_attributeFlags = flags;
	}
	else
	{
		filesystem->Read( &m_attributeFlags, sizeof(unsigned short), file );
	}

	// load extent of area
	filesystem->Read( &m_extent, 6*sizeof(float), file );

	m_center.x = (m_extent.lo.x + m_extent.hi.x)/2.0f;
	m_center.y = (m_extent.lo.y + m_extent.hi.y)/2.0f;
	m_center.z = (m_extent.lo.z + m_extent.hi.z)/2.0f;

	// load heights of implicit corners
	filesystem->Read( &m_neZ, sizeof(float), file );
	filesystem->Read( &m_swZ, sizeof(float), file );

	CheckWaterLevel();

	// load connections (IDs) to adjacent areas
	// in the enum order NORTH, EAST, SOUTH, WEST
	for( int d=0; d<NUM_DIRECTIONS; d++ )
	{
		// load number of connections for this direction
		unsigned int count;
		int result = filesystem->Read( &count, sizeof(unsigned int), file );
		Assert( result == sizeof(unsigned int) );

		for( unsigned int i=0; i<count; ++i )
		{
			NavConnect connect;
			result = filesystem->Read( &connect.id, sizeof(unsigned int), file );
			Assert( result == sizeof(unsigned int) );

			// don't allow self-referential connections
			if ( connect.id != m_id )
			{
				m_connect[d].AddToTail( connect );
			}
		}
	}

	if (version < 5)
		return;

	//
	// Load Place data
	//
	PlaceDirectory::IndexType entry;
	filesystem->Read( &entry, sizeof(entry), file );

	// convert entry to actual Place
	SetPlace( placeDirectory.IndexToPlace( entry ) );

	if ( version < 7 )
		return;

	// load ladder data
	for ( int dir=0; dir<CNavLadder::NUM_LADDER_DIRECTIONS; ++dir )
	{
		// load number of connections for this direction
		unsigned int count;
		filesystem->Read( &count, sizeof(unsigned int), file );
		
		for( unsigned int i=0; i<count; ++i )
		{
			NavLadderConnect connect;
			filesystem->Read( &connect.id, sizeof(unsigned int), file );

			bool alreadyConnected = false;
			FOR_EACH_LL( m_ladder[dir], j )
			{
				if ( m_ladder[dir][j].id == connect.id )
				{
					alreadyConnected = true;
					break;
				}
			}

			if ( !alreadyConnected )
			{
				m_ladder[dir].AddToTail( connect );
			}
		}
	}

	if ( version < 8 )
		return;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Convert loaded IDs to pointers
 * Make sure all IDs are converted, even if corrupt data is found.
 */
NavErrorType CNavArea::PostLoad( void )
{
	NavErrorType error = NAV_OK;

	for ( int dir=0; dir<CNavLadder::NUM_LADDER_DIRECTIONS; ++dir )
	{
		FOR_EACH_LL( m_ladder[dir], it )
		{
			NavLadderConnect& connect = m_ladder[dir][it];

			unsigned int id = connect.id;

			if ( TheNavMesh->GetLadders().Find( connect.ladder ) == TheNavMesh->GetLadders().InvalidIndex() )
			{
				connect.ladder = TheNavMesh->GetLadderByID( id );
			}

			if (id && connect.ladder == NULL)
			{
				Msg( "CNavArea::PostLoad: Corrupt navigation ladder data. Cannot connect Navigation Areas.\n" );
				error = NAV_CORRUPT_DATA;
			}
		}
	}

	// connect areas together
	for( int d=0; d<NUM_DIRECTIONS; d++ )
	{
		FOR_EACH_LL( m_connect[d], it )
		{
			NavConnect *connect = &m_connect[ d ][ it ];

			unsigned int id = connect->id;
			connect->area = TheNavMesh->GetNavAreaByID( id );
			if (id && connect->area == NULL)
			{
				Msg( "CNavArea::PostLoad: Corrupt navigation data. Cannot connect Navigation Areas.\n" );
				error = NAV_CORRUPT_DATA;
			}
		}
	}

	// build overlap list
	/// @todo Optimize this
	FOR_EACH_LL( TheNavAreaList, nit )
	{
		CNavArea *area = TheNavAreaList[ nit ];

		if (area == this)
			continue;

		if (IsOverlapping( area ))
			m_overlapList.AddToTail( area );
	}

	return error;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return the filename for this map's "nav map" file
 */
const char *CNavMesh::GetFilename( void ) const
{
	// filename is local to game dir for Steam, so we need to prepend game dir for regular file save
	char gamePath[256];
	engine->GetGameDir( gamePath, 256 );

	static char filename[256];
	Q_snprintf( filename, sizeof( filename ), "%s\\maps\\%s.nav", gamePath, STRING( gpGlobals->mapname ) );

	return filename;
}

//--------------------------------------------------------------------------------------------------------------
/*
============
COM_FixSlashes

Changes all '/' characters into '\' characters, in place.
============
*/
inline void COM_FixSlashes( char *pname )
{
#ifdef _WIN32
	while ( *pname ) 
	{
		if ( *pname == '/' )
			*pname = '\\';
		pname++;
	}
#else
	while ( *pname ) 
	{
		if ( *pname == '\\' )
			*pname = '/';
		pname++;
	}
#endif
}

/**
 * Store Navigation Mesh to a file
 */
bool CNavMesh::Save( void ) const
{
	const char *filename = GetFilename();
	if (filename == NULL)
		return false;

	//
	// Store the NAV file
	//
	COM_FixSlashes( const_cast<char *>(filename) );

	// get size of source bsp file for later (before we open the nav file for writing, in
	// case of failure)
	char *bspFilename = GetBspFilename( filename );
	if (bspFilename == NULL)
	{
		return false;
	}

	FileHandle_t file = filesystem->Open( filename, "wb" );

	if (!file)
	{
		return false;
	}

	// store "magic number" to help identify this kind of file
	unsigned int magic = NAV_MAGIC_NUMBER;
	filesystem->Write( &magic, sizeof(unsigned int), file );

	// store version number of file
	
	
	
	// 4 = Includes size of source bsp file to verify nav data correlation
	// ---- Beta Release at V4 -----
	// 5 = Added Place info
	// ---- Conversion to Src ------
	// 6 = Added Ladder info
	// 7 = Areas store ladder ID's so ladders can have one-way connections
	
	// 9 = Promoted CNavArea's attribute flags to a short
	unsigned int version = NavCurrentVersion;
	filesystem->Write( &version, sizeof(unsigned int), file );

	// store the size of source bsp file in the nav file
	// so we can test if the bsp changed since the nav file was made
	unsigned int bspSize = filesystem->Size( bspFilename );
	DevMsg( "Size of bsp file '%s' is %u bytes.\n", bspFilename, bspSize );

	filesystem->Write( &bspSize, sizeof(unsigned int), file );


	//
	// Build a directory of the Places in this map
	//
	placeDirectory.Reset();

	FOR_EACH_LL( TheNavAreaList, nit )
	{
		CNavArea *area = TheNavAreaList[ nit ];

		Place place = area->GetPlace();

		if (place)
		{
			placeDirectory.AddPlace( place );
		}
	}

	placeDirectory.Save( file );


	//
	// Store navigation areas
	//
	{
		// store number of areas
		unsigned int count = TheNavAreaList.Count();
		filesystem->Write( &count, sizeof(unsigned int), file );

		// store each area
		FOR_EACH_LL( TheNavAreaList, it )
		{
			CNavArea *area = TheNavAreaList[ it ];

			area->Save( file, version );
		}
	}

	//
	// Store ladders
	//
	{
		// store number of ladders
		unsigned int count = m_ladderList.Count();
		filesystem->Write( &count, sizeof(unsigned int), file );

		// store each ladder
		FOR_EACH_LL( m_ladderList, it )
		{
			CNavLadder *ladder = m_ladderList[ it ];

			ladder->Save( file, version );
		}
	}

	filesystem->Flush( file );
	filesystem->Close( file );

	unsigned int navSize = filesystem->Size( filename );
	DevMsg( "Size of nav file '%s' is %u bytes.\n", filename, navSize );

	return true;
}


//--------------------------------------------------------------------------------------------------------------
static NavErrorType CheckNavFile( const char *bspFilename )
{
	if ( !bspFilename )
		return NAV_CANT_ACCESS_FILE;

	char bspPathname[256];
	char filename[256];
	Q_strncpy( bspPathname, "maps/", sizeof( bspPathname ) );
	Q_strncat( bspPathname, bspFilename, sizeof( bspPathname ), COPY_ALL_CHARACTERS );
	Q_strncpy( filename, bspPathname, sizeof( filename ) );
	Q_SetExtension( filename, ".nav", sizeof( filename ) );

	bool navIsInBsp = false;
	FileHandle_t file = filesystem->Open( filename, "rb", "MOD" );	// this ignores .nav files embedded in the .bsp ...
	if ( !file )
	{
		navIsInBsp = true;
		file = filesystem->Open( filename, "rb", "GAME" );	// ... and this looks for one if it's the only one around.
	}

	if (!file)
	{
		return NAV_CANT_ACCESS_FILE;
	}

	// check magic number
	int result;
	unsigned int magic;
	result = filesystem->Read( &magic, sizeof(unsigned int), file );
	if (!result || magic != NAV_MAGIC_NUMBER)
	{
		filesystem->Close( file );
		return NAV_INVALID_FILE;
	}

	// read file version number
	unsigned int version;
	result = filesystem->Read( &version, sizeof(unsigned int), file );
	if (!result || version > NavCurrentVersion || version < 4)
	{
		filesystem->Close( file );
		return NAV_BAD_FILE_VERSION;
	}

	// get size of source bsp file and verify that the bsp hasn't changed
	unsigned int saveBspSize;
	filesystem->Read( &saveBspSize, sizeof(unsigned int), file );

	// verify size
	unsigned int bspSize = filesystem->Size( bspPathname );

	if (bspSize != saveBspSize && !navIsInBsp)
	{
		return NAV_FILE_OUT_OF_DATE;
	}

	return NAV_OK;
}


//--------------------------------------------------------------------------------------------------------------
void CommandNavCheckFileConsistency( void )
{
	if ( !UTIL_IsCommandIssuedByServerAdmin() )
		return;

	FileFindHandle_t findHandle;
	const char *bspFilename = filesystem->FindFirstEx( "maps/*.bsp", "MOD", &findHandle );
	while ( bspFilename )
	{
		switch ( CheckNavFile( bspFilename ) )
		{
		case NAV_CANT_ACCESS_FILE:
			Warning( "Missing nav file for %s\n", bspFilename );
			break;
		case NAV_INVALID_FILE:
			Warning( "Invalid nav file for %s\n", bspFilename );
			break;
		case NAV_BAD_FILE_VERSION:
			Warning( "Old nav file for %s\n", bspFilename );
			break;
		case NAV_FILE_OUT_OF_DATE:
			Warning( "The nav file for %s is built from an old version of the map\n", bspFilename );
			break;
		case NAV_OK:
			Msg( "The nav file for %s is up-to-date\n", bspFilename );
			break;
		}

		bspFilename = filesystem->FindNext( findHandle );
	}
	filesystem->FindClose( findHandle );
}
static ConCommand nav_check_file_consistency( "nav_check_file_consistency", CommandNavCheckFileConsistency, "Scans the maps directory and reports any missing/out-of-date navigation files.", FCVAR_GAMEDLL | FCVAR_CHEAT );


//--------------------------------------------------------------------------------------------------------------
/**
 * Load AI navigation data from a file
 */
NavErrorType CNavMesh::Load( void )
{
	// free previous navigation mesh data
	Reset();
	placeDirectory.Reset();

	CNavArea::m_nextID = 1;

	// nav filename is derived from map filename
	char filename[256];
	Q_snprintf( filename, sizeof( filename ), "maps\\%s.nav", STRING( gpGlobals->mapname ) );

	bool navIsInBsp = false;
	FileHandle_t file = filesystem->Open( filename, "rb", "MOD" );	// this ignores .nav files embedded in the .bsp ...
	if ( !file )
	{
		navIsInBsp = true;
		file = filesystem->Open( filename, "rb", "GAME" );	// ... and this looks for one if it's the only one around.
	}

	if (!file)
	{
		return NAV_CANT_ACCESS_FILE;
	}

	// check magic number
	int result;
	unsigned int magic;
	result = filesystem->Read( &magic, sizeof(unsigned int), file );
	if (!result || magic != NAV_MAGIC_NUMBER)
	{
		Msg( "Invalid navigation file '%s'.\n", filename );
		filesystem->Close( file );
		return NAV_INVALID_FILE;
	}

	// read file version number
	unsigned int version;
	result = filesystem->Read( &version, sizeof(unsigned int), file );
	if (!result || version > NavCurrentVersion)
	{
		Msg( "Unknown navigation file version.\n" );
		filesystem->Close( file );
		return NAV_BAD_FILE_VERSION;
	}

	if (version >= 4)
	{
		// get size of source bsp file and verify that the bsp hasn't changed
		unsigned int saveBspSize;
		filesystem->Read( &saveBspSize, sizeof(unsigned int), file );

		// verify size
		char *bspFilename = GetBspFilename( filename );
		if ( bspFilename == NULL )
		{
			filesystem->Close( file );
			return NAV_INVALID_FILE;
		}

		unsigned int bspSize = filesystem->Size( bspFilename );

		if (bspSize != saveBspSize && !navIsInBsp)
		{
			if ( engine->IsDedicatedServer() )
			{
				// Warning doesn't print to the dedicated server console, so we'll use Msg instead
				Msg( "The Navigation Mesh was built using a different version of this map.\n" );
			}
			else
			{
				Warning( "The Navigation Mesh was built using a different version of this map.\n" );
			}
			m_isFromCurrentMap = false;
		}
	}

	// load Place directory
	if (version >= 5)
	{
		placeDirectory.Load( file );
	}

	// get number of areas
	unsigned int count;
	unsigned int i;
	result = filesystem->Read( &count, sizeof(unsigned int), file );

	Extent extent;
	extent.lo.x = 9999999999.9f;
	extent.lo.y = 9999999999.9f;
	extent.hi.x = -9999999999.9f;
	extent.hi.y = -9999999999.9f;

	// load the areas and compute total extent
	for( i=0; i<count; ++i )
	{
		CNavArea *area = new CNavArea;
		area->Load( file, version );
		TheNavAreaList.AddToTail( area );

		const Extent &areaExtent = area->GetExtent();

		// check validity of nav area
		if (areaExtent.lo.x >= areaExtent.hi.x || areaExtent.lo.y >= areaExtent.hi.y)
			Warning( "WARNING: Degenerate Navigation Area #%d at ( %g, %g, %g )\n", 
						area->GetID(), area->m_center.x, area->m_center.y, area->m_center.z );

		if (areaExtent.lo.x < extent.lo.x)
			extent.lo.x = areaExtent.lo.x;
		if (areaExtent.lo.y < extent.lo.y)
			extent.lo.y = areaExtent.lo.y;
		if (areaExtent.hi.x > extent.hi.x)
			extent.hi.x = areaExtent.hi.x;
		if (areaExtent.hi.y > extent.hi.y)
			extent.hi.y = areaExtent.hi.y;
	}

	// add the areas to the grid
	AllocateGrid( extent.lo.x, extent.hi.x, extent.lo.y, extent.hi.y );

	FOR_EACH_LL( TheNavAreaList, it )
	{
		AddNavArea( TheNavAreaList[ it ] );
	}


	//
	// Set up all the ladders
	//
	if (version >= 6)
	{
		result = filesystem->Read( &count, sizeof(unsigned int), file );

		// load the ladders
		for( i=0; i<count; ++i )
		{
			CNavLadder *ladder = new CNavLadder;
			ladder->Load( file, version );
			m_ladderList.AddToTail( ladder );
		}
	}
	else
	{
		BuildLadders();
	}

	// allow areas to connect to each other, etc
	FOR_EACH_LL( TheNavAreaList, pit )
	{
		CNavArea *area = TheNavAreaList[ pit ];
		area->PostLoad();
	}

	// the Navigation Mesh has been successfully loaded
	m_isLoaded = true;

	filesystem->Close( file );

	return NAV_OK;
}
