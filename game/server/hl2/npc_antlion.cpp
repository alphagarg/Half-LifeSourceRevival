//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:		Panthereye - the leaping blue freak of nature
//
//=============================================================================//

#include "cbase.h"
#include "ai_hint.h"
#include "ai_squad.h"
#include "ai_moveprobe.h"
#include "ai_route.h"
#include "npcevent.h"
#include "gib.h"
#include "entitylist.h"
#include "ndebugoverlay.h"
#include "antlion_dust.h"
#include "engine/IEngineSound.h"
#include "globalstate.h"
#include "movevars_shared.h"
#include "te_effect_dispatch.h"
#include "vehicle_base.h"
#include "mapentities.h"
#include "antlion_maker.h"
#include "npc_antlion.h"
#include "decals.h"
#include "hl2_shareddefs.h"
#include "explode.h"
#include "baseparticleentity.h"
#include "props.h"
#include "particle_parse.h"
#include "ai_tacticalservices.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//Debug visualization
ConVar	g_debug_antlion( "g_debug_antlion", "0" );

// base antlion stuff
ConVar	sk_panthereye_health( "sk_panthereye_health", "0" );
ConVar	sk_panthereye_swipe_damage( "sk_panthereye_swipe_damage", "0" );
ConVar	sk_panthereye_jump_damage( "sk_panthereye_jump_damage", "0" );
ConVar  sk_panthereye_air_attack_dmg( "sk_panthereye_air_attack_dmg", "0" );

ConVar  g_test_new_antlion_jump( "g_test_new_antlion_jump", "1", FCVAR_ARCHIVE );
ConVar	antlion_easycrush( "antlion_easycrush", "1" );
ConVar g_antlion_cascade_push( "g_antlion_cascade_push", "1", FCVAR_ARCHIVE );
 
int AE_ANTLION_WALK_FOOTSTEP;
int AE_ANTLION_MELEE_HIT1;
int AE_ANTLION_MELEE_HIT2;
int AE_ANTLION_MELEE_POUNCE;
int AE_ANTLION_FOOTSTEP_SOFT;
int AE_ANTLION_FOOTSTEP_HEAVY;
int AE_ANTLION_START_JUMP;
int AE_ANTLION_MELEE1_SOUND;
int AE_ANTLION_MELEE2_SOUND;


//Attack range definitions
#define	ANTLION_MELEE1_RANGE		64.0f
#define	ANTLION_MELEE2_RANGE		64.0f
#define	ANTLION_MELEE2_RANGE_MAX	64.0f
#define	ANTLION_MELEE2_RANGE_MIN	64.0f
#define	ANTLION_JUMP_MIN			16.0f

#define	ANTLION_JUMP_MAX_RISE		96.0f
#define	ANTLION_JUMP_MAX			384.0f

//Interaction IDs
int g_interactionAntlionFoundTarget = 0;
int g_interactionAntlionFiredAtTarget = 0;

#define	ANTLION_MODEL			"models/panthereye.mdl"

#define	ANTLION_OBEY_FOLLOW_TIME	5.0f


//==================================================
// AntlionSquadSlots
//==================================================

enum
{	
	SQUAD_SLOT_ANTLION_JUMP = LAST_SHARED_SQUADSLOT,
};

//==================================================
// Antlion Activities
//==================================================

int ACT_ANTLION_JUMP_START;
int ACT_ANTLION_POUNCE;
int ACT_ANTLION_POUNCE_MOVING;
int ACT_ANTLION_DROWN;
int ACT_ANTLION_LAND;


//==================================================
// CNPC_Antlion
//==================================================

CNPC_Antlion::CNPC_Antlion( void )
{
	m_flIdleDelay	= 0.0f;
	m_flJumpTime	= 0.0f;
	m_flPounceTime	= 0.0f;
	m_flObeyFollowTime = 0.0f;

	m_flAlertRadius	= 256.0f;
	m_flFieldOfView	= -0.5f;
	
	m_flIgnoreSoundTime	= 0.0f;
	m_bHasHeardSound	= false;

	m_flNextAcknowledgeTime = 0.0f;
	m_flNextJumpPushTime = 0.0f;

	m_vecLastJumpAttempt.Init();
	m_vecSavedJump.Init();

	m_hFightGoalTarget = NULL;
	m_hFollowTarget = NULL;
	m_bLoopingStarted = false;

	m_bForcedStuckJump = false;
	m_nBodyBone = -1;
}

LINK_ENTITY_TO_CLASS( monster_panthereye, CNPC_Antlion );

//==================================================
// CNPC_Antlion::m_DataDesc
//==================================================

BEGIN_DATADESC( CNPC_Antlion )

	DEFINE_KEYFIELD( m_flAlertRadius,		FIELD_FLOAT,	"radius" ),
	DEFINE_KEYFIELD( m_flEludeDistance,		FIELD_FLOAT,	"eludedist" ),

	DEFINE_FIELD( m_vecSaveSpitVelocity,	FIELD_VECTOR ),
	DEFINE_FIELD( m_flIdleDelay,			FIELD_TIME ),
	DEFINE_FIELD( m_flJumpTime,				FIELD_TIME ),
	DEFINE_FIELD( m_flPounceTime,			FIELD_TIME ),
	DEFINE_FIELD( m_iContext,				FIELD_INTEGER ),
	DEFINE_FIELD( m_vecSavedJump,			FIELD_VECTOR ),
	DEFINE_FIELD( m_vecLastJumpAttempt,		FIELD_VECTOR ),
	DEFINE_FIELD( m_flIgnoreSoundTime,		FIELD_TIME ),
	DEFINE_FIELD( m_vecHeardSound,			FIELD_POSITION_VECTOR ),
	DEFINE_FIELD( m_bHasHeardSound,			FIELD_BOOLEAN ),
	DEFINE_FIELD( m_flNextAcknowledgeTime,	FIELD_TIME ),
	DEFINE_FIELD( m_hFollowTarget,			FIELD_EHANDLE ),
	DEFINE_FIELD( m_hFightGoalTarget,		FIELD_EHANDLE ),
	DEFINE_FIELD( m_strParentSpawner,		FIELD_STRING ),
	DEFINE_FIELD( m_flSuppressFollowTime,	FIELD_FLOAT ),
	DEFINE_FIELD( m_MoveState,				FIELD_INTEGER ),
	DEFINE_FIELD( m_flObeyFollowTime,		FIELD_TIME ),
	DEFINE_FIELD( m_bLeapAttack,			FIELD_BOOLEAN ),
	DEFINE_FIELD( m_bDisableJump,			FIELD_BOOLEAN ),
	DEFINE_FIELD( m_flTimeDrown,			FIELD_TIME ),
	DEFINE_FIELD( m_flTimeDrownSplash,		FIELD_TIME ),
	DEFINE_FIELD( m_bDontExplode,			FIELD_BOOLEAN ),
	DEFINE_FIELD( m_flNextJumpPushTime,		FIELD_TIME ),
	DEFINE_FIELD( m_bForcedStuckJump,		FIELD_BOOLEAN ),
	// DEFINE_FIELD( m_bLoopingStarted, FIELD_BOOLEAN ),
	//			  m_FollowBehavior
	//			  m_AssaultBehavior

	DEFINE_INPUTFUNC( FIELD_STRING,	"FightToPosition", InputFightToPosition ),
	DEFINE_INPUTFUNC( FIELD_STRING,	"StopFightToPosition", InputStopFightToPosition ),
	DEFINE_INPUTFUNC( FIELD_VOID,	"EnableJump", InputEnableJump ),
	DEFINE_INPUTFUNC( FIELD_VOID,	"DisableJump", InputDisableJump ),
	DEFINE_INPUTFUNC( FIELD_STRING,	"JumpAtTarget", InputJumpAtTarget ),

	DEFINE_OUTPUT( m_OnReachFightGoal, "OnReachedFightGoal" ),

	// Function Pointers
	DEFINE_ENTITYFUNC( Touch ),

	// DEFINE_FIELD( FIELD_SHORT, m_hFootstep ),
END_DATADESC()

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::Spawn( void )
{
	Precache();

	SetModel( ANTLION_MODEL );
	SetBloodColor( BLOOD_COLOR_YELLOW );

	SetHullType(HULL_MEDIUM);
	SetHullSizeNormal();
	SetDefaultEyeOffset();
	
	SetNavType( NAV_GROUND );

	m_NPCState	= NPC_STATE_NONE;

	m_iHealth	= sk_panthereye_health.GetFloat();

	SetSolid( SOLID_BBOX );
	AddSolidFlags( FSOLID_NOT_STANDABLE );

	
	SetMoveType( MOVETYPE_STEP );

	//Only do this if a squadname appears in the entity
	if ( m_SquadName != NULL_STRING )
	{
		CapabilitiesAdd( bits_CAP_SQUAD );
	}

	SetCollisionGroup( HL2COLLISION_GROUP_ANTLION );

	CapabilitiesAdd( bits_CAP_MOVE_GROUND | bits_CAP_MOVE_JUMP | bits_CAP_INNATE_MELEE_ATTACK1 | bits_CAP_INNATE_MELEE_ATTACK2 );

	// JAY: Optimize these out for now
	if ( HasSpawnFlags( SF_ANTLION_USE_GROUNDCHECKS ) == false )
		 CapabilitiesAdd( bits_CAP_SKIP_NAV_GROUND_CHECK );

	NPCInit();

	// Antlions will always pursue
	m_flDistTooFar = FLT_MAX;

	m_bDisableJump = false;

	BaseClass::Spawn();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::Activate( void )
{
	// If we're friendly to the player, setup a relationship to reflect it
	if ( IsAllied() )
	{
		// Handle all clients
		for ( int i = 1; i <= gpGlobals->maxClients; i++ )
		{
			CBasePlayer *pPlayer = UTIL_PlayerByIndex( i );

			if ( pPlayer != NULL )
			{
				AddEntityRelationship( pPlayer, D_LI, 99 );
			}
		}
	}

	BaseClass::Activate();
}


//-----------------------------------------------------------------------------
// Purpose: override this to simplify the physics shadow of the antlions
//-----------------------------------------------------------------------------
bool CNPC_Antlion::CreateVPhysics()
{
	bool bRet = BaseClass::CreateVPhysics();
	return bRet;
}

// Use all the gibs
#define	NUM_ANTLION_GIBS_UNIQUE	3
const char *pszAntlionGibs_Unique[NUM_ANTLION_GIBS_UNIQUE] = {
	"models/gibs/antlion_gib_large_1.mdl",
	"models/gibs/antlion_gib_large_2.mdl",
	"models/gibs/antlion_gib_large_3.mdl"
};

#define	NUM_ANTLION_GIBS_MEDIUM	3
const char *pszAntlionGibs_Medium[NUM_ANTLION_GIBS_MEDIUM] = {
	"models/gibs/antlion_gib_medium_1.mdl",
	"models/gibs/antlion_gib_medium_2.mdl",
	"models/gibs/antlion_gib_medium_3.mdl"
};

// XBox sucks
#define	NUM_ANTLION_GIBS_SMALL	3
const char *pszAntlionGibs_Small[NUM_ANTLION_GIBS_SMALL] = {
	"models/gibs/antlion_gib_small_1.mdl",
	"models/gibs/antlion_gib_small_2.mdl",
	"models/gibs/antlion_gib_small_3.mdl"
};

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::Precache( void )
{
	PrecacheModel( ANTLION_MODEL );
	PropBreakablePrecacheAll( MAKE_STRING( ANTLION_MODEL ) );
	PrecacheParticleSystem( "blood_impact_antlion_01" );
	PrecacheParticleSystem( "AntlionGib" );

	for ( int i = 0; i < NUM_ANTLION_GIBS_UNIQUE; ++i )
	{
		PrecacheModel( pszAntlionGibs_Unique[ i ] );
	}
	for ( int i = 0; i < NUM_ANTLION_GIBS_MEDIUM; ++i )
	{
		PrecacheModel( pszAntlionGibs_Medium[ i ] );
	}
	for ( int i = 0; i < NUM_ANTLION_GIBS_SMALL; ++i )
	{
		PrecacheModel( pszAntlionGibs_Small[ i ] );
	}

	PrecacheScriptSound( "NPC_Antlion.MeleeAttack" );
	m_hFootstep = PrecacheScriptSound( "NPC_Antlion.Footstep" );
	PrecacheScriptSound( "NPC_Antlion.FootstepSoft" );
	PrecacheScriptSound( "NPC_Antlion.FootstepHeavy" );
	PrecacheScriptSound( "NPC_Antlion.MeleeAttackSingle" );
	PrecacheScriptSound( "NPC_Antlion.MeleeAttackDouble" );
	PrecacheScriptSound( "NPC_Antlion.Idle" );
	PrecacheScriptSound( "NPC_Antlion.Pain" );
	PrecacheScriptSound( "NPC_Antlion.Land" );

	BaseClass::Precache();
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
inline CBaseEntity *CNPC_Antlion::EntityToWatch( void )
{
	return ( m_hFollowTarget != NULL ) ? m_hFollowTarget.Get() : GetEnemy();
}


//-----------------------------------------------------------------------------
// Purpose: Cache whatever pose parameters we intend to use
//-----------------------------------------------------------------------------
void	CNPC_Antlion::PopulatePoseParameters( void )
{
	m_poseHead_Pitch = LookupPoseParameter("head_pitch");
	m_poseHead_Yaw   = LookupPoseParameter("head_yaw" );

	BaseClass::PopulatePoseParameters();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::UpdateHead( void )
{/*
	float yaw = GetPoseParameter( m_poseHead_Yaw );
	float pitch = GetPoseParameter( m_poseHead_Pitch );

	CBaseEntity *pTarget = EntityToWatch();

	if ( pTarget != NULL )
	{
		Vector	enemyDir = pTarget->WorldSpaceCenter() - WorldSpaceCenter();
		VectorNormalize( enemyDir );
		
		if ( DotProduct( enemyDir, BodyDirection3D() ) < 0.0f )
		{
			SetPoseParameter( m_poseHead_Yaw,	UTIL_Approach( 0, yaw, 10 ) );
			SetPoseParameter( m_poseHead_Pitch, UTIL_Approach( 0, pitch, 10 ) );
			
			return;
		}

		float facingYaw = VecToYaw( BodyDirection3D() );
		float yawDiff = VecToYaw( enemyDir );
		yawDiff = UTIL_AngleDiff( yawDiff, facingYaw + yaw );

		float facingPitch = UTIL_VecToPitch( BodyDirection3D() );
		float pitchDiff = UTIL_VecToPitch( enemyDir );
		pitchDiff = UTIL_AngleDiff( pitchDiff, facingPitch + pitch );

		SetPoseParameter( m_poseHead_Yaw, UTIL_Approach( yaw + yawDiff, yaw, 50 ) );
		SetPoseParameter( m_poseHead_Pitch, UTIL_Approach( pitch + pitchDiff, pitch, 50 ) );
	}
	else
	{
		SetPoseParameter( m_poseHead_Yaw,	UTIL_Approach( 0, yaw, 10 ) );
		SetPoseParameter( m_poseHead_Pitch, UTIL_Approach( 0, pitch, 10 ) );
	}*/
}

#define	ANTLION_VIEW_FIELD_NARROW	0.85f

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pEntity - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::FInViewCone( CBaseEntity *pEntity )
{
	m_flFieldOfView = ( GetEnemy() != NULL ) ? ANTLION_VIEW_FIELD_NARROW : VIEW_FIELD_WIDE;

	return BaseClass::FInViewCone( pEntity );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &vecSpot - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::FInViewCone( const Vector &vecSpot )
{
	m_flFieldOfView = ( GetEnemy() != NULL ) ? ANTLION_VIEW_FIELD_NARROW : VIEW_FIELD_WIDE;

	return BaseClass::FInViewCone( vecSpot );
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
bool CNPC_Antlion::CanBecomeRagdoll()
{
	// This prevents us from dying in the regular way. It forces a schedule selection
	// that will select SCHED_DIE, where we can do our poison burst thing.
	return BaseClass::CanBecomeRagdoll();
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pVictim - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::Event_Killed( const CTakeDamageInfo &info )
{
	VacateStrategySlot();

	if ( info.GetDamageType() & DMG_CRUSH )
	{
		CSoundEnt::InsertSound( SOUND_PHYSICS_DANGER, GetAbsOrigin(), 256, 0.5f, this );
	}

	BaseClass::Event_Killed( info );

	CBaseEntity *pAttacker = info.GetInflictor();

	if ( pAttacker && pAttacker->GetServerVehicle() && ShouldGib( info ) == true )
	{
		trace_t tr;
		UTIL_TraceLine( GetAbsOrigin() + Vector( 0, 0, 64 ), pAttacker->GetAbsOrigin(), MASK_SOLID, this, COLLISION_GROUP_NONE, &tr );
		UTIL_DecalTrace( &tr, "Antlion.Splat" );

		SpawnBlood( GetAbsOrigin(), g_vecAttackDir, BloodColor(), info.GetDamage() );

		CPASAttenuationFilter filter( this );
		EmitSound( filter, entindex(), "NPC_Antlion.RunOverByVehicle" );
	}

	// Stop our zap effect!
	SetContextThink( NULL, gpGlobals->curtime, "ZapThink" );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void CNPC_Antlion::MeleeAttack( float distance, float damage, QAngle &viewPunch, Vector &shove )
{
	Vector vecForceDir;

	CBaseEntity *pHurt = CheckTraceHullAttack( distance, -Vector(16,16,32), Vector(16,16,32), damage, DMG_SLASH, 5.0f );

	if ( pHurt )
	{
		vecForceDir = ( pHurt->WorldSpaceCenter() - WorldSpaceCenter() );

		if ( FClassnameIs( pHurt, "monster_human_grunt" ) )
		{
			CTakeDamageInfo	dmgInfo( this, this, pHurt->m_iHealth+25, DMG_SLASH );
			CalculateMeleeDamageForce( &dmgInfo, vecForceDir, pHurt->GetAbsOrigin() );
			pHurt->TakeDamage( dmgInfo );
			return;
		}

		CBasePlayer *pPlayer = ToBasePlayer( pHurt );

		if ( pPlayer != NULL )
		{
			//Kick the player angles
			if ( !(pPlayer->GetFlags() & FL_GODMODE ) && pPlayer->GetMoveType() != MOVETYPE_NOCLIP )
			{
				pPlayer->ViewPunch( viewPunch );

				Vector	dir = pHurt->GetAbsOrigin() - GetAbsOrigin();
				VectorNormalize(dir);

				QAngle angles;
				VectorAngles( dir, angles );
				Vector forward, right;
				AngleVectors( angles, &forward, &right, NULL );

				//Push the target back
				pHurt->ApplyAbsVelocityImpulse( - right * shove[1] - forward * shove[0] );
			}
		}

		// Play a random attack hit sound
		EmitSound( "NPC_Antlion.MeleeAttack" );
	}
}

// Number of times the antlions will attempt to generate a random chase position
#define NUM_CHASE_POSITION_ATTEMPTS		3

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &targetPos - 
//			&result - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::FindChasePosition( const Vector &targetPos, Vector &result )
{
	if ( HasSpawnFlags( SF_ANTLION_USE_GROUNDCHECKS ) == true )
	{
		 result = targetPos;
		 return true;
	}

	Vector runDir = ( targetPos - GetAbsOrigin() );
	VectorNormalize( runDir );
	
	Vector	vRight, vUp;
	VectorVectors( runDir, vRight, vUp );

	for ( int i = 0; i < NUM_CHASE_POSITION_ATTEMPTS; i++ )
	{
		result	= targetPos;
		result += -runDir * random->RandomInt( 64, 128 );
		result += vRight * random->RandomInt( -128, 128 );
		
		//FIXME: We need to do a more robust search here
		// Find a ground position and try to get there
		if ( GetGroundPosition( result, result ) )
			return true;
	}
	
	//TODO: If we're making multiple inquiries to this, make sure it's evenly spread

	if ( g_debug_antlion.GetInt() == 1 )
	{
		NDebugOverlay::Cross3D( result, -Vector(32,32,32), Vector(32,32,32), 255, 255, 0, true, 2.0f );
	}

	return false;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &testPos - 
//-----------------------------------------------------------------------------
bool CNPC_Antlion::GetGroundPosition( const Vector &testPos, Vector &result )
{
	// Trace up to clear the ground
	trace_t	tr;
	AI_TraceHull( testPos, testPos + Vector( 0, 0, 64 ), NAI_Hull::Mins( GetHullType() ), NAI_Hull::Maxs( GetHullType() ), MASK_NPCSOLID, this, COLLISION_GROUP_NONE, &tr );

	// If we're stuck in solid, this can't be valid
	if ( tr.allsolid )
	{
		if ( g_debug_antlion.GetInt() == 3 )
		{
			NDebugOverlay::BoxDirection( testPos, NAI_Hull::Mins( GetHullType() ), NAI_Hull::Maxs( GetHullType() ) + Vector( 0, 0, 128 ), Vector( 0, 0, 1 ), 255, 0, 0, true, 2.0f );
		}

		return false;
	}

	if ( g_debug_antlion.GetInt() == 3 )
	{
		NDebugOverlay::BoxDirection( testPos, NAI_Hull::Mins( GetHullType() ), NAI_Hull::Maxs( GetHullType() ) + Vector( 0, 0, 128 ), Vector( 0, 0, 1 ), 0, 255, 0, true, 2.0f );
	}

	// Trace down to find the ground
	AI_TraceHull( tr.endpos, tr.endpos - Vector( 0, 0, 128 ), NAI_Hull::Mins( GetHullType() ), NAI_Hull::Maxs( GetHullType() ), MASK_NPCSOLID, this, COLLISION_GROUP_NONE, &tr );

	if ( g_debug_antlion.GetInt() == 3 )
	{
		NDebugOverlay::BoxDirection( tr.endpos, NAI_Hull::Mins( GetHullType() ) - Vector( 0, 0, 256 ), NAI_Hull::Maxs( GetHullType() ), Vector( 0, 0, 1 ), 255, 255, 0, true, 2.0f );
	}

	// We must end up on the floor with this trace
	if ( tr.fraction < 1.0f )
	{
		if ( g_debug_antlion.GetInt() == 3 )
		{
			NDebugOverlay::Cross3D( tr.endpos, NAI_Hull::Mins( GetHullType() ), NAI_Hull::Maxs( GetHullType() ), 255, 0, 0, true, 2.0f );
		}

		result = tr.endpos;
		return true;
	}

	// Ended up in open space
	return false;
}
void CNPC_Antlion::ManageFleeCapabilities( bool bEnable )
{
	if ( bEnable == false )
	{
		//Remove the jump capabilty when we build our route.
		//We'll enable it back again after the route has been built.
		CapabilitiesRemove( bits_CAP_MOVE_JUMP );

		if ( HasSpawnFlags( SF_ANTLION_USE_GROUNDCHECKS ) == false  )
			 CapabilitiesRemove( bits_CAP_SKIP_NAV_GROUND_CHECK );
	}
	else
	{
		if ( m_bDisableJump == false )
			 CapabilitiesAdd( bits_CAP_MOVE_JUMP );

		if ( HasSpawnFlags( SF_ANTLION_USE_GROUNDCHECKS ) == false  )
			 CapabilitiesAdd( bits_CAP_SKIP_NAV_GROUND_CHECK );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : soundType - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::GetPathToSoundFleePoint( int soundType )
{
	CSound *pSound = GetLoudestSoundOfType( soundType );

	if ( pSound == NULL  )
	{
		//NOTENOTE: If you're here, there's a disparity between Listen() and GetLoudestSoundOfType() - jdw
		TaskFail( "Unable to find thumper sound!" );
		return false;
	}

	ManageFleeCapabilities( false );

	//Try and find a hint-node first
	CHintCriteria	hintCriteria;

	hintCriteria.SetHintType( HINT_ANTLION_THUMPER_FLEE_POINT );
	hintCriteria.SetFlag( bits_HINT_NODE_NEAREST );
	hintCriteria.AddIncludePosition( WorldSpaceCenter(), 2500 );

	CAI_Hint *pHint = CAI_HintManager::FindHint( WorldSpaceCenter(), hintCriteria );

	Vector vecFleeGoal;
	Vector vecSoundPos = pSound->GetSoundOrigin();

	// Put the sound location on the same plane as the antlion.
	vecSoundPos.z = GetAbsOrigin().z;

	Vector vecFleeDir = GetAbsOrigin() - vecSoundPos;
	VectorNormalize( vecFleeDir );

	if ( pHint != NULL )
	{
		// Get our goal position
		pHint->GetPosition( this, &vecFleeGoal );

		// Find a route to that position
		AI_NavGoal_t goal( vecFleeGoal, (Activity) ACT_RUN, 128, AIN_DEF_FLAGS );

		if ( GetNavigator()->SetGoal( goal ) )
		{
			pHint->Lock( this );
			pHint->Unlock( 2.0f );

			GetNavigator()->SetArrivalDirection( -vecFleeDir );

			ManageFleeCapabilities( true );
			return true;
		}
	}

	//Make us offset this a little at least
	float flFleeYaw = VecToYaw( vecFleeDir ) + random->RandomInt( -20, 20 );

	vecFleeDir = UTIL_YawToVector( flFleeYaw );

	// Move us to the outer radius of the noise (with some randomness)
	vecFleeGoal = vecSoundPos + vecFleeDir * ( pSound->Volume() + random->RandomInt( 32, 64 ) );

	// Find a route to that position
	AI_NavGoal_t goal( vecFleeGoal + Vector( 0, 0, 8 ), (Activity) ACT_RUN, 512, AIN_DEF_FLAGS );

	if ( GetNavigator()->SetGoal( goal ) )
	{
		GetNavigator()->SetArrivalDirection( -vecFleeDir );

		ManageFleeCapabilities( true );
		return true;
	}

	ManageFleeCapabilities( true );
	return false;
}

//-----------------------------------------------------------------------------
// Purpose: Returns whether the enemy has been seen within the time period supplied
// Input  : flTime - Timespan we consider
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::SeenEnemyWithinTime( float flTime )
{
	float flLastSeenTime = GetEnemies()->LastTimeSeen( GetEnemy() );
	return ( flLastSeenTime != 0.0f && ( gpGlobals->curtime - flLastSeenTime ) < flTime );
}

//-----------------------------------------------------------------------------
// Purpose: Test whether this antlion can hit the target
//-----------------------------------------------------------------------------
bool CNPC_Antlion::InnateWeaponLOSCondition( const Vector &ownerPos, const Vector &targetPos, bool bSetConditions )
{
	if ( GetNextAttack() > gpGlobals->curtime )
		return false;

	return BaseClass::InnateWeaponLOSCondition( ownerPos, targetPos, bSetConditions );
}

//
//	FIXME: Create this in a better fashion!
//

Vector VecCheckThrowTolerance( CBaseEntity *pEdict, const Vector &vecSpot1, Vector vecSpot2, float flSpeed, float flTolerance )
{
	flSpeed = MAX( 1.0f, flSpeed );

	float flGravity = GetCurrentGravity();

	Vector vecGrenadeVel = (vecSpot2 - vecSpot1);

	// throw at a constant time
	float time = vecGrenadeVel.Length( ) / flSpeed;
	vecGrenadeVel = vecGrenadeVel * (1.0 / time);

	// adjust upward toss to compensate for gravity loss
	vecGrenadeVel.z += flGravity * time * 0.5;

	Vector vecApex = vecSpot1 + (vecSpot2 - vecSpot1) * 0.5;
	vecApex.z += 0.5 * flGravity * (time * 0.5) * (time * 0.5);


	trace_t tr;
	UTIL_TraceLine( vecSpot1, vecApex, MASK_SOLID, pEdict, COLLISION_GROUP_NONE, &tr );
	if (tr.fraction != 1.0)
	{
		return vec3_origin;
	}

	UTIL_TraceLine( vecApex, vecSpot2, MASK_SOLID_BRUSHONLY, pEdict, COLLISION_GROUP_NONE, &tr );
	if ( tr.fraction != 1.0 )
	{
		bool bFail = true;

		// Didn't make it all the way there, but check if we're within our tolerance range
		if ( flTolerance > 0.0f )
		{
			float flNearness = ( tr.endpos - vecSpot2 ).LengthSqr();
			if ( flNearness < Square( flTolerance ) )
			{
				bFail = false;
			}
		}
		
		if ( bFail )
		{
			return vec3_origin;
		}
	}
	return vecGrenadeVel;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : flDuration - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::DelaySquadAttack( float flDuration )
{
	if ( GetSquad() )
	{
		// Reduce the duration by as much as 50% of the total time to make this less robotic
		float flAdjDuration = flDuration - random->RandomFloat( 0.0f, (flDuration*0.5f) );
		GetSquad()->BroadcastInteraction( g_interactionAntlionFiredAtTarget, (void *)&flAdjDuration, this );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pEvent - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::HandleAnimEvent( animevent_t *pEvent )
{
	if ( pEvent->event == AE_ANTLION_WALK_FOOTSTEP )
	{
		MakeAIFootstepSound( 240.0f );
		EmitSound( "NPC_Antlion.Footstep", m_hFootstep, pEvent->eventtime );
		
		Vector direction;
		AngleVectors(GetAbsAngles(), &direction); // Then it's re-set here to what it needs to be set to;
		SetAbsVelocity(GetAbsVelocity() + Vector(direction.x * 100, direction.y * 100, 0)); // And finally used here. --GARG
		
		return;
	}

	if ( pEvent->event == AE_ANTLION_MELEE_HIT1 )
	{
		QAngle qa( 20.0f, 0.0f, -12.0f );
		Vector vec( -250.0f, 1.0f, 1.0f );
		MeleeAttack( ANTLION_MELEE1_RANGE, sk_panthereye_swipe_damage.GetFloat(), qa, vec );
		return;
	}

	if ( pEvent->event == AE_ANTLION_MELEE_HIT2 )
	{
		QAngle qa( 20.0f, 0.0f, 0.0f );
		Vector vec( -350.0f, 1.0f, 1.0f );
		MeleeAttack( ANTLION_MELEE1_RANGE, sk_panthereye_swipe_damage.GetFloat(), qa, vec );
		return;
	}

	if ( pEvent->event == AE_ANTLION_MELEE_POUNCE )
	{
		QAngle qa( 4.0f, 0.0f, 0.0f );
		Vector vec( -250.0f, 1.0f, 1.0f );
		MeleeAttack( ANTLION_MELEE2_RANGE, sk_panthereye_swipe_damage.GetFloat(), qa, vec );
		return;
	}
	
	if ( pEvent->event == AE_ANTLION_FOOTSTEP_SOFT )
	{
		EmitSound( "NPC_Antlion.FootstepSoft", pEvent->eventtime );
		return;
	}

	if ( pEvent->event == AE_ANTLION_FOOTSTEP_HEAVY )
	{
		EmitSound( "NPC_Antlion.FootstepHeavy", pEvent->eventtime );
		return;
	}
	
	
	if ( pEvent->event == AE_ANTLION_MELEE1_SOUND )
	{
		EmitSound( "NPC_Antlion.MeleeAttackSingle" );
		return;
	}
	
	if ( pEvent->event == AE_ANTLION_MELEE2_SOUND )
	{
		EmitSound( "NPC_Antlion.MeleeAttackDouble" );
		return;
	}

	if ( pEvent->event == AE_ANTLION_START_JUMP )
	{
		Msg("AE_ANTLION_START_JUMP\n");
		StartJump();
		return;
	}

	BaseClass::HandleAnimEvent( pEvent );
}

bool CNPC_Antlion::IsUnusableNode(int iNodeID, CAI_Hint *pHint)
{
	bool iBaseReturn = BaseClass::IsUnusableNode( iNodeID, pHint );

	if ( g_test_new_antlion_jump.GetBool() == 0 )
		 return iBaseReturn;

	CAI_Node *pNode = GetNavigator()->GetNetwork()->GetNode( iNodeID );

	if ( pNode )
	{
		if ( pNode->IsLocked() )
			 return true;
	}

	return iBaseReturn;
}

void CNPC_Antlion::LockJumpNode( void )
{
	if ( HasSpawnFlags( SF_ANTLION_USE_GROUNDCHECKS ) == false )
		 return;
	
	if ( GetNavigator()->GetPath() == NULL )
		 return;

	if ( g_test_new_antlion_jump.GetBool() == false )
		 return;

	AI_Waypoint_t *pWaypoint = GetNavigator()->GetPath()->GetCurWaypoint();

	while ( pWaypoint )
	{
		AI_Waypoint_t *pNextWaypoint = pWaypoint->GetNext();
		if ( pNextWaypoint && pNextWaypoint->NavType() == NAV_JUMP && pWaypoint->iNodeID != NO_NODE )
		{
			CAI_Node *pNode = GetNavigator()->GetNetwork()->GetNode( pWaypoint->iNodeID );

			if ( pNode )
			{
				//NDebugOverlay::Box( pNode->GetOrigin(), Vector( -16, -16, -16 ), Vector( 16, 16, 16 ), 255, 0, 0, 0, 2 );
				pNode->Lock( 0.5f );
				break;
			}
		}
		else
		{
			pWaypoint = pWaypoint->GetNext();
		}
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
bool CNPC_Antlion::OnObstructionPreSteer( AILocalMoveGoal_t *pMoveGoal, float distClear, AIMoveResult_t *pResult )
{
	bool iBaseReturn = BaseClass::OnObstructionPreSteer( pMoveGoal, distClear, pResult );

	if ( g_test_new_antlion_jump.GetBool() == false )
		 return iBaseReturn;

	if ( HasSpawnFlags( SF_ANTLION_USE_GROUNDCHECKS ) == false )
		 return iBaseReturn;

	CAI_BaseNPC *pBlocker = pMoveGoal->directTrace.pObstruction->MyNPCPointer();

	if ( pBlocker && pBlocker->Classify() == CLASS_ALIEN_PREDATOR )
	{
		// HACKHACK
		CNPC_Antlion *pAntlion = dynamic_cast< CNPC_Antlion * > ( pBlocker );

		if ( pAntlion )
		{
			if ( pAntlion->AllowedToBePushed() == true && GetEnemy() == NULL )
			{
				//NDebugOverlay::Box( pAntlion->GetAbsOrigin(), GetHullMins(), GetHullMaxs(), 0, 255, 0, 0, 2 );
				pAntlion->GetMotor()->SetIdealYawToTarget( WorldSpaceCenter() );
				pAntlion->SetSchedule( SCHED_MOVE_AWAY );
				pAntlion->m_flNextJumpPushTime = gpGlobals->curtime + 2.0f;
			}
		}
	}

	return iBaseReturn;
}

bool NPC_Antlion_IsAntlion( CBaseEntity *pEntity )
{
	CNPC_Antlion *pAntlion = dynamic_cast<CNPC_Antlion *>(pEntity);

	return pAntlion ? true : false;
}

class CTraceFilterAntlion : public CTraceFilterEntitiesOnly
{
public:
	CTraceFilterAntlion( const CBaseEntity *pEntity ) { m_pIgnore = pEntity; }

	virtual bool ShouldHitEntity( IHandleEntity *pHandleEntity, int contentsMask )
	{
		CBaseEntity *pEntity = EntityFromEntityHandle( pHandleEntity );

		if ( m_pIgnore == pEntity )
			 return false;
		
		if ( pEntity->IsNPC() == false )
			 return false;
		
		if ( NPC_Antlion_IsAntlion( pEntity ) )
			 return true;
		
		return false;
	}
private:
	
	const CBaseEntity		*m_pIgnore;
};


//-----------------------------------------------------------------------------
// Purpose:  
//-----------------------------------------------------------------------------
void CNPC_Antlion::StartTask( const Task_t *pTask )
{
	switch ( pTask->iTask ) 
	{
	case TASK_ANTLION_FIND_COVER_FROM_SAVEPOSITION:
		{
			Vector coverPos;

			if ( GetTacticalServices()->FindCoverPos( m_vSavePosition, EyePosition(), 0, CoverRadius(), &coverPos ) ) 
			{
				AI_NavGoal_t goal(coverPos, ACT_RUN, AIN_HULL_TOLERANCE);
				GetNavigator()->SetGoal( goal );

				m_flMoveWaitFinished = gpGlobals->curtime + pTask->flTaskData;
			}
			else
			{
				// no coverwhatsoever.
				TaskFail(FAIL_NO_COVER);
			}
		}
		break;

	case TASK_ANNOUNCE_ATTACK:
		{
			EmitSound( "NPC_Antlion.MeleeAttackSingle" );
			TaskComplete();
			break;
		}

	case TASK_ANTLION_FACE_JUMP:
		break;

	case TASK_ANTLION_DROWN:
	{
		// Set the gravity really low here! Sink slowly
		SetGravity( 0 );
		SetAbsVelocity( vec3_origin );
		m_flTimeDrownSplash = gpGlobals->curtime + random->RandomFloat( 0, 0.5 );
		m_flTimeDrown = gpGlobals->curtime + 4;
		break;
	}

	case TASK_ANTLION_REACH_FIGHT_GOAL:

		m_OnReachFightGoal.FireOutput( this, this );
		TaskComplete();
		break;

	case TASK_ANTLION_DISMOUNT_NPC:
		{
			CBaseEntity *pGroundEnt = GetGroundEntity();
			
			if( pGroundEnt != NULL )
			{
				trace_t trace;
				CTraceFilterAntlion traceFilter( this );
				AI_TraceHull( GetAbsOrigin(), GetAbsOrigin(), WorldAlignMins(), WorldAlignMaxs(), MASK_SOLID, &traceFilter, &trace );

				if ( trace.m_pEnt )
				{
					m_bDontExplode = true;
					OnTakeDamage( CTakeDamageInfo( this, this, m_iHealth+1, DMG_GENERIC ) );
					return;
				}

				// Jump behind the other NPC so I don't block their path.
				Vector vecJumpDir; 

				pGroundEnt->GetVectors( &vecJumpDir, NULL, NULL );

				SetGroundEntity( NULL );
				
				// Bump up
				UTIL_SetOrigin( this, GetAbsOrigin() + Vector( 0, 0 , 1 ) );
				
				SetAbsVelocity( vecJumpDir * -200 + Vector( 0, 0, 100 ) );
			}
			else
			{
				// Dead or gone now
				TaskComplete();
			}
		}

		break;

	case TASK_ANTLION_WAIT_FOR_TRIGGER:
		m_flIdleDelay = gpGlobals->curtime + 1.0f;

		break;

	case TASK_ANTLION_JUMP:
		
		if ( CheckLanding() )
		{
			TaskComplete();
		}

		break;

	case TASK_ANTLION_GET_THUMPER_ESCAPE_PATH:
		{
			if ( GetPathToSoundFleePoint( SOUND_THUMPER ) )			
			{
				TaskComplete();
			}
			else
			{
				TaskFail( FAIL_NO_REACHABLE_NODE );
			}
		}
		
		break;

	case TASK_ANTLION_GET_PHYSICS_DANGER_ESCAPE_PATH:
		{
			if ( GetPathToSoundFleePoint( SOUND_PHYSICS_DANGER ) )
			{
				TaskComplete();
			}
			else
			{
				TaskFail( FAIL_NO_REACHABLE_NODE );
			}
		}
		
		break;


	default:
		BaseClass::StartTask( pTask );
		break;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pTask - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::RunTask( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_ANTLION_FACE_JUMP:
		{
			Vector	jumpDir = m_vecSavedJump;
			VectorNormalize( jumpDir );
			
			QAngle	jumpAngles;
			VectorAngles( jumpDir, jumpAngles );

			GetMotor()->SetIdealYawAndUpdate( jumpAngles[YAW], AI_KEEP_YAW_SPEED );
			SetTurnActivity();
			
			if ( GetMotor()->DeltaIdealYaw() < 2 )
			{
				TaskComplete();
			}
		}

		break;

	case TASK_ANTLION_DROWN:
	{
		if ( gpGlobals->curtime > m_flTimeDrownSplash )
		{
			float flWaterZ = UTIL_FindWaterSurface( GetAbsOrigin(), GetAbsOrigin().z, GetAbsOrigin().z + NAI_Hull::Maxs( GetHullType() ).z );

			CEffectData	data;
			data.m_fFlags = 0;
			data.m_vOrigin = GetAbsOrigin();
			data.m_vOrigin.z = flWaterZ;
			data.m_vNormal = Vector( 0, 0, 1 );
			data.m_flScale = random->RandomFloat( 12.0, 16.0 );

			DispatchEffect( "watersplash", data );
			
			m_flTimeDrownSplash = gpGlobals->curtime + random->RandomFloat( 0.5, 2.5 );
		}
	
		if ( gpGlobals->curtime > m_flTimeDrown )
		{
			m_bDontExplode = true;
			OnTakeDamage( CTakeDamageInfo( this, this, m_iHealth+1, DMG_DROWN ) );
			TaskComplete();
		}
		break;
	}

	case TASK_ANTLION_REACH_FIGHT_GOAL:
		break;

	case TASK_ANTLION_DISMOUNT_NPC:
		
		if ( GetFlags() & FL_ONGROUND )
		{
			CBaseEntity *pGroundEnt = GetGroundEntity();

			if ( ( pGroundEnt != NULL ) && ( ( pGroundEnt->MyNPCPointer() != NULL ) || pGroundEnt->GetSolidFlags() & FSOLID_NOT_STANDABLE ) )
			{
				// Jump behind the other NPC so I don't block their path.
				Vector vecJumpDir; 

				pGroundEnt->GetVectors( &vecJumpDir, NULL, NULL );

				SetGroundEntity( NULL );	
				
				// Bump up
				UTIL_SetOrigin( this, GetAbsOrigin() + Vector( 0, 0 , 1 ) );
				
				Vector vecRandom = RandomVector( -250.0f, 250.0f );
				vecRandom[2] = random->RandomFloat( 100.0f, 200.0f );
				SetAbsVelocity( vecRandom );
			}
			else if ( IsActivityFinished() )
			{
				TaskComplete();
			}
		}
		
		break;

	case TASK_ANTLION_WAIT_FOR_TRIGGER:
		
		if ( ( m_flIdleDelay > gpGlobals->curtime ) || GetEntityName() != NULL_STRING )
			return;

		TaskComplete();

		break;

	case TASK_ANTLION_JUMP:

		if ( CheckLanding() )
		{
			TaskComplete();
		}

		break;

	default:
		BaseClass::RunTask( pTask );
		break;
	}
}

bool CNPC_Antlion::AllowedToBePushed( void )
{
	if ( IsCurSchedule( SCHED_ANTLION_RUN_TO_FIGHT_GOAL ) )
		return false;

	if ( IsRunningDynamicInteraction() )
		return false;

	if ( IsMoving() == false
		 && GetNavType() != NAV_JUMP && m_flNextJumpPushTime <= gpGlobals->curtime )
	{
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------
// Purpose: Returns true if a reasonable jumping distance
// Input  :
// Output :
//-----------------------------------------------------------------------------
bool CNPC_Antlion::IsJumpLegal( const Vector &startPos, const Vector &apex, const Vector &endPos ) const
{
	const float MAX_JUMP_RISE = ANTLION_JUMP_MAX_RISE;
	const float MAX_JUMP_DROP		= 256;
	const float MAX_JUMP_DISTANCE = ANTLION_JUMP_MAX;
	const float MIN_JUMP_DISTANCE = ANTLION_JUMP_MIN;

	if ( CAntlionRepellant::IsPositionRepellantFree( endPos ) == false )
		 return false;
	
	//Adrian: Don't try to jump if my destination is right next to me.
	if ( ( endPos - GetAbsOrigin()).Length() < MIN_JUMP_DISTANCE ) 
		 return false;

	if ( HasSpawnFlags( SF_ANTLION_USE_GROUNDCHECKS ) && g_test_new_antlion_jump.GetBool() == true )
	{
		trace_t	tr;
		AI_TraceHull( endPos, endPos, GetHullMins(), GetHullMaxs(), MASK_NPCSOLID, this, COLLISION_GROUP_NONE, &tr );
		
		if ( tr.m_pEnt )
		{
			CAI_BaseNPC *pBlocker = tr.m_pEnt->MyNPCPointer();

			if ( pBlocker && pBlocker->Classify() == CLASS_ALIEN_PREDATOR )
			{
				// HACKHACK
				CNPC_Antlion *pAntlion = dynamic_cast< CNPC_Antlion * > ( pBlocker );

				if ( pAntlion )
				{
					if ( pAntlion->AllowedToBePushed() == true )
					{
					//	NDebugOverlay::Line( GetAbsOrigin(), endPos, 255, 0, 0, 0, 2 );
					//	NDebugOverlay::Box( pAntlion->GetAbsOrigin(), GetHullMins(), GetHullMaxs(), 0, 0, 255, 0, 2 );
						pAntlion->GetMotor()->SetIdealYawToTarget( endPos );
						pAntlion->SetSchedule( SCHED_MOVE_AWAY );
						pAntlion->m_flNextJumpPushTime = gpGlobals->curtime + 2.0f;
					}
				}
			}
		}
	}

	return BaseClass::IsJumpLegal( startPos, apex, endPos, MAX_JUMP_RISE, MAX_JUMP_DROP, MAX_JUMP_DISTANCE );
}

bool CNPC_Antlion::IsFirmlyOnGround( void )
{
	if( !( GetFlags()&FL_ONGROUND ) )
		return false;

	trace_t tr;

	float flHeight =  fabs( GetHullMaxs().z - GetHullMins().z );
	
	Vector vOrigin = GetAbsOrigin() + Vector( GetHullMins().x, GetHullMins().y, 0 );
//	NDebugOverlay::Line( vOrigin, vOrigin - Vector( 0, 0, flHeight * 0.5  ), 255, 0, 0, true, 5 );
	UTIL_TraceLine( vOrigin, vOrigin - Vector( 0, 0, flHeight * 0.5  ), MASK_NPCSOLID, this, GetCollisionGroup(), &tr );

	if ( tr.fraction != 1.0f )
		 return true;
	
	vOrigin = GetAbsOrigin() - Vector( GetHullMins().x, GetHullMins().y, 0 );
//	NDebugOverlay::Line( vOrigin, vOrigin - Vector( 0, 0, flHeight * 0.5  ), 255, 0, 0, true, 5 );
	UTIL_TraceLine( vOrigin, vOrigin - Vector( 0, 0, flHeight * 0.5  ), MASK_NPCSOLID, this, GetCollisionGroup(), &tr );

	if ( tr.fraction != 1.0f )
		 return true;

	vOrigin = GetAbsOrigin() + Vector( GetHullMins().x, -GetHullMins().y, 0 );
//	NDebugOverlay::Line( vOrigin, vOrigin - Vector( 0, 0, flHeight * 0.5  ), 255, 0, 0, true, 5 );
	UTIL_TraceLine( vOrigin, vOrigin - Vector( 0, 0, flHeight * 0.5  ), MASK_NPCSOLID, this, GetCollisionGroup(), &tr );

	if ( tr.fraction != 1.0f )
		 return true;

	vOrigin = GetAbsOrigin() + Vector( -GetHullMins().x, GetHullMins().y, 0 );
//	NDebugOverlay::Line( vOrigin, vOrigin - Vector( 0, 0, flHeight * 0.5  ), 255, 0, 0, true, 5 );
	UTIL_TraceLine( vOrigin, vOrigin - Vector( 0, 0, flHeight * 0.5  ), MASK_NPCSOLID, this, GetCollisionGroup(), &tr );

	if ( tr.fraction != 1.0f )
		 return true;
	
	return false;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CNPC_Antlion::SelectFailSchedule( int failedSchedule, int failedTask, AI_TaskFailureCode_t taskFailCode )
{
	if ( m_FollowBehavior.GetNumFailedFollowAttempts() >= 2 )
	{
		if( IsFirmlyOnGround() == false )
		{
			Vector vecJumpDir; 
				
			vecJumpDir.z = 0;
			vecJumpDir.x = 0;
			vecJumpDir.y = 0;
			
			while( vecJumpDir.x == 0 && vecJumpDir.y == 0 )
			{
				vecJumpDir.x = random->RandomInt( -1, 1 ); 
				vecJumpDir.y = random->RandomInt( -1, 1 );
			}

			vecJumpDir.NormalizeInPlace();

			SetGroundEntity( NULL );
	
			m_vecSavedJump = vecJumpDir * 512 + Vector( 0, 0, 256 );
			m_bForcedStuckJump = true;
	
			return SCHED_ANTLION_JUMP;
		}
	}

	return BaseClass::SelectFailSchedule( failedSchedule, failedTask, taskFailCode );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::ShouldJump( void )
{
	if ( GetEnemy() == NULL )
		return false;

	//Too soon to try to jump
	if ( m_flJumpTime > gpGlobals->curtime )
		return false;

	// only jump if you're on the ground
  	if (!(GetFlags() & FL_ONGROUND) || GetNavType() == NAV_JUMP )
		return false;

	// Don't jump if I'm not allowed
	if ( ( CapabilitiesGet() & bits_CAP_MOVE_JUMP ) == false )
		return false;

	Vector vEnemyForward, vForward;

	GetEnemy()->GetVectors( &vEnemyForward, NULL, NULL );
	GetVectors( &vForward, NULL, NULL );

	float flDot = DotProduct( vForward, vEnemyForward );

	if ( flDot < 0.5f )
		 flDot = 0.5f;

	Vector vecPredictedPos;

	//Get our likely position in two seconds
	UTIL_PredictedPosition( GetEnemy(), flDot * 2.5f, &vecPredictedPos );

	// Don't jump if we're already near the target
	if ( ( GetAbsOrigin() - vecPredictedPos ).LengthSqr() < (512*512) )
		return false;

	//Don't retest if the target hasn't moved enough
	//FIXME: Check your own distance from last attempt as well
	if ( ( ( m_vecLastJumpAttempt - vecPredictedPos ).LengthSqr() ) < (128*128) )
	{
		m_flJumpTime = gpGlobals->curtime + random->RandomFloat( 1.0f, 2.0f );		
		return false;
	}

	Vector	targetDir = ( vecPredictedPos - GetAbsOrigin() );

	float flDist = VectorNormalize( targetDir );

	// don't jump at target it it's very close
	if (flDist < ANTLION_JUMP_MIN)
		return false;

	Vector	targetPos = vecPredictedPos + ( targetDir * (GetHullWidth()*4.0f) );

	if ( CAntlionRepellant::IsPositionRepellantFree( targetPos ) == false )
		 return false;

	// Try the jump
	AIMoveTrace_t moveTrace;
	GetMoveProbe()->MoveLimit( NAV_JUMP, GetAbsOrigin(), targetPos, MASK_NPCSOLID, GetNavTargetEntity(), &moveTrace );

	//See if it succeeded
	if ( IsMoveBlocked( moveTrace.fStatus ) )
	{
		if ( g_debug_antlion.GetInt() == 2 )
		{
			NDebugOverlay::Box( targetPos, GetHullMins(), GetHullMaxs(), 255, 0, 0, 0, 5 );
			NDebugOverlay::Line( GetAbsOrigin(), targetPos, 255, 0, 0, 0, 5 );
		}

		m_flJumpTime = gpGlobals->curtime + random->RandomFloat( 1.0f, 2.0f );
		return false;
	}

	if ( g_debug_antlion.GetInt() == 2 )
	{
		NDebugOverlay::Box( targetPos, GetHullMins(), GetHullMaxs(), 0, 255, 0, 0, 5 );
		NDebugOverlay::Line( GetAbsOrigin(), targetPos, 0, 255, 0, 0, 5 );
	}

	//Save this jump in case the next time fails
	m_vecSavedJump = moveTrace.vJumpVelocity;
	m_vecLastJumpAttempt = targetPos;

	return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int CNPC_Antlion::TranslateSchedule( int scheduleType )
{
	return BaseClass::TranslateSchedule( scheduleType );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
Activity CNPC_Antlion::NPC_TranslateActivity( Activity baseAct )
{
	return BaseClass::NPC_TranslateActivity( baseAct );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : int
//-----------------------------------------------------------------------------
int CNPC_Antlion::ChooseMoveSchedule( void )
{
	// See if we need to invalidate our fight goal
	if ( ShouldResumeFollow() )
	{
		// Set us back to following
		SetMoveState( ANTLION_MOVE_FOLLOW );
	}

	// Figure out our move state
	switch( m_MoveState )
	{
	case ANTLION_MOVE_FREE:
		return SCHED_NONE;	// Let the base class handle us
		break;

	// Fighting to a position
	case ANTLION_MOVE_FIGHT_TO_GOAL:
		{
			if ( m_hFightGoalTarget )
			{
				float targetDist = UTIL_DistApprox( WorldSpaceCenter(), m_hFightGoalTarget->GetAbsOrigin() );

				if ( targetDist > 256 )
				{
					Vector testPos;
					Vector targetPos = ( m_hFightGoalTarget ) ? m_hFightGoalTarget->GetAbsOrigin() : m_vSavePosition;

					// Find a suitable chase position
					if ( FindChasePosition( targetPos, testPos ) )
					{
						m_vSavePosition = testPos;
						return SCHED_ANTLION_RUN_TO_FIGHT_GOAL;
					}
				}
			}
		}
		break;

	// Following a goal
	case ANTLION_MOVE_FOLLOW:
		{
			if ( m_FollowBehavior.CanSelectSchedule() )
			{
				DeferSchedulingToBehavior( &m_FollowBehavior );
				return BaseClass::SelectSchedule();
			}
		}
		break;
	}

	return SCHED_NONE;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::ZapThink( void )
{
	CEffectData	data;
	data.m_nEntIndex = entindex();
	data.m_flMagnitude = 4;
	data.m_flScale = random->RandomFloat( 0.25f, 1.0f );

	DispatchEffect( "TeslaHitboxes", data );
	
	if ( m_flZapDuration > gpGlobals->curtime )
	{
		SetContextThink( &CNPC_Antlion::ZapThink, gpGlobals->curtime + random->RandomFloat( 0.05f, 0.25f ), "ZapThink" );
	}
	else
	{
		SetContextThink( NULL, gpGlobals->curtime, "ZapThink" );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : int
//-----------------------------------------------------------------------------
int CNPC_Antlion::SelectSchedule( void )
{
	// Clear out this condition
	ClearCondition( COND_ANTLION_RECEIVED_ORDERS );

	// See if a friendly player is pushing us away
	if ( HasCondition( COND_PLAYER_PUSHING ) )
		return SCHED_MOVE_AWAY;

	if( HasCondition( COND_ANTLION_IN_WATER ) )
	{
		// No matter what, drown in water
		return SCHED_ANTLION_DROWN;
	}

	//Hear a physics danger sound?
	if( HasCondition( COND_HEAR_PHYSICS_DANGER ) )
	{
		CTakeDamageInfo info;
		PainSound( info );
		return SCHED_ANTLION_FLEE_PHYSICS_DANGER;
	}

	//On another NPC's head?
	if( HasCondition( COND_ANTLION_ON_NPC ) )
	{
		// You're on an NPC's head. Get off.
		return SCHED_ANTLION_DISMOUNT_NPC;
	}

	// If we're scripted to jump at a target, do so
	if ( HasCondition( COND_ANTLION_CAN_JUMP_AT_TARGET ) )
	{
		// NDebugOverlay::Cross3D( m_vecSavedJump, 32.0f, 255, 0, 0, true, 2.0f );
		ClearCondition( COND_ANTLION_CAN_JUMP_AT_TARGET );
		return SCHED_ANTLION_JUMP;
	}

	if( m_AssaultBehavior.CanSelectSchedule() )
	{
		DeferSchedulingToBehavior( &m_AssaultBehavior );
		return BaseClass::SelectSchedule();
	}

	//Otherwise do basic state schedule selection
	switch ( m_NPCState )
	{	
	case NPC_STATE_COMBAT:
	{
		// Lunge at the enemy
		if (HasCondition(COND_CAN_MELEE_ATTACK2))
		{
			m_flPounceTime = gpGlobals->curtime + 1.5f;

			if (m_bLeapAttack == true)
				return SCHED_ANTLION_POUNCE_MOVING;
			else
				return SCHED_ANTLION_POUNCE;
		}

		// Try to jump
		if (HasCondition(COND_ANTLION_CAN_JUMP))
			return SCHED_ANTLION_JUMP;
	}
		break;

	default:
		{
			int	moveSched = ChooseMoveSchedule();

			if ( moveSched != SCHED_NONE )
				return moveSched;

			if ( GetEnemy() == NULL && ( HasCondition( COND_LIGHT_DAMAGE ) || HasCondition( COND_HEAVY_DAMAGE ) ) )
			{
				Vector vecEnemyLKP;

				// Retrieve a memory for the damage taken
				// Fill in where we're trying to look
				if ( GetEnemies()->Find( AI_UNKNOWN_ENEMY ) )
				{
					vecEnemyLKP = GetEnemies()->LastKnownPosition( AI_UNKNOWN_ENEMY );
				}
				else
				{
					// Don't have an enemy, so face the direction the last attack came from (don't face north)
					vecEnemyLKP = WorldSpaceCenter() + ( g_vecAttackDir * 128 );
				}
				
				// If we're already facing the attack direction, then take cover from it
				if ( FInViewCone( vecEnemyLKP ) )
				{
					// Save this position for our cover search
					m_vSavePosition = vecEnemyLKP;
					return SCHED_ANTLION_TAKE_COVER_FROM_SAVEPOSITION;
				}
				
				// By default, we'll turn to face the attack
			}
		}
		break;
	}

	return BaseClass::SelectSchedule();
}

void CNPC_Antlion::Ignite ( float flFlameLifetime, bool bNPCOnly, float flSize, bool bCalledByLevelDesigner )
{
#ifdef HL2_EPISODIC
	float flDamage = m_iHealth + 1;

	CTakeDamageInfo	dmgInfo( this, this, flDamage, DMG_GENERIC );
	GuessDamageForce( &dmgInfo, Vector( 0, 0, 8 ), GetAbsOrigin() );
	TakeDamage( dmgInfo );
#else
	BaseClass::Ignite( flFlameLifetime, bNPCOnly, flSize, bCalledByLevelDesigner );
#endif

}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int CNPC_Antlion::OnTakeDamage_Alive( const CTakeDamageInfo &info )
{
	CTakeDamageInfo newInfo = info;

	if( hl2_episodic.GetBool() && antlion_easycrush.GetBool() )
	{
		if( newInfo.GetDamageType() & DMG_CRUSH )
		{
			if( newInfo.GetInflictor() && newInfo.GetInflictor()->VPhysicsGetObject() )
			{
				float flMass = newInfo.GetInflictor()->VPhysicsGetObject()->GetMass();

				if( flMass > 250.0f && newInfo.GetDamage() < GetHealth() )
				{
					newInfo.SetDamage( GetHealth() );
				}
			}
		}
	}

	// Find out how much damage we're about to take
	int nDamageTaken = BaseClass::OnTakeDamage_Alive( newInfo );
	if ( gpGlobals->curtime - m_flLastDamageTime < 0.5f )
	{
		// Accumulate it
		m_nSustainedDamage += nDamageTaken;
	}
	else
	{
		// Reset, it's been too long
		m_nSustainedDamage = nDamageTaken;
	}

	m_flLastDamageTime = gpGlobals->curtime;

	return nDamageTaken;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::TraceAttack( const CTakeDamageInfo &info, const Vector &vecDir, trace_t *ptr, CDmgAccumulator *pAccumulator )
{
	CTakeDamageInfo newInfo = info;

	Vector	vecShoveDir = vecDir;
	vecShoveDir.z = 0.0f;

	BaseClass::TraceAttack( newInfo, vecDir, ptr, pAccumulator );
}

void CNPC_Antlion::StopLoopingSounds( void )
{
	return;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::IdleSound( void )
{
	EmitSound( "NPC_Antlion.Idle" );
	m_flIdleDelay = gpGlobals->curtime + 4.0f;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::PainSound( const CTakeDamageInfo &info )
{
	EmitSound( "NPC_Antlion.Pain" );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : 
//-----------------------------------------------------------------------------
float CNPC_Antlion::GetIdealAccel( void ) const
{
	return GetIdealSpeed() * 2.0;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : float
//-----------------------------------------------------------------------------
float CNPC_Antlion::MaxYawSpeed( void )
{
	switch ( GetActivity() )
	{
	case ACT_IDLE:		
		return 32.0f;
		break;
	
	case ACT_WALK:
		return 16.0f;
		break;
	
	default:
	case ACT_RUN:
		return 32.0f;
		break;
	}

	return 32.0f;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::ShouldPlayIdleSound( void )
{
	//Only do idles in the right states
	if ( ( m_NPCState != NPC_STATE_IDLE && m_NPCState != NPC_STATE_ALERT ) )
		return false;

	//Gagged monsters don't talk
	if ( m_spawnflags & SF_NPC_GAG )
		return false;

	//Don't cut off another sound or play again too soon
	if ( m_flIdleDelay > gpGlobals->curtime )
		return false;

	//Randomize it a bit
	if ( random->RandomInt( 0, 20 ) )
		return false;

	return true;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pFriend - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::NotifyDeadFriend( CBaseEntity *pFriend )
{
	SetCondition( COND_ANTLION_SQUADMATE_KILLED );
	BaseClass::NotifyDeadFriend( pFriend );
}


//-----------------------------------------------------------------------------
// Purpose: Determine whether or not to check our attack conditions
//-----------------------------------------------------------------------------
bool CNPC_Antlion::FCanCheckAttacks( void )
{
	return BaseClass::FCanCheckAttacks();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CNPC_Antlion::RangeAttack1Conditions( float flDot, float flDist )
{
	if ( GetNextAttack() > gpGlobals->curtime )
		return COND_NOT_FACING_ATTACK;

	if ( flDot < DOT_10DEGREE )
		return COND_NOT_FACING_ATTACK;
	
	if ( flDist > (150*12) )
		return COND_TOO_FAR_TO_ATTACK;

	if ( flDist < (20*12) )
		return COND_TOO_CLOSE_TO_ATTACK;

	return COND_CAN_RANGE_ATTACK1;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CNPC_Antlion::MeleeAttack1Conditions( float flDot, float flDist )
{
#if 1 //NOTENOTE: Use predicted position melee attacks

	//Get our likely position in one half second
	Vector vecPrPos;
	UTIL_PredictedPosition( GetEnemy(), 0.5f, &vecPrPos );

	//Get the predicted distance and direction
	float flPrDist = ( vecPrPos - GetAbsOrigin() ).LengthSqr();
	if ( flPrDist > Square( ANTLION_MELEE1_RANGE ) )
		return COND_TOO_FAR_TO_ATTACK;

	// Compare our target direction to our body facing
	Vector2D vec2DPrDir	= ( vecPrPos - GetAbsOrigin() ).AsVector2D();
	Vector2D vec2DBodyDir = BodyDirection2D().AsVector2D();
	
	float flPrDot = DotProduct2D ( vec2DPrDir, vec2DBodyDir );
	if ( flPrDot < 0.5f )
		return COND_NOT_FACING_ATTACK;

	trace_t	tr;
	AI_TraceHull( WorldSpaceCenter(), GetEnemy()->WorldSpaceCenter(), -Vector(8,8,8), Vector(8,8,8), MASK_NPCSOLID, this, COLLISION_GROUP_NONE, &tr );

	// If the hit entity isn't our target and we don't hate it, don't hit it
	if ( tr.m_pEnt != GetEnemy() && tr.fraction < 1.0f && IRelationType( tr.m_pEnt ) != D_HT )
		return 0;

#else

	if ( flDot < 0.5f )
		return COND_NOT_FACING_ATTACK;

	float flAdjustedDist = ANTLION_MELEE1_RANGE;

	if ( GetEnemy() )
	{
		// Give us extra space if our enemy is in a vehicle
		CBaseCombatCharacter *pCCEnemy = GetEnemy()->MyCombatCharacterPointer();
		if ( pCCEnemy != NULL && pCCEnemy->IsInAVehicle() )
		{
			flAdjustedDist *= 2.0f;
		}
	}

	if ( flDist > flAdjustedDist )
		return COND_TOO_FAR_TO_ATTACK;

	trace_t	tr;
	AI_TraceHull( WorldSpaceCenter(), GetEnemy()->WorldSpaceCenter(), -Vector(8,8,8), Vector(8,8,8), MASK_SOLID_BRUSHONLY, this, COLLISION_GROUP_NONE, &tr );

	if ( tr.fraction < 1.0f )
		return 0;

#endif

	return COND_CAN_MELEE_ATTACK1;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : flDot - 
//			flDist - 
// Output : int
//-----------------------------------------------------------------------------
int CNPC_Antlion::MeleeAttack2Conditions( float flDot, float flDist )
{
	Msg("Check for pounce\n");
	// See if it's too soon to pounce again
	if ( m_flPounceTime > gpGlobals->curtime )
		return 0;

	float		flPrDist, flPrDot;
	Vector		vecPrPos;
	Vector2D	vec2DPrDir;

	//Get our likely position in one half second
	UTIL_PredictedPosition( GetEnemy(), 0.25f, &vecPrPos );

	//Get the predicted distance and direction
	flPrDist	= ( vecPrPos - GetAbsOrigin() ).Length();
	vec2DPrDir	= ( vecPrPos - GetAbsOrigin() ).AsVector2D();

	Vector vecBodyDir = BodyDirection2D();

	Vector2D vec2DBodyDir = vecBodyDir.AsVector2D();
	
	flPrDot	= DotProduct2D ( vec2DPrDir, vec2DBodyDir );

	if ( ( flPrDist > ANTLION_MELEE2_RANGE_MAX ) )
	{
		m_flPounceTime = gpGlobals->curtime + 0.2f;
		return COND_TOO_FAR_TO_ATTACK;
	}
	else if ( ( flPrDist < ANTLION_MELEE2_RANGE_MIN ) )
	{
		m_flPounceTime = gpGlobals->curtime + 0.2f;
		return COND_TOO_CLOSE_TO_ATTACK;
	}

	trace_t	tr;
	AI_TraceHull( WorldSpaceCenter(), GetEnemy()->WorldSpaceCenter(), -Vector(8,8,8), Vector(8,8,8), MASK_SOLID_BRUSHONLY, this, COLLISION_GROUP_NONE, &tr );

	if ( tr.fraction < 1.0f )
		return 0;

	if ( IsMoving() )
		 m_bLeapAttack = true;
	else
		 m_bLeapAttack = false;

	Msg("Check for pounce succeeded\n");
	return COND_CAN_MELEE_ATTACK2;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : interactionType - 
//			*data - 
//			*sender - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::HandleInteraction( int interactionType, void *data, CBaseCombatCharacter *sender )
{
	return false;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::Alone( void )
{
	if ( m_pSquad == NULL )
		return true;

	if ( m_pSquad->NumMembers() <= 1 )
		return true;

	return false;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::StartJump( void )
{
	if ( m_bForcedStuckJump == false )
	{
		// FIXME: Why must this be true?
		// Must be jumping at an enemy
		// if ( GetEnemy() == NULL )
		//	return;

		//Don't jump if we're not on the ground
		if ( ( GetFlags() & FL_ONGROUND ) == false )
			return;
	}

	//Take us off the ground
	SetGroundEntity( NULL );
	SetAbsVelocity( m_vecSavedJump );

	m_bForcedStuckJump = false;
#if HL2_EPISODIC
	m_bHasDoneAirAttack = false;
#endif

	//Setup our jump time so that we don't try it again too soon
	m_flJumpTime = gpGlobals->curtime + random->RandomInt( 2, 6 );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : sHint - 
//			nNodeNum - 
// Output : bool CAI_BaseNPC::FValidateHintType
//-----------------------------------------------------------------------------
bool CNPC_Antlion::FValidateHintType( CAI_Hint *pHint )
{
	return true;
}

bool NPC_CheckBrushExclude( CBaseEntity *pEntity, CBaseEntity *pBrush );
//-----------------------------------------------------------------------------
// traceline methods
//-----------------------------------------------------------------------------
class CTraceFilterSimpleNPCExclude : public CTraceFilterSimple
{
public:
	DECLARE_CLASS( CTraceFilterSimpleNPCExclude, CTraceFilterSimple );

	CTraceFilterSimpleNPCExclude( const IHandleEntity *passentity, int collisionGroup )
		: CTraceFilterSimple( passentity, collisionGroup )
	{
	}

	bool ShouldHitEntity( IHandleEntity *pHandleEntity, int contentsMask )
	{
		Assert( dynamic_cast<CBaseEntity*>(pHandleEntity) );
		CBaseEntity *pTestEntity = static_cast<CBaseEntity*>(pHandleEntity);

		if ( GetPassEntity() )
		{
			CBaseEntity *pEnt = gEntList.GetBaseEntity( GetPassEntity()->GetRefEHandle() );

			if ( pEnt->IsNPC() )
			{
				if ( NPC_CheckBrushExclude( pEnt, pTestEntity ) == true )
					return false;
			}
		}
		return BaseClass::ShouldHitEntity( pHandleEntity, contentsMask );
	}
};

//-----------------------------------------------------------------------------
// Purpose: Monitor the antlion's jump to play the proper landing sequence
//-----------------------------------------------------------------------------
bool CNPC_Antlion::CheckLanding( void )
{
	trace_t	tr;
	Vector	testPos;

	//Amount of time to predict forward
	const float	timeStep = 0.1f;

	//Roughly looks one second into the future
	testPos = GetAbsOrigin() + ( GetAbsVelocity() * timeStep );
	testPos[2] -= ( 0.5 * GetCurrentGravity() * GetGravity() * timeStep * timeStep);

	if ( g_debug_antlion.GetInt() == 2 )
	{
		NDebugOverlay::Line( GetAbsOrigin(), testPos, 255, 0, 0, 0, 0.5f );
		NDebugOverlay::Cross3D( m_vecSavedJump, -Vector(2,2,2), Vector(2,2,2), 0, 255, 0, true, 0.5f );
	} 
	
	// Look below
	AI_TraceHull( GetAbsOrigin(), testPos, NAI_Hull::Mins( GetHullType() ), NAI_Hull::Maxs( GetHullType() ), MASK_NPCSOLID, this, COLLISION_GROUP_NONE, &tr );

	//See if we're about to contact, or have already contacted the ground
	if ( ( tr.fraction != 1.0f ) || ( GetFlags() & FL_ONGROUND ) )
	{
		int	sequence = SelectWeightedSequence( (Activity)ACT_ANTLION_LAND );

		if ( GetSequence() != sequence )
		{
			VacateStrategySlot();
			SetIdealActivity( (Activity) ACT_ANTLION_LAND );

			CreateDust( false );
			EmitSound( "NPC_Antlion.Land" );

			if ( GetEnemy() && GetEnemy()->IsPlayer()  )
			{
				CBasePlayer *pPlayer = ToBasePlayer( GetEnemy() );

				if ( pPlayer && pPlayer->IsInAVehicle() == false )
				{
					QAngle qa( 4.0f, 0.0f, 0.0f );
					Vector vec( -250.0f, 1.0f, 1.0f );
					MeleeAttack( ANTLION_MELEE1_RANGE, sk_panthereye_swipe_damage.GetFloat(), qa, vec );
				}
			}

			SetAbsVelocity( GetAbsVelocity() * 0.33f );
			return false;
		}

		return IsActivityFinished();
	}

	return false;
}



//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pEntity - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::QuerySeeEntity( CBaseEntity *pEntity, bool bOnlyHateOrFearIfNPC )
{
	//If we're under the ground, don't look at enemies
	if ( IsEffectActive( EF_NODRAW ) )
		return false;

	return BaseClass::QuerySeeEntity(pEntity, bOnlyHateOrFearIfNPC);
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::CreateDust( bool placeDecal )
{
	trace_t	tr;
	AI_TraceLine( GetAbsOrigin()+Vector(0,0,1), GetAbsOrigin()-Vector(0,0,64), MASK_SOLID_BRUSHONLY | CONTENTS_PLAYERCLIP | CONTENTS_MONSTERCLIP, this, COLLISION_GROUP_NONE, &tr );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pSound - 
//-----------------------------------------------------------------------------
bool CNPC_Antlion::QueryHearSound( CSound *pSound )
{
	if ( !BaseClass::QueryHearSound( pSound ) )
		return false;
		
	//Do the normal behavior at this point
	return true;
}

//-----------------------------------------------------------------------------
// Purpose: Allows for modification of the interrupt mask for the current schedule.
//			In the most cases the base implementation should be called first.
//-----------------------------------------------------------------------------
void CNPC_Antlion::BuildScheduleTestBits( void )
{
	//Don't allow any modifications when scripted
	if ( m_NPCState == NPC_STATE_SCRIPT )
		return;

	// If we're allied with the player, don't be startled by him
	if ( IsAllied() )
	{
		ClearCustomInterruptCondition( COND_HEAR_PLAYER );
		SetCustomInterruptCondition( COND_PLAYER_PUSHING );
	}

	//Make sure we interrupt a run schedule if we can jump
	if ( IsCurSchedule(SCHED_CHASE_ENEMY) )
	{
		SetCustomInterruptCondition( COND_ANTLION_CAN_JUMP );
		SetCustomInterruptCondition( COND_ENEMY_UNREACHABLE );
	}

	if ( !IsCurSchedule( SCHED_ANTLION_DROWN ) )
	{
		// Interrupt any schedule unless already drowning.
		SetCustomInterruptCondition( COND_ANTLION_IN_WATER );
	}
	else
	{
		// Don't stop drowning just because you're in water!
		ClearCustomInterruptCondition( COND_ANTLION_IN_WATER );
	}

	// Make sure we don't stop in midair
	/*
	if ( GetActivity() == ACT_JUMP || GetActivity() == ACT_GLIDE || GetActivity() == ACT_LAND )
	{
		ClearCustomInterruptCondition( COND_NEW_ENEMY );
	}
	*/
	
	//Interrupt any schedule unless already fleeing.
	if( !IsCurSchedule(SCHED_ANTLION_FLEE_THUMPER)			&& 		
		!IsCurSchedule(SCHED_ANTLION_FLEE_PHYSICS_DANGER)	&& 		
		!IsCurSchedule(SCHED_ANTLION_JUMP)					&&
		!IsCurSchedule(SCHED_ANTLION_DISMOUNT_NPC)			&& 
		( GetFlags() & FL_ONGROUND ) )
	{
		// Only do these if not jumping as well
		if (!IsCurSchedule(SCHED_ANTLION_JUMP))
		{
			if ( GetEnemy() == NULL )
			{
				SetCustomInterruptCondition( COND_HEAR_PHYSICS_DANGER );
			}
			
			SetCustomInterruptCondition( COND_ANTLION_CAN_JUMP_AT_TARGET );

			if ( GetNavType() != NAV_JUMP )
				 SetCustomInterruptCondition( COND_ANTLION_RECEIVED_ORDERS );
		}

		SetCustomInterruptCondition( COND_ANTLION_ON_NPC );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pEnemy - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::IsValidEnemy( CBaseEntity *pEnemy )
{
	//See if antlions are friendly to the player in this map
	if ( IsAllied() && pEnemy->IsPlayer() )
		return false;

	if ( pEnemy->IsWorld() )
		return false;

	// If we're following an entity we limit our attack distances
	if ( m_FollowBehavior.GetFollowTarget() != NULL )
	{
		float enemyDist = ( pEnemy->GetAbsOrigin() - GetAbsOrigin() ).LengthSqr();

		if ( m_flObeyFollowTime > gpGlobals->curtime )
		{
			// Unless we're right next to the enemy, follow our target
			if ( enemyDist > (128*128) )
				return false;
		}
		else
		{
			// Otherwise don't follow if the target is far 
			if ( enemyDist > (2000*2000) )
				return false;
		}
	}

	return BaseClass::IsValidEnemy( pEnemy );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::GatherConditions( void )
{
	BaseClass::GatherConditions();

	// See if I've landed on an NPC!
	CBaseEntity *pGroundEnt = GetGroundEntity();
	
	if ( ( ( pGroundEnt != NULL ) && ( pGroundEnt->GetSolidFlags() & FSOLID_NOT_STANDABLE ) ) && ( GetFlags() & FL_ONGROUND ) && ( !IsEffectActive( EF_NODRAW ) && !pGroundEnt->IsEffectActive( EF_NODRAW ) ) )
	{
		SetCondition( COND_ANTLION_ON_NPC );
	}
	else
	{
		ClearCondition( COND_ANTLION_ON_NPC );
	}

	// See if our follow target is too far off
/*	if ( m_hFollowTarget != NULL )
	{
		float targetDist = UTIL_DistApprox( WorldSpaceCenter(), m_hFollowTarget->GetAbsOrigin() );

		if ( targetDist > 400 )
		{
			SetCondition( COND_ANTLION_FOLLOW_TARGET_TOO_FAR );
		}
		else
		{
			ClearCondition( COND_ANTLION_FOLLOW_TARGET_TOO_FAR );
		}
	}*/

	if ( IsCurSchedule(SCHED_FALL_TO_GROUND ) == false &&
		 IsEffectActive( EF_NODRAW ) == false )
	{
		if( m_lifeState == LIFE_ALIVE && GetWaterLevel() > 1 )
		{
			// Start Drowning!
			SetCondition( COND_ANTLION_IN_WATER );
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::PrescheduleThink( void )
{
	UpdateHead();

	//New Enemy? Try to jump at him.
	if ( HasCondition( COND_NEW_ENEMY ) )
	{
		m_flJumpTime = 0.0f;
	}

	// See if we should jump because of desirables conditions, or a scripted request
	if ( ShouldJump() )
	{
		SetCondition( COND_ANTLION_CAN_JUMP );
	}
	else
	{
		ClearCondition( COND_ANTLION_CAN_JUMP );
	}

	BaseClass::PrescheduleThink();
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : flDamage - 
//			bitsDamageType - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::IsLightDamage( const CTakeDamageInfo &info )
{
	if ( ( random->RandomInt( 0, 1 ) ) && ( info.GetDamage() > 3 ) )
		return true;

	return false;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::IsAllied( void )
{
	return ( GlobalEntity_GetState( "antlion_allied" ) == GLOBAL_ON );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::ShouldResumeFollow( void )
{
	if ( IsAllied() == false )
		return false;

	if ( m_MoveState == ANTLION_MOVE_FOLLOW || m_hFollowTarget == NULL )
		return false;

	if ( m_flSuppressFollowTime > gpGlobals->curtime )
		return false;

	if ( GetEnemy() != NULL )
	{
		m_flSuppressFollowTime = gpGlobals->curtime + random->RandomInt( 5, 10 );
		return false;
	}

	//TODO: See if the follow target has wandered off too far from where we last followed them to
	
	return true;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::ShouldAbandonFollow( void )
{
	// Never give up if we can see the goal
	if ( m_FollowBehavior.FollowTargetVisible() )
		return false;

	// Never give up if we're too close
	float flDistance = UTIL_DistApprox2D( m_FollowBehavior.GetFollowTarget()->WorldSpaceCenter(), WorldSpaceCenter() );

	if ( flDistance < 1500 )
		return false;

	if ( flDistance > 1500 * 2.0f )
		return true;

	// If we've failed too many times, give up
	if ( m_FollowBehavior.GetNumFailedFollowAttempts() )
		return true;

	// If the target simply isn't reachable to us, give up
	if ( m_FollowBehavior.TargetIsUnreachable() )
		return true;

	return false;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pTarget - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::SetFightTarget( CBaseEntity *pTarget )
{
	m_hFightGoalTarget = pTarget;

	SetCondition( COND_ANTLION_RECEIVED_ORDERS );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &inputdata - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::InputFightToPosition( inputdata_t &inputdata )
{
	if ( IsAlive() == false )
		return;

	CBaseEntity *pEntity = gEntList.FindEntityByName( NULL, inputdata.value.String(), NULL, inputdata.pActivator, inputdata.pCaller );

	if ( pEntity != NULL )
	{
		SetFightTarget( pEntity );
		SetFollowTarget( NULL );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &inputdata - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::InputStopFightToPosition( inputdata_t &inputdata )
{
	SetFightTarget( NULL );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pEnemy - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::GatherEnemyConditions( CBaseEntity *pEnemy )
{
	// Do the base class
	BaseClass::GatherEnemyConditions( pEnemy );

	// If we're not already too far away, check again
	//TODO: Check to make sure we don't already have a condition set that removes the need for this
	if ( HasCondition( COND_ENEMY_UNREACHABLE ) == false )
	{
		Vector	predPosition;
		UTIL_PredictedPosition( GetEnemy(), 1.0f, &predPosition );

		Vector	predDir = ( predPosition - GetAbsOrigin() );
		float	predLength = VectorNormalize( predDir );

		// See if we'll be outside our effective target range
		if ( predLength > m_flEludeDistance )
		{
			Vector	predVelDir = ( predPosition - GetEnemy()->GetAbsOrigin() );
			float	predSpeed  = VectorNormalize( predVelDir );

			// See if the enemy is moving mostly away from us
			if ( ( predSpeed > 512.0f ) && ( DotProduct( predVelDir, predDir ) > 0.0f ) )
			{
				// Mark the enemy as eluded
				ClearEnemyMemory();
				SetEnemy( NULL );
				SetIdealState( NPC_STATE_ALERT );
				SetCondition( COND_ENEMY_UNREACHABLE );
			}
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &info - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::ShouldGib( const CTakeDamageInfo &info )
{
	if ( info.GetDamageType() & (DMG_NEVERGIB|DMG_DISSOLVE) )
		return false;

	if ( info.GetDamageType() & (DMG_ALWAYSGIB|DMG_BLAST) )
		return true;

	if ( m_iHealth < -20 )
		return true;
	
	return false;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::CorpseGib(const CTakeDamageInfo &info)
{
	// Use the bone position to handle being moved by an animation (like a dynamic scripted sequence)
	static int s_nBodyBone = -1;
	if (s_nBodyBone == -1)
	{
		s_nBodyBone = LookupBone("Antlion.Body_Bone");
	}

	Vector vecOrigin;
	QAngle angBone;
	GetBonePosition(s_nBodyBone, vecOrigin, angBone);

	DispatchParticleEffect("AntlionGib", vecOrigin, QAngle(0, 0, 0));

	Vector velocity = vec3_origin;
	AngularImpulse	angVelocity = RandomAngularImpulse(-150, 150);
	breakablepropparams_t params(EyePosition(), GetAbsAngles(), velocity, angVelocity);
	params.impactEnergyScale = 1.0f;
	params.defBurstScale = 150.0f;
	params.defCollisionGroup = COLLISION_GROUP_DEBRIS;
	PropBreakableCreateAll(GetModelIndex(), NULL, params, this, -1, true, true);

	return true;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pOther - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::Touch( CBaseEntity *pOther )
{
	//See if the touching entity is a vehicle
	CBasePlayer *pPlayer = ToBasePlayer( AI_GetSinglePlayer() );
	
	// FIXME: Technically we'll want to check to see if a vehicle has touched us with the player OR NPC driver

	if ( pPlayer && pPlayer->IsInAVehicle() )
	{
		IServerVehicle	*pVehicle = pPlayer->GetVehicle();
		CBaseEntity *pVehicleEnt = pVehicle->GetVehicleEnt();

		if ( pVehicleEnt == pOther )
		{
			CPropVehicleDriveable	*pDrivableVehicle = dynamic_cast<CPropVehicleDriveable *>( pVehicleEnt );

			if ( pDrivableVehicle != NULL )
			{
				//Get tossed!
				Vector	vecShoveDir = pOther->GetAbsVelocity();
				Vector	vecTargetDir = GetAbsOrigin() - pOther->GetAbsOrigin();
				
				VectorNormalize( vecShoveDir );
				VectorNormalize( vecTargetDir );

				if (((pDrivableVehicle->m_nRPM > 75) && DotProduct(vecShoveDir, vecTargetDir) <= 0))
				{
					// We're being shoved
					CTakeDamageInfo	dmgInfo(pVehicleEnt, pPlayer, 0, DMG_VEHICLE);
					PainSound(dmgInfo);

					vecTargetDir[2] = 0.0f;

					ApplyAbsVelocityImpulse((vecTargetDir * 250.0f) + Vector(0, 0, 64.0f));
					SetGroundEntity(NULL);

					CSoundEnt::InsertSound(SOUND_PHYSICS_DANGER, GetAbsOrigin(), 256, 0.5f, this);
				}
			}
		}
	}

	BaseClass::Touch( pOther );

	// in episodic, an antlion colliding with the player in midair does him damage.
	// pursuant bugs 58590, 56960, this happens only once per glide.
#ifdef HL2_EPISODIC 
	if ( GetActivity() == ACT_GLIDE && IsValidEnemy( pOther ) && !m_bHasDoneAirAttack )
	{
		CTakeDamageInfo	dmgInfo( this, this, sk_panthereye_air_attack_dmg.GetInt(), DMG_SLASH );

		CalculateMeleeDamageForce( &dmgInfo, Vector( 0, 0, 1 ), GetAbsOrigin() );
		pOther->TakeDamage( dmgInfo );

		//Kick the player angles
		bool bIsPlayer = pOther->IsPlayer();
		if ( bIsPlayer && !(pOther->GetFlags() & FL_GODMODE ) && pOther->GetMoveType() != MOVETYPE_NOCLIP )
		{
			pOther->ViewPunch( QAngle( 4.0f, 0.0f, 0.0f ) );
		}

		// set my "I have already attacked someone" flag
		if ( bIsPlayer || pOther->IsNPC())
		{
			m_bHasDoneAirAttack = true;
		}
	}
#endif

	// Did the player touch me?
	if ( pOther->IsPlayer() )
	{
		// Don't test for this if the pusher isn't friendly
		if ( IsValidEnemy( pOther ) )
			return;

		// Ignore if pissed at player
		if ( m_afMemory & bits_MEMORY_PROVOKED )
			return;
	
		if ( !IsCurSchedule( SCHED_MOVE_AWAY ))
			 TestPlayerPushing( pOther );
	}
}

//-----------------------------------------------------------------------------
// Purpose: turn in the direction of movement
// Output :
//-----------------------------------------------------------------------------
bool CNPC_Antlion::OverrideMoveFacing( const AILocalMoveGoal_t &move, float flInterval )
{
	//Adrian: Make antlions face the thumper while they flee away.
	if ( IsCurSchedule( SCHED_ANTLION_FLEE_THUMPER ) )
	{
		CSound *pSound = GetLoudestSoundOfType( SOUND_THUMPER );

		if ( pSound )
		{
			AddFacingTarget( pSound->GetSoundOrigin(), 1.0, 0.5f );
		}
	}
	else if ( GetEnemy() && GetNavigator()->GetMovementActivity() == ACT_RUN )
  	{
		// FIXME: this will break scripted sequences that walk when they have an enemy
		Vector vecEnemyLKP = GetEnemyLKP();
		if ( UTIL_DistApprox( vecEnemyLKP, GetAbsOrigin() ) < 512 )
		{
			// Only start facing when we're close enough
			AddFacingTarget( GetEnemy(), vecEnemyLKP, 1.0, 0.2 );
		}
	}

	return BaseClass::OverrideMoveFacing( move, flInterval );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::InputDisableJump( inputdata_t &inputdata )
{
	m_bDisableJump = true;
	CapabilitiesRemove( bits_CAP_MOVE_JUMP );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Antlion::InputEnableJump( inputdata_t &inputdata )
{
	m_bDisableJump = false;
	CapabilitiesAdd( bits_CAP_MOVE_JUMP );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *pTarget - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::SetFollowTarget( CBaseEntity *pTarget )
{
	m_FollowBehavior.SetFollowTarget( pTarget );
	m_hFollowTarget = pTarget;
	m_flObeyFollowTime = gpGlobals->curtime + ANTLION_OBEY_FOLLOW_TIME;

	SetCondition( COND_ANTLION_RECEIVED_ORDERS );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::CreateBehaviors( void )
{
	AddBehavior( &m_FollowBehavior );
	AddBehavior( &m_AssaultBehavior );

	return BaseClass::CreateBehaviors();
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : state - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::SetMoveState( AntlionMoveState_e state )
{
	m_MoveState = state;

	switch( m_MoveState )
	{
	case ANTLION_MOVE_FOLLOW:

		m_FollowBehavior.SetFollowTarget( m_hFollowTarget );
		
		// Clear any previous state
		m_flSuppressFollowTime = 0;
		
		break;
	
	case ANTLION_MOVE_FIGHT_TO_GOAL:
		
		m_FollowBehavior.SetFollowTarget( NULL );

		// Keep the time we started this
		m_flSuppressFollowTime = gpGlobals->curtime + random->RandomInt( 10, 15 );
		break;

	default:
		break;
	}
}

//-----------------------------------------------------------------------------
// Purpose: Special version helps other NPCs hit overturned antlion
//-----------------------------------------------------------------------------
Vector CNPC_Antlion::BodyTarget( const Vector &posSrc, bool bNoisy /*= true*/ )
{ 
	// Cache the bone away to avoid future lookups
	if ( m_nBodyBone == -1 )
	{
		CBaseAnimating *pAnimating = GetBaseAnimating();
		m_nBodyBone = pAnimating->LookupBone( "Antlion.Body_Bone" );
	}

	// Get the exact position in our center of mass (thorax)
	Vector vecResult;
	QAngle vecAngle;
	GetBonePosition( m_nBodyBone, vecResult, vecAngle );
	
	if ( bNoisy )
		return vecResult + RandomVector( -8, 8 );

	return vecResult;
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &inputdata - 
//-----------------------------------------------------------------------------
void CNPC_Antlion::InputJumpAtTarget( inputdata_t &inputdata )
{
	CBaseEntity *pJumpTarget = gEntList.FindEntityByName( NULL, inputdata.value.String(), this, inputdata.pActivator, inputdata.pCaller );
	if ( pJumpTarget == NULL )
	{
		Msg("Unable to find jump target named (%s)\n", inputdata.value.String() );
		return;
	}

#if HL2_EPISODIC

	// Try the jump
	AIMoveTrace_t moveTrace;
	Vector targetPos = pJumpTarget->GetAbsOrigin();

	// initialize jump state
	float minJumpHeight = 0.0;
	float maxHorzVel = 800.0f;

	// initial jump, sets baseline for minJumpHeight
	Vector vecApex;
	Vector rawJumpVel = GetMoveProbe()->CalcJumpLaunchVelocity(GetAbsOrigin(), targetPos, GetCurrentGravity() * GetJumpGravity(), &minJumpHeight, maxHorzVel, &vecApex );

	if ( g_debug_antlion.GetInt() == 2 )
	{
		NDebugOverlay::Box( targetPos, GetHullMins(), GetHullMaxs(), 0, 255, 0, 0, 5 );
		NDebugOverlay::Line( GetAbsOrigin(), targetPos, 0, 255, 0, 0, 5 );
		NDebugOverlay::Line( GetAbsOrigin(), rawJumpVel, 255, 255, 0, 0, 5 );
	}

	m_vecSavedJump = rawJumpVel;

#else	

	// Get the direction and speed to our target
	Vector vecJumpDir = ( pJumpTarget->GetAbsOrigin() - GetAbsOrigin() );
	VectorNormalize( vecJumpDir );
	vecJumpDir *= 800.0f;	// FIXME: We'd like to pass this in as a parameter, but comma delimited lists are bad
	m_vecSavedJump = vecJumpDir;

#endif

	SetCondition( COND_ANTLION_CAN_JUMP_AT_TARGET );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
bool CNPC_Antlion::IsHeavyDamage( const CTakeDamageInfo &info )
{
	return BaseClass::IsHeavyDamage( info );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : bForced - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CNPC_Antlion::CanRunAScriptedNPCInteraction( bool bForced /*= false*/ )
{
	return BaseClass::CanRunAScriptedNPCInteraction( bForced );
}

//---------------------------------------------------------
// Save/Restore
//---------------------------------------------------------
BEGIN_DATADESC( CAntlionRepellant )
	DEFINE_KEYFIELD( m_flRepelRadius,	FIELD_FLOAT,	"repelradius" ),
	DEFINE_FIELD( m_bEnabled, FIELD_BOOLEAN ),
	DEFINE_INPUTFUNC( FIELD_VOID,	"Enable", InputEnable ),
	DEFINE_INPUTFUNC( FIELD_VOID,	"Disable", InputDisable ),
END_DATADESC()

static CUtlVector< CHandle< CAntlionRepellant > >m_hRepellantList;


CAntlionRepellant::~CAntlionRepellant()
{
	m_hRepellantList.FindAndRemove( this );
}

void CAntlionRepellant::Spawn( void )
{
	BaseClass::Spawn();
	m_bEnabled = true;

	m_hRepellantList.AddToTail( this );
}

void CAntlionRepellant::InputEnable( inputdata_t &inputdata )
{
	m_bEnabled = true;

	if ( m_hRepellantList.HasElement( this ) == false )
		 m_hRepellantList.AddToTail( this );
}

void CAntlionRepellant::InputDisable( inputdata_t &inputdata )
{
	m_bEnabled = false;
	m_hRepellantList.FindAndRemove( this );
}

float CAntlionRepellant::GetRadius( void )
{
	if ( m_bEnabled == false )
		 return 0.0f;

	return m_flRepelRadius;
}

void CAntlionRepellant::OnRestore( void )
{
	BaseClass::OnRestore();

	if ( m_bEnabled == true )
	{
		if ( m_hRepellantList.HasElement( this ) == false )
			 m_hRepellantList.AddToTail( this );
	}
}

bool CAntlionRepellant::IsPositionRepellantFree( Vector vDesiredPos )
{
	for ( int i = 0; i < m_hRepellantList.Count(); i++ )
	{
		if ( m_hRepellantList[i] )
		{
			CAntlionRepellant *pRep = m_hRepellantList[i].Get();

			if ( pRep )
			{
				float flDist = (vDesiredPos - pRep->GetAbsOrigin()).Length();

				if ( flDist <= pRep->GetRadius() )
					 return false;
			}
		}
	}

	return true;
}

LINK_ENTITY_TO_CLASS( point_antlion_repellant, CAntlionRepellant);


//-----------------------------------------------------------------------------
//
// Schedules
//
//-----------------------------------------------------------------------------

AI_BEGIN_CUSTOM_NPC( npc_antlion, CNPC_Antlion )

	//Register our interactions
	DECLARE_INTERACTION( g_interactionAntlionFoundTarget )
	DECLARE_INTERACTION( g_interactionAntlionFiredAtTarget )

	//Conditions
	DECLARE_CONDITION( COND_ANTLION_ON_NPC )
	DECLARE_CONDITION( COND_ANTLION_CAN_JUMP )
	DECLARE_CONDITION( COND_ANTLION_FOLLOW_TARGET_TOO_FAR )
	DECLARE_CONDITION( COND_ANTLION_RECEIVED_ORDERS )
	DECLARE_CONDITION( COND_ANTLION_IN_WATER )
	DECLARE_CONDITION( COND_ANTLION_CAN_JUMP_AT_TARGET )
	DECLARE_CONDITION( COND_ANTLION_SQUADMATE_KILLED )
		
	//Squad slots
	DECLARE_SQUADSLOT( SQUAD_SLOT_ANTLION_JUMP )

	//Tasks
	DECLARE_TASK( TASK_ANTLION_JUMP )
	DECLARE_TASK( TASK_ANTLION_WAIT_FOR_TRIGGER )
	DECLARE_TASK( TASK_ANTLION_DISMOUNT_NPC )
	DECLARE_TASK( TASK_ANTLION_REACH_FIGHT_GOAL )
	DECLARE_TASK( TASK_ANTLION_GET_PHYSICS_DANGER_ESCAPE_PATH )
	DECLARE_TASK( TASK_ANTLION_FACE_JUMP )
	DECLARE_TASK( TASK_ANTLION_DROWN )
	DECLARE_TASK( TASK_ANTLION_GET_PATH_TO_RANDOM_NODE )
	DECLARE_TASK( TASK_ANTLION_FIND_COVER_FROM_SAVEPOSITION )

	//Activities
	DECLARE_ACTIVITY( ACT_ANTLION_JUMP_START )
	//DECLARE_ACTIVITY( ACT_ANTLION_POUNCE )
	//DECLARE_ACTIVITY( ACT_ANTLION_POUNCE_MOVING )
	DECLARE_ACTIVITY( ACT_ANTLION_DROWN )
	DECLARE_ACTIVITY( ACT_ANTLION_LAND )

	//Events
	DECLARE_ANIMEVENT( AE_ANTLION_WALK_FOOTSTEP )
	DECLARE_ANIMEVENT( AE_ANTLION_MELEE_HIT1 )
	//DECLARE_ANIMEVENT( AE_ANTLION_FOOTSTEP_SOFT )
	//DECLARE_ANIMEVENT( AE_ANTLION_FOOTSTEP_HEAVY )
	DECLARE_ANIMEVENT( AE_ANTLION_START_JUMP )
	DECLARE_ANIMEVENT( AE_ANTLION_MELEE1_SOUND )
	//DECLARE_ANIMEVENT( AE_ANTLION_MELEE2_SOUND )

	//Schedules

	//==================================================
	// Jump
	//==================================================

	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_JUMP,

		"	Tasks"
		"		TASK_STOP_MOVING				0"
		"		TASK_ANTLION_FACE_JUMP			0"
		"		TASK_PLAY_SEQUENCE				ACTIVITY:ACT_ANTLION_JUMP_START"
		"		TASK_ANTLION_JUMP				0"
		""
		"	Interrupts"
		"		COND_TASK_FAILED"
	)

	//=========================================================
	// Headcrab has landed atop another NPC. Get down!
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_DISMOUNT_NPC,

		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_ANTLION_DISMOUNT_NPC	0"

		"	Interrupts"
	)

	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_RUN_TO_FIGHT_GOAL,

		"	Tasks"
		"		TASK_SET_TOLERANCE_DISTANCE		128"
		"		TASK_GET_PATH_TO_SAVEPOSITION	0"
		"		TASK_RUN_PATH					0"
		"		TASK_WAIT_FOR_MOVEMENT			0"
		"		TASK_ANTLION_REACH_FIGHT_GOAL	0"

		"	Interrupts"
		"		COND_NEW_ENEMY"
		"		COND_HEAVY_DAMAGE"
		"		COND_LIGHT_DAMAGE"
		"		COND_HEAVY_DAMAGE"
		"		COND_ANTLION_CAN_JUMP"
	)

	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_RUN_TO_FOLLOW_GOAL,

		"	Tasks"
		"		TASK_SET_TOLERANCE_DISTANCE		128"
		"		TASK_GET_PATH_TO_SAVEPOSITION	0"
		"		TASK_RUN_PATH					0"
		"		TASK_WAIT_FOR_MOVEMENT			0"

		"	Interrupts"
		"		COND_NEW_ENEMY"
		"		COND_HEAVY_DAMAGE"
		"		COND_ANTLION_CAN_JUMP"
		"		COND_ANTLION_FOLLOW_TARGET_TOO_FAR"
	)


	//==================================================
	// Run from the sound of a physics crash
	//==================================================
	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_FLEE_PHYSICS_DANGER,
		
		"	Tasks"
		"		TASK_SET_FAIL_SCHEDULE						SCHEDULE:SCHED_CHASE_ENEMY"
		"		TASK_ANTLION_GET_PHYSICS_DANGER_ESCAPE_PATH	1024"
		"		TASK_RUN_PATH								0"
		"		TASK_WAIT_FOR_MOVEMENT						0"
		"		TASK_STOP_MOVING							0"
		""
		"	Interrupts"
		"		COND_TASK_FAILED"
	)

	// Pounce forward at our enemy
	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_POUNCE,

		"	Tasks"
		"		TASK_STOP_MOVING		0"
		"		TASK_FACE_ENEMY			0"
		"		TASK_ANNOUNCE_ATTACK	1"
		"		TASK_RESET_ACTIVITY		0"
		"		TASK_PLAY_SEQUENCE		ACTIVITY:ACT_ANTLION_POUNCE"

		"	Interrupts"
		"		COND_TASK_FAILED"
	)
	// Pounce forward at our enemy
	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_POUNCE_MOVING,

		"	Tasks"
		"		TASK_STOP_MOVING		0"
		"		TASK_FACE_ENEMY			0"
		"		TASK_ANNOUNCE_ATTACK	1"
		"		TASK_RESET_ACTIVITY		0"
		"		TASK_PLAY_SEQUENCE		ACTIVITY:ACT_ANTLION_POUNCE_MOVING"

		"	Interrupts"
		"		COND_TASK_FAILED"
	)

	//=========================================================
	// The irreversible process of drowning
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_DROWN,

		"	Tasks"
		"		TASK_SET_ACTIVITY			ACTIVITY:ACT_ANTLION_DROWN"
		"		TASK_ANTLION_DROWN			0"
		""
		"	Interrupts"
	)

	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_TAKE_COVER_FROM_ENEMY,

		"	Tasks"
		"		TASK_SET_FAIL_SCHEDULE			SCHEDULE:SCHED_FAIL_TAKE_COVER"
		"		TASK_FIND_COVER_FROM_ENEMY		0"
		"		TASK_RUN_PATH					0"
		"		TASK_WAIT_FOR_MOVEMENT			0"
		"		TASK_STOP_MOVING				0"
		""
		"	Interrupts"
		"		COND_TASK_FAILED"
		"		COND_NEW_ENEMY"
	)

	DEFINE_SCHEDULE
	(
		SCHED_ANTLION_TAKE_COVER_FROM_SAVEPOSITION,

		"	Tasks"
		"		TASK_SET_FAIL_SCHEDULE						SCHEDULE:SCHED_FAIL_TAKE_COVER"
		"		TASK_ANTLION_FIND_COVER_FROM_SAVEPOSITION	0"
		"		TASK_RUN_PATH								0"
		"		TASK_WAIT_FOR_MOVEMENT						0"
		"		TASK_STOP_MOVING							0"
		""
		"	Interrupts"
		"		COND_TASK_FAILED"
		"		COND_NEW_ENEMY"
	)

AI_END_CUSTOM_NPC()

//-----------------------------------------------------------------------------
// Purpose: Whether or not the entity is a common antlion
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool IsAntlion( CBaseEntity *pEntity )
{
	return true;
}
