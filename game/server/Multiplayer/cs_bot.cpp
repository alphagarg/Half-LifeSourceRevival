//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#include "cbase.h"
#include "hl1mp_gamerules.h"
#include "func_breakablesurf.h"
#include "obstacle_pushaway.h"

#include "cs_bot.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

LINK_ENTITY_TO_CLASS( cs_bot, CCSBot );

BEGIN_DATADESC( CCSBot )

END_DATADESC()

//--------------------------------------------------------------------------------------------------------------
/**
 * Change movement speed to walking
 */
void CCSBot::Walk( void )
{
	Msg("Walk\n");
	if (m_mustRunTimer.IsElapsed())
	{
		BaseClass::Walk();
	}
	else
	{
		// must run
		Run();
	}
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if jump was started.
 * This is extended from the base jump to disallow jumping when in a crouch area.
 */
bool CCSBot::Jump( bool mustJump )
{
	Msg("Jump\n");
	// prevent jumping if we're crouched, unless we're in a crouchjump area - jump wins
	bool inCrouchJumpArea = (m_lastKnownArea && 
		(m_lastKnownArea->GetAttributes() & NAV_MESH_CROUCH) &&
		(m_lastKnownArea->GetAttributes() & NAV_MESH_JUMP));

	if ( !IsUsingLadder() && IsDucked() && !inCrouchJumpArea )
	{
		return false;
	}

	return BaseClass::Jump( mustJump );
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Invoked when injured by something
 * NOTE: We dont want to directly call Attack() here, or the bots will have super-human reaction times when injured
 */
int CCSBot::OnTakeDamage( const CTakeDamageInfo &info )
{
	Msg("OnTakeDamage\n");
	CBaseEntity *attacker = info.GetInflictor();

	// getting hurt makes us alert
	BecomeAlert();
	StopWaiting();

	if (attacker->IsPlayer() && IsEnemy( attacker ))
	{
		// Track previous attacker so we don't try to panic multiple times for a shotgun blast
		CHL1MP_Player *lastAttacker = m_attacker;
		float lastAttackedTimestamp = m_attackedTimestamp;

		// keep track of our last attacker
		m_attacker = reinterpret_cast<CHL1MP_Player *>( attacker );
		m_attackedTimestamp = gpGlobals->curtime;

		if ( !IsSurprised() && (m_attacker != lastAttacker || m_attackedTimestamp != lastAttackedTimestamp) )
		{
			CHL1MP_Player *enemy = static_cast<CHL1MP_Player *>( attacker );

			// being hurt by an enemy we can't see causes panic
			if (!IsVisible( enemy, CHECK_FOV ))
			{
				// if not attacking anything, look around to try to find attacker
				if (!IsAttacking())
				{
					Panic();
				}
				else	// we are attacking
				{
					if (!IsEnemyVisible())
					{
						// can't see our current enemy, panic to acquire new attacker
						Panic();
					}
				}
			}
		}
	}

	// extend
	return BaseClass::OnTakeDamage( info );
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Invoked when killed
 */
void CCSBot::Event_Killed( const CTakeDamageInfo &info )
{ 
	Msg("Event_Killed\n");
	
	FireWeaponAtEnemy();

	// extend
	BaseClass::Event_Killed( info );
	CHL1MP_Player::Event_Killed( info );
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if line segment intersects rectagular volume
 */
#define HI_X	0x01
#define LO_X 0x02
#define HI_Y	0x04
#define LO_Y 0x08
#define HI_Z	0x10
#define LO_Z 0x20

inline bool IsIntersectingBox( const Vector& start, const Vector& end, const Vector& boxMin, const Vector& boxMax )
{
	Msg("IsIntersectingBox\n");
	unsigned char startFlags = 0;
	unsigned char endFlags = 0;

	// classify start point
	if (start.x < boxMin.x)
		startFlags |= LO_X;
	if (start.x > boxMax.x)
		startFlags |= HI_X;

	if (start.y < boxMin.y)
		startFlags |= LO_Y;
	if (start.y > boxMax.y)
		startFlags |= HI_Y;

	if (start.z < boxMin.z)
		startFlags |= LO_Z;
	if (start.z > boxMax.z)
		startFlags |= HI_Z;

	// classify end point
	if (end.x < boxMin.x)
		endFlags |= LO_X;
	if (end.x > boxMax.x)
		endFlags |= HI_X;

	if (end.y < boxMin.y)
		endFlags |= LO_Y;
	if (end.y > boxMax.y)
		endFlags |= HI_Y;

	if (end.z < boxMin.z)
		endFlags |= LO_Z;
	if (end.z > boxMax.z)
		endFlags |= HI_Z;

	// trivial reject
	if (startFlags & endFlags)
		return false;

	/// @todo Do exact line/box intersection check

	return true;
}


extern void UTIL_DrawBox( Extent *extent, int lifetime, int red, int green, int blue );

//--------------------------------------------------------------------------------------------------------------
/**
 * When bot is touched by another entity.
 */
void CCSBot::Touch( CBaseEntity *other )
{
	Msg("Touch\n");
	// EXTEND
	BaseClass::Touch( other );

	// if we have touched a higher-priority player, make way
	/// @todo Need to account for reaction time, etc.
	if (other->IsPlayer())
	{
		// if we are on a ladder, don't move
		if (IsUsingLadder())
			return;

		CHL1MP_Player *player = static_cast<CHL1MP_Player *>( other );

		// get priority of other player
		unsigned int otherPri = TheCSBots()->GetPlayerPriority( player );

		// get our priority
		unsigned int myPri = TheCSBots()->GetPlayerPriority( this );

		// if our priority is better, don't budge
		if (myPri < otherPri)
			return;

		// they are higher priority - make way, unless we're already making way for someone more important
		if (m_avoid != NULL)
		{
			unsigned int avoidPri = TheCSBots()->GetPlayerPriority( static_cast<CBasePlayer *>( static_cast<CBaseEntity *>( m_avoid ) ) );
			if (avoidPri < otherPri)
			{
				// ignore 'other' because we're already avoiding someone better
				return;
			}
		}

		m_avoid = other;
		m_avoidTimestamp = gpGlobals->curtime;
	}

	// Check for breakables we're actually touching
	// If we're not stuck or crouched, we don't care
	if ( !m_isStuck && !IsCrouching() && !IsOnLadder() )
		return;

	// See if it's breakable
	if ( IsBreakableEntity( other ) )
	{
		// it's breakable - try to shoot it.
		SetLookAt( "Breakable", other->WorldSpaceCenter(), PRIORITY_HIGH, 0.1f, false, 5.0f, true );
	}
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if we are busy doing something important
 */
bool CCSBot::IsBusy( void ) const
{
	Msg("IsBusy\n");
	if (IsAttacking())
	{
		return true;
	}

	return false;
}

//--------------------------------------------------------------------------------------------------------------
void CCSBot::BotDeathThink( void )
{
	Msg("BotDeathThink\n");
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Assign given player as our current enemy to attack
 */
void CCSBot::SetBotEnemy( CHL1MP_Player *enemy )
{
	Msg("SetBotEnemy\n");
	if (m_enemy != enemy)
	{
		m_enemy = enemy; 
		m_currentEnemyAcquireTimestamp = gpGlobals->curtime;

		PrintIfWatched( "SetBotEnemy: %s\n", (enemy) ? enemy->GetPlayerName() : "(NULL)" );
	}
}

//--------------------------------------------------------------------------------------------------------------
/**
 * If we are not on the navigation mesh (m_currentArea == NULL),
 * move towards last known area.
 * Return false if off mesh.
 */
bool CCSBot::StayOnNavMesh( void )
{
	Msg("StayOnNavMesh\n");
	if (m_currentArea == NULL)
	{
		// move back onto the area map

		// if we have no lastKnownArea, we probably started off
		// of the nav mesh - find the closest nav area and use it
		CNavArea *goalArea;
		if (!m_currentArea && !m_lastKnownArea)
		{
			goalArea = TheNavMesh->GetNearestNavArea( GetCentroid( this ) );
			PrintIfWatched( "Started off the nav mesh - moving to closest nav area...\n" );
		}
		else
		{
			goalArea = m_lastKnownArea;
			PrintIfWatched( "Getting out of NULL area...\n" );
		}

		if (goalArea)
		{
			Vector pos;
			goalArea->GetClosestPointOnArea( GetCentroid( this ), &pos );

			// move point into area
			Vector to = pos - GetCentroid( this );
			to.NormalizeInPlace();

			const float stepInDist = 5.0f;		// how far to "step into" an area - must be less than min area size
			pos = pos + (stepInDist * to);

			MoveTowardsPosition( pos );
		}

		// if we're stuck, try to get un-stuck
		// do stuck movements last, so they override normal movement
		if (m_isStuck)
			Wiggle();
				
		return false;
	}

	return true;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return last enemy that hurt us
 */
CHL1MP_Player *CCSBot::GetAttacker( void ) const
{
	Msg("GetAttacker\n");
	if (m_attacker && m_attacker->IsAlive())
		return m_attacker;

	return NULL;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Immediately jump off of our ladder, if we're on one
 */
void CCSBot::GetOffLadder( void )
{
	Msg("GetOffLadder\n");
	if (IsUsingLadder())
	{
		Jump( MUST_JUMP );
		DestroyPath();
	}
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if we are outnumbered by enemies
 */
bool CCSBot::IsOutnumbered( void ) const
{
	Msg("IsOutnumbered\n");
	return false;
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Return number of enemies we are outnumbered by
 */
int CCSBot::OutnumberedCount( void ) const
{
	Msg("OutnumberedCount\n");
	return 0;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Sets our current disposition
 */
void CCSBot::SetDisposition( DispositionType disposition ) 
{
	Msg("SetDisposition\n");
	m_disposition = disposition;

	if (m_disposition != IGNORE_ENEMIES)
		m_ignoreEnemiesTimer.Invalidate();
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Return our current disposition
 */
CCSBot::DispositionType CCSBot::GetDisposition( void ) const
{
	Msg("DispositionType\n");
	if (!m_ignoreEnemiesTimer.IsElapsed())
		return IGNORE_ENEMIES;
	
	return m_disposition;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Ignore enemies for a short duration
 */
void CCSBot::IgnoreEnemies( float duration )
{
	Msg("IgnoreEnemies\n");
	m_ignoreEnemiesTimer.Start( duration );
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if we are in a hurry 
 */
bool CCSBot::IsHurrying( void ) const
{
	Msg("IsHurrying\n");
	if (!m_hurryTimer.IsElapsed())
		return true;

	return false;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if we haven't seen an enemy for "a long time"
 */
bool CCSBot::HasNotSeenEnemyForLongTime( void ) const
{
	Msg("HasNotSeenEnemyForLongTime\n");
	const float longTime = 30.0f;
	return (GetTimeSinceLastSawEnemy() > longTime);
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return string describing current task
 * NOTE: This MUST be kept in sync with the CCSBot::TaskType enum
 */
const char *CCSBot::GetTaskName( void ) const
{
	Msg("GetTaskName\n");
	static char *name[ NUM_TASKS ] = 
	{
		"SEEK_AND_DESTROY",
		"HOLD_POSITION",
		"MOVE_TO_LAST_KNOWN_ENEMY_POSITION"
	};

	return name[ (int)GetTask() ];
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Return string describing current disposition
 * NOTE: This MUST be kept in sync with the CCSBot::DispositionType enum
 */
const char *CCSBot::GetDispositionName( void ) const
{
	Msg("GetDispositionName\n");
	static char *name[ NUM_DISPOSITIONS ] = 
	{
		"ENGAGE_AND_INVESTIGATE",
		"OPPORTUNITY_FIRE",
		"SELF_DEFENSE",
		"IGNORE_ENEMIES"
	};

	return name[ (int)GetDisposition() ];
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Fill in a CUserCmd with our data
 */
void CCSBot::BuildUserCmd( CUserCmd& cmd, const QAngle& viewangles, float forwardmove, float sidemove, float upmove, int buttons, byte impulse )
{
	Msg("BuildUserCmd\n");
	Q_memset( &cmd, 0, sizeof( cmd ) );

	// Don't walk when ducked - it's painfully slow
	if ( m_Local.m_bDucked || m_Local.m_bDucking )
	{
		buttons &= ~IN_SPEED;
	}

	cmd.command_number = gpGlobals->tickcount;
	cmd.forwardmove = forwardmove;
	cmd.sidemove = sidemove;
	cmd.upmove = upmove;
	cmd.buttons = buttons;
	cmd.impulse = impulse;

	VectorCopy( viewangles, cmd.viewangles );
	cmd.random_seed = random->RandomInt( 0, 0x7fffffff );
}

//--------------------------------------------------------------------------------------------------------------
