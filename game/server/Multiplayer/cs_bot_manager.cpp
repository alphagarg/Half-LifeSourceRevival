//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#pragma warning( disable : 4530 )					// STL uses exceptions, but we are not compiling with them - ignore warning

#include "cbase.h"

#include "cs_bot.h"
#include "nav_area.h"
#include "hl1mp_gamerules.h"
#include "Multiplayer/shared_util.h"
#include "KeyValues.h"
#include "tier0/icommandline.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#ifdef _WIN32
#pragma warning (disable:4701)				// disable warning that variable *may* not be initialized 
#endif

CBotManager *TheBots = NULL;

bool CCSBotManager::m_isMapDataLoaded = false;

int g_nClientPutInServerOverrides = 0;

int UTIL_CSSBotsInGame( void );

ConVar bot_join_delay( "bot_join_delay", "0", FCVAR_GAMEDLL, "Prevents bots from joining the server for this many seconds after a map change." );

/**
 * Determine whether bots can be used or not
 */
inline bool AreBotsAllowed()
{
	// If they pass in -nobots, don't allow bots.  This is for people who host servers, to
	// allow them to disallow bots to enforce CPU limits.
	const char *nobots = CommandLine()->CheckParm( "-nobots" );
	if ( nobots )
	{
		return false;
	}

	return true;
}


//--------------------------------------------------------------------------------------------------------------
void InstallBotControl( void )
{
	if ( TheBots != NULL )
		delete TheBots;

	TheBots = new CCSBotManager;
}


//--------------------------------------------------------------------------------------------------------------
void RemoveBotControl( void )
{
	if ( TheBots != NULL )
		delete TheBots;

	TheBots = NULL;
}


//--------------------------------------------------------------------------------------------------------------
CBasePlayer* ClientPutInServerOverride_Bot( edict_t *pEdict, const char *playername )
{
	CBasePlayer *pPlayer = TheBots->AllocateAndBindBotEntity( pEdict );
	if ( pPlayer )
	{
		pPlayer->SetPlayerName( playername );
	}
	++g_nClientPutInServerOverrides;

	return pPlayer;
}

//--------------------------------------------------------------------------------------------------------------
// Constructor
CCSBotManager::CCSBotManager()
{
	m_serverActive = false;

	m_eventListenersEnabled = true;
	m_commonEventListeners.AddToTail( &m_PlayerFootstepEvent );
	m_commonEventListeners.AddToTail( &m_PlayerFallDamageEvent );
	m_commonEventListeners.AddToTail( &m_DoorMovingEvent );
	m_commonEventListeners.AddToTail( &m_BreakPropEvent );
	m_commonEventListeners.AddToTail( &m_BreakBreakableEvent );
	m_commonEventListeners.AddToTail( &m_WeaponFireEvent );
	m_commonEventListeners.AddToTail( &m_WeaponFireOnEmptyEvent );
	m_commonEventListeners.AddToTail( &m_WeaponReloadEvent );
	m_commonEventListeners.AddToTail( &m_WeaponZoomEvent );
	m_commonEventListeners.AddToTail( &m_BulletImpactEvent );
	m_commonEventListeners.AddToTail( &m_NavBlockedEvent );

	TheBotProfiles = new BotProfileManager;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Invoked when a new round begins
 */
void CCSBotManager::RestartRound( void )
{
	// extend
	CBotManager::RestartRound();

	m_lastSeenEnemyTimestamp = -9999.9f;
}

//--------------------------------------------------------------------------------------------------------------

void UTIL_DrawBox( Extent *extent, int lifetime, int red, int green, int blue )
{
	int darkRed = red/2;
	int darkGreen = green/2;
	int darkBlue = blue/2;

	Vector v[8];
	v[0].x = extent->lo.x; v[0].y = extent->lo.y; v[0].z = extent->lo.z;
	v[1].x = extent->hi.x; v[1].y = extent->lo.y; v[1].z = extent->lo.z;
	v[2].x = extent->hi.x; v[2].y = extent->hi.y; v[2].z = extent->lo.z;
	v[3].x = extent->lo.x; v[3].y = extent->hi.y; v[3].z = extent->lo.z;
	v[4].x = extent->lo.x; v[4].y = extent->lo.y; v[4].z = extent->hi.z;
	v[5].x = extent->hi.x; v[5].y = extent->lo.y; v[5].z = extent->hi.z;
	v[6].x = extent->hi.x; v[6].y = extent->hi.y; v[6].z = extent->hi.z;
	v[7].x = extent->lo.x; v[7].y = extent->hi.y; v[7].z = extent->hi.z;

	static int edge[] = 
	{
		1, 2, 3, 4, -1,
		5, 6, 7, 8, -5,
		1, -5,
		2, -6,
		3, -7,
		4, -8,
		0
	};

	Vector from, to;
	bool restart = true;
	for( int i=0; edge[i] != 0; ++i )
	{
		if (restart)
		{
			to = v[ edge[i]-1 ];
			restart = false;
			continue;
		}
		
		from = to;

		int index = edge[i];
		if (index < 0)
		{
			restart = true;
			index = -index;
		}

		to = v[ index-1 ];

		NDebugOverlay::Line( from, to, darkRed, darkGreen, darkBlue, true, 0.1f );
		NDebugOverlay::Line( from, to, red, green, blue, false, 0.15f );
	}
}

//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::EnableEventListeners( bool enable )
{
	if ( m_eventListenersEnabled == enable )
	{
		return;
	}

	m_eventListenersEnabled = enable;

	// enable/disable the most frequent event listeners, to improve performance when no bots are present.
	for ( int i=0; i<m_commonEventListeners.Count(); ++i )
	{
		if ( enable )
		{
			gameeventmanager->AddListener( m_commonEventListeners[i], m_commonEventListeners[i]->GetEventName(), true );
		}
		else
		{
			gameeventmanager->RemoveListener( m_commonEventListeners[i] );
		}
	}
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Called each frame
 */
void CCSBotManager::StartFrame( void )
{
	if ( !AreBotsAllowed() )
	{
		EnableEventListeners( false );
		return;
	}

	// EXTEND
	CBotManager::StartFrame();

	MaintainBotQuota();
	EnableEventListeners( UTIL_CSSBotsInGame() > 0 );

	if ( m_checkTransientAreasTimer.IsElapsed() && !nav_edit.GetBool() )
	{
		CUtlVector< CNavArea * >& transientAreas = TheNavMesh->GetTransientAreas();
		for ( int i=0; i<transientAreas.Count(); ++i )
		{
			CNavArea *area = transientAreas[i];
			if ( area->GetAttributes() & NAV_MESH_TRANSIENT )
			{
				area->UpdateBlocked();
			}
		}

		m_checkTransientAreasTimer.Start( 2.0f );
	}
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if the bot can use this weapon
 */
bool CCSBotManager::IsWeaponUseable( const CBaseHL1MPCombatWeapon *weapon ) const
{
	if (weapon == NULL)
		return false;

	return true;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Invoked when a map has just been loaded
 */
void CCSBotManager::ServerActivate( void )
{
	m_isMapDataLoaded = false;

	TheBotProfiles->Reset();

	// tell the Navigation Mesh system what CS spawn points are named
	TheNavMesh->SetPlayerSpawnName( "info_player_deathmatch" );

	RestartRound();

	m_serverActive = true;
}


void CCSBotManager::ServerDeactivate( void )
{
	m_serverActive = false;
}

void CCSBotManager::ClientDisconnect( CBaseEntity *entity )
{
}



//--------------------------------------------------------------------------------------------------------------
/**
* Parses out bot name/template/etc params from the current ConCommand
*/
void BotArgumentsFromArgv( const CCommand &args, const char **name, BotDifficultyType *difficulty, bool *all = NULL )
{
	static char s_name[MAX_PLAYER_NAME_LENGTH];

	s_name[0] = 0;
	*name = s_name;
	*difficulty = NUM_DIFFICULTY_LEVELS;
	if ( all )
	{
		*all = false;
	}

	for ( int arg=1; arg<args.ArgC(); ++arg )
	{
		bool found = false;

		const char *token = args[arg];
		if ( all && FStrEq( token, "all" ) )
		{
			*all = true;
			found = true;
		}
		
		for( int i=0; i<NUM_DIFFICULTY_LEVELS && !found; ++i )
		{
			if (!stricmp( BotDifficultyName[i], token ))
			{
				*difficulty = (BotDifficultyType)i;
				found = true;
			}
		}

		if ( !found )
		{
			Q_strncpy( s_name, token, sizeof( s_name ) );
		}
	}
}


//--------------------------------------------------------------------------------------------------------------
CON_COMMAND_F( bot_add, "bot_add <difficulty> <name> - Adds a bot matching the given criteria.", FCVAR_GAMEDLL )
{
	if ( !UTIL_IsCommandIssuedByServerAdmin() )
		return;

	const char *name;
	BotDifficultyType difficulty;
	BotArgumentsFromArgv( args, &name, &difficulty );
	TheCSBots()->BotAddCommand( FROM_CONSOLE, name, difficulty );
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Collects all bots matching the given criteria (player name, profile template name, and difficulty)
 */
class CollectBots
{
public:
	CollectBots( const char *name, BotDifficultyType difficulty )
	{
		m_name = name;
		m_difficulty = difficulty;
	}

	bool operator() ( CBasePlayer *player )
	{
		if ( !player->IsBot() )
		{
			return true;
		}

		CCSBot *bot = dynamic_cast< CCSBot * >(player);
		if ( !bot || !bot->GetProfile() )
		{
			return true;
		}

		if ( m_name && *m_name )
		{
			// accept based on name
			if ( FStrEq( m_name, bot->GetProfile()->GetName() ) )
			{
				m_bots.RemoveAll();
				m_bots.AddToTail( bot );
				return false;
			}

			// Reject based on profile template name
			if ( !bot->GetProfile()->InheritsFrom( m_name ) )
			{
				return true;
			}
		}

		// reject based on difficulty
		if ( m_difficulty != NUM_DIFFICULTY_LEVELS )
		{
			if ( !bot->GetProfile()->IsDifficulty( m_difficulty ) )
			{
				return true;
			}
		}

		// A match!
		m_bots.AddToTail( bot );

		return true;
	}

	CUtlVector< CCSBot * > m_bots;

private:
	const char *m_name;
	BotDifficultyType m_difficulty;
};

//--------------------------------------------------------------------------------------------------------------
CON_COMMAND_F( bot_kill, "bot_kill <all> <difficulty> <name> - Kills a specific bot, or all bots, matching the given criteria.", FCVAR_GAMEDLL )
{
	if ( !UTIL_IsCommandIssuedByServerAdmin() )
		return;

	const char *name;
	BotDifficultyType difficulty;
	bool all;

	BotArgumentsFromArgv( args, &name, &difficulty, &all );
	if ( (!name || !*name) && difficulty == NUM_DIFFICULTY_LEVELS )
	{
		all = true;
	}

	CollectBots collector( name, difficulty );
	ForEachPlayer( collector );

	for ( int i=0; i<collector.m_bots.Count(); ++i )
	{
		CCSBot *bot = collector.m_bots[i];
		if ( !bot->IsAlive() )
			continue;

		bot->CommitSuicide();
		if ( !all )
		{
			return;
		}
	}
}


//--------------------------------------------------------------------------------------------------------------
CON_COMMAND_F( bot_kick, "bot_kick <all> <difficulty> <name> - Kicks a specific bot, or all bots, matching the given criteria.", FCVAR_GAMEDLL )
{
	if ( !UTIL_IsCommandIssuedByServerAdmin() )
		return;

	const char *name;
	BotDifficultyType difficulty;
	bool all;

	BotArgumentsFromArgv( args, &name, &difficulty, &all );
	if ( (!name || !*name) && difficulty == NUM_DIFFICULTY_LEVELS )
	{
		all = true;
	}

	CollectBots collector( name, difficulty );
	ForEachPlayer( collector );

	for ( int i=0; i<collector.m_bots.Count(); ++i )
	{
		CCSBot *bot = collector.m_bots[i];
		engine->ServerCommand( UTIL_VarArgs( "kick \"%s\"\n", bot->GetPlayerName() ) );
		if ( !all )
		{
			// adjust bot quota so kicked bot is not immediately added back in
			int newQuota = cv_bot_quota.GetInt() - 1;
			cv_bot_quota.SetValue( clamp( newQuota, 0, cv_bot_quota.GetInt() ) );
			return;
		}
	}

	// adjust bot quota so kicked bot is not immediately added back in
	if ( all && (!name || !*name) && difficulty == NUM_DIFFICULTY_LEVELS )
	{
		cv_bot_quota.SetValue( 0 );
	}
	else
	{
		int newQuota = cv_bot_quota.GetInt() - collector.m_bots.Count();
		cv_bot_quota.SetValue( clamp( newQuota, 0, cv_bot_quota.GetInt() ) );
	}
}


//--------------------------------------------------------------------------------------------------------------
CON_COMMAND_F( bot_crowbars_only, "Restricts the bots to only using Crowbars", FCVAR_GAMEDLL )
{
	if ( !UTIL_IsCommandIssuedByServerAdmin() )
		return;

	cv_bot_allow_pistols.SetValue( 0 );
	cv_bot_allow_shotguns.SetValue( 0 );
	cv_bot_allow_sub_machine_guns.SetValue( 0 );
	cv_bot_allow_rifles.SetValue( 0 );
	cv_bot_allow_machine_guns.SetValue( 0 );
}


//--------------------------------------------------------------------------------------------------------------
CON_COMMAND_F( bot_pistols_only, "Restricts the bots to only using pistols", FCVAR_GAMEDLL )
{
	if ( !UTIL_IsCommandIssuedByServerAdmin() )
		return;

	cv_bot_allow_pistols.SetValue( 1 );
	cv_bot_allow_shotguns.SetValue( 0 );
	cv_bot_allow_sub_machine_guns.SetValue( 0 );
	cv_bot_allow_rifles.SetValue( 0 );
	cv_bot_allow_machine_guns.SetValue( 0 );
}


//--------------------------------------------------------------------------------------------------------------
CON_COMMAND_F( bot_all_weapons, "Allows the bots to use all weapons", FCVAR_GAMEDLL )
{
	if ( !UTIL_IsCommandIssuedByServerAdmin() )
		return;

	cv_bot_allow_pistols.SetValue( 1 );
	cv_bot_allow_shotguns.SetValue( 1 );
	cv_bot_allow_sub_machine_guns.SetValue( 1 );
	cv_bot_allow_rifles.SetValue( 1 );
	cv_bot_allow_machine_guns.SetValue( 1 );
}


//--------------------------------------------------------------------------------------------------------------
CON_COMMAND_F( bot_goto_mark, "Sends a bot to the selected nav area (useful for testing navigation meshes)", FCVAR_GAMEDLL | FCVAR_CHEAT )
{
	if ( !UTIL_IsCommandIssuedByServerAdmin() )
		return;

	// tell the first bot we find to go to our marked area
	CNavArea *area = TheNavMesh->GetMarkedArea();
	if (area)
	{
		for ( int i = 1; i <= gpGlobals->maxClients; ++i )
		{
			CBasePlayer *player = static_cast<CBasePlayer *>( UTIL_PlayerByIndex( i ) );

			if (player == NULL)
				continue;
	
			if (player->IsBot())
			{
				CCSBot *bot = dynamic_cast<CCSBot *>( player );

				if ( bot )
				{
					bot->MoveTo( area->GetCenter(), FASTEST_ROUTE );
				}

				break;
			}
		}
	}
}


//--------------------------------------------------------------------------------------------------------------
#if 0
CON_COMMAND_F( bot_memory_usage, "Reports on the bots' memory usage", FCVAR_GAMEDLL )
{
	if ( !UTIL_IsCommandIssuedByServerAdmin() )
		return;

	Msg( "Memory usage:\n" );

	Msg( "  %d bytes per bot\n", sizeof(CCSBot) );

	Msg( "  %d Navigation Areas @ %d bytes each = %d bytes\n", 
					TheNavMesh->GetNavAreaCount(),
					sizeof( CNavArea ),
					TheNavMesh->GetNavAreaCount() * sizeof( CNavArea ) );
}
#endif


bool CCSBotManager::ServerCommand( const char *cmd )
{
	return false;
}


bool CCSBotManager::ClientCommand( CBasePlayer *player, const CCommand &args )
{
	return false;
}


/**
 * Process the "bot_add" console command
 */
bool CCSBotManager::BotAddCommand( bool isFromConsole, const char *profileName, BotDifficultyType difficulty )
{
	if ( !TheNavMesh->IsLoaded() )
	{
		// If there isn't a Navigation Mesh in memory, create one
		if ( !TheNavMesh->IsGenerating() )
		{
			if ( !m_isMapDataLoaded )
			{
				TheNavMesh->BeginGeneration();
				m_isMapDataLoaded = true;
			}
			return false;
		}
	}

	// dont allow bots to join if the Navigation Mesh is being generated
	if (TheNavMesh->IsGenerating())
		return false;

	const BotProfile *profile = NULL;

	if ( !isFromConsole )
	{
		profileName = NULL;
		difficulty = GetDifficultyLevel();
	}
	else
	{
		if ( difficulty == NUM_DIFFICULTY_LEVELS )
		{
			difficulty = GetDifficultyLevel();
		}
	}

	if ( profileName && *profileName )
	{
		bool ignoreHumans = false;
		if (UTIL_IsNameTaken( profileName, ignoreHumans ))
		{
			if ( isFromConsole )
			{
				Msg( "Error - %s is already in the game.\n", profileName );
			}
			return true;
		}

		// try to add a bot by name
		profile = TheBotProfiles->GetProfile( profileName );
		if ( !profile )
		{
			// try to add a bot by template
			profile = TheBotProfiles->GetProfileMatchingTemplate( profileName, difficulty );
			if ( !profile )
			{
				if ( isFromConsole )
				{
					Msg( "Error - no profile for '%s' exists.\n", profileName );
				}
				return true;
			}
		}
	}
	else
	{
		profile = TheBotProfiles->GetRandomProfile( difficulty );
		if (profile == NULL)
		{
			if ( isFromConsole )
			{
				Msg( "Couldn't get bot profile - either all profiles at this difficulty level are in use, or the profiles file is broken.\n" );
			}
			//return true; Testing... testing... *clears throat*
		}
	}

	// create the actual bot
	CCSBot *bot = CreateBot<CCSBot>( profile );

	if (bot == NULL)
	{
		if ( isFromConsole )
		{
			Msg( "Error: CreateBot() failed.\n" );
		}
		return false;
	}

	if (isFromConsole)
	{
		// increase the bot quota to account for manually added bot
		cv_bot_quota.SetValue( cv_bot_quota.GetInt() + 1 );
	}

	return true;
}

int UTIL_CSSBotsInGame()
{
	int count = 0;

	for (int i = 1; i <= gpGlobals->maxClients; ++i )
	{
		CCSBot *player = dynamic_cast<CCSBot *>(UTIL_PlayerByIndex( i ));

		if ( player == NULL )
			continue;

		count++;
	}

	return count;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Keep a minimum quota of bots in the game
 */
void CCSBotManager::MaintainBotQuota( void )
{
	if ( !AreBotsAllowed() )
		return;

	if (TheNavMesh->IsGenerating())
		return;

	int totalHumansInGame = UTIL_HumansInGame();
	int humanPlayersInGame = UTIL_HumansInGame();

	// don't add bots until local player has been registered, to make sure he's player ID #1
	if (!engine->IsDedicatedServer() && totalHumansInGame == 0)
		return;

	int desiredBotCount = cv_bot_quota.GetInt();
	int botsInGame = UTIL_CSSBotsInGame();

	if ( FStrEq( cv_bot_quota_mode.GetString(), "fill" ) )
	{
		// If bot_quota_mode is 'fill', we want the number of bots and humans together to equal bot_quota
		// unless the round is already in progress, in which case we play with what we've been dealt
		
		desiredBotCount = max( 0, desiredBotCount - humanPlayersInGame );
	}
	else if ( FStrEq( cv_bot_quota_mode.GetString(), "match" ) )
	{
		// If bot_quota_mode is 'match', we want the number of bots to be bot_quota * total humans
		// unless the round is already in progress, in which case we play with what we've been dealt
		
		desiredBotCount = (int)max( 0, cv_bot_quota.GetFloat() * humanPlayersInGame );
	}

	// wait for a player to join, if necessary
	if (cv_bot_join_after_player.GetBool())
	{
		if (humanPlayersInGame == 0)
			desiredBotCount = 0;
	}

	// if bots will auto-vacate, we need to keep one slot open to allow players to join
	if (cv_bot_auto_vacate.GetBool())
		desiredBotCount = min( desiredBotCount, gpGlobals->maxClients - (humanPlayersInGame + 1) );
	else
		desiredBotCount = min( desiredBotCount, gpGlobals->maxClients - humanPlayersInGame );
	
	// add bots if necessary
	if (desiredBotCount > botsInGame)
	{
		TheCSBots()->BotAddCommand();
	}
	else if (desiredBotCount < botsInGame)
	{
		// kick a bot to maintain quota
		
		UTIL_KickBot();
	}
}

//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnServerShutdown( IGameEvent *event )
{
	if ( !engine->IsDedicatedServer() )
	{
		// Since we're a listenserver, save some config info for the next time we start up
		static const char *botVars[] =
		{
			"bot_quota",
			"bot_difficulty",
			"bot_prefix",
			"bot_defer_to_human",
			"bot_join_after_player",
			"bot_allow_pistols",
			"bot_allow_shotguns",
			"bot_allow_sub_machine_guns",
			"bot_allow_machine_guns",
			"bot_allow_rifles"
		};
		
		KeyValues *data = new KeyValues( "ServerConfig" );

		// load the config data
		if (data)
		{
			data->LoadFromFile( filesystem, "ServerConfig.vdf", "GAME" );
			for ( int i=0; i<sizeof(botVars)/sizeof(botVars[0]); ++i )
			{
				const char *varName = botVars[i];
				if ( varName )
				{
					ConVar *var = cvar->FindVar( varName );
					if ( var )
					{
						data->SetString( varName, var->GetString() );
					}
				}
			}
			data->SaveToFile( filesystem, "ServerConfig.vdf", "GAME" );
			data->deleteThis();
		}
		return;
	}
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnPlayerFootstep( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnPlayerFootstep, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnPlayerDeath( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnPlayerDeath, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnPlayerFallDamage( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnPlayerFallDamage, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnRoundEnd( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnRoundEnd, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnRoundStart( IGameEvent *event )
{
	RestartRound();

	CCSBOTMANAGER_ITERATE_BOTS( OnRoundStart, event );
}


//--------------------------------------------------------------------------------------------------------------
static CBaseEntity * SelectSpawnSpot( const char *pEntClassName )
{
	CBaseEntity* pSpot = NULL;

	// Find the next spawn spot.
	pSpot = gEntList.FindEntityByClassname( pSpot, pEntClassName );

	if ( pSpot == NULL ) // skip over the null point
		pSpot = gEntList.FindEntityByClassname( pSpot, pEntClassName );

	CBaseEntity *pFirstSpot = pSpot;
	do 
	{
		if ( pSpot )
		{
			// check if pSpot is valid
			if ( pSpot->GetAbsOrigin() == Vector( 0, 0, 0 ) )
			{
				pSpot = gEntList.FindEntityByClassname( pSpot, pEntClassName );
				continue;
			}

			// if so, go to pSpot
			return pSpot;
		}
		// increment pSpot
		pSpot = gEntList.FindEntityByClassname( pSpot, pEntClassName );
	} while ( pSpot != pFirstSpot ); // loop if we're not back to the start

	return NULL;
}

//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnRoundFreezeEnd( IGameEvent *event )
{
	bool reenableEvents = m_NavBlockedEvent.IsEnabled();

	m_NavBlockedEvent.Enable( false ); // don't listen to nav_blocked events - there could be several, and we don't have bots pathing
	CUtlVector< CNavArea * >& transientAreas = TheNavMesh->GetTransientAreas();
	for ( int i=0; i<transientAreas.Count(); ++i )
	{
		CNavArea *area = transientAreas[i];
		if ( area->GetAttributes() & NAV_MESH_TRANSIENT )
		{
			area->UpdateBlocked();
		}
	}
	if ( reenableEvents )
	{
		m_NavBlockedEvent.Enable( true );
	}
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnNavBlocked( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnNavBlocked, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnDoorMoving( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnDoorMoving, event );
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Check all nav areas inside the breakable's extent to see if players would now fall through
 */
class CheckAreasOverlappingBreakable
{
public:
	CheckAreasOverlappingBreakable( CBaseEntity *breakable )
	{
		m_breakable = breakable;
		ICollideable *collideable = breakable->GetCollideable();
		collideable->WorldSpaceSurroundingBounds( &m_breakableExtent.lo, &m_breakableExtent.hi );

		const float expand = 10.0f;
		m_breakableExtent.lo += Vector( -expand, -expand, -expand );
		m_breakableExtent.hi += Vector(  expand,  expand,  expand );
	}

	bool operator() ( CNavArea *area )
	{
		const Extent &areaExtent = area->GetExtent();

		if (areaExtent.hi.x >= m_breakableExtent.lo.x && areaExtent.lo.x <= m_breakableExtent.hi.x &&
			areaExtent.hi.y >= m_breakableExtent.lo.y && areaExtent.lo.y <= m_breakableExtent.hi.y &&
			areaExtent.hi.z >= m_breakableExtent.lo.z && areaExtent.lo.z <= m_breakableExtent.hi.z)
		{
			// area overlaps the breakable
			area->CheckFloor( m_breakable );
		}

		return true;
	}

private:
	Extent m_breakableExtent;
	CBaseEntity *m_breakable;
};


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnBreakBreakable( IGameEvent *event )
{
	CheckAreasOverlappingBreakable collector( UTIL_EntityByIndex( event->GetInt( "entindex" ) ) );
	TheNavMesh->ForAllAreas( collector );

	CCSBOTMANAGER_ITERATE_BOTS( OnBreakBreakable, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnBreakProp( IGameEvent *event )
{
	CheckAreasOverlappingBreakable collector( UTIL_EntityByIndex( event->GetInt( "entindex" ) ) );
	TheNavMesh->ForAllAreas( collector );

	CCSBOTMANAGER_ITERATE_BOTS( OnBreakProp, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnWeaponFire( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnWeaponFire, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnWeaponFireOnEmpty( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnWeaponFireOnEmpty, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnWeaponReload( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnWeaponReload, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnWeaponZoom( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnWeaponZoom, event );
}


//--------------------------------------------------------------------------------------------------------------
void CCSBotManager::OnBulletImpact( IGameEvent *event )
{
	CCSBOTMANAGER_ITERATE_BOTS( OnBulletImpact, event );
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Return priority of player (0 = max pri)
 */
unsigned int CCSBotManager::GetPlayerPriority( CBasePlayer *player ) const
{
	const unsigned int lowestPriority = 0xFFFFFFFF;

	if (!player->IsPlayer())
		return lowestPriority;

	// human players have highest priority
	if (!player->IsBot())
		return 0;

	CCSBot *bot = dynamic_cast<CCSBot *>( player );

	if ( !bot )
		return 0;

	// everyone else is ranked by their unique ID (which cannot be zero)
	return 1 + bot->GetID();
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Returns a random spawn point
 */
CBaseEntity *CCSBotManager::GetRandomSpawn() const
{
	CUtlVector< CBaseEntity * > spawnSet;
	CBaseEntity *spot;

	for( spot = gEntList.FindEntityByClassname( NULL, "info_player_deathmatch" );
		 spot;
		 spot = gEntList.FindEntityByClassname( spot, "info_player_deathmatch" ) )
	{
		spawnSet.AddToTail( spot );			
	}

	if (spawnSet.Count() == 0)
	{
		return NULL;
	}

	// select one at random
	int which = RandomInt( 0, spawnSet.Count()-1 );
	return spawnSet[ which ];
}