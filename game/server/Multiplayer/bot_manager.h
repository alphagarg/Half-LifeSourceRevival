//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#ifndef BASE_CONTROL_H
#define BASE_CONTROL_H

#pragma warning( disable : 4530 )					// STL uses exceptions, but we are not compiling with them - ignore warning

extern float g_BotUpkeepInterval;					///< duration between bot upkeeps
extern float g_BotUpdateInterval;					///< duration between bot updates
const int g_BotUpdateSkipCount = 2;					///< number of upkeep periods to skip update

class CNavArea;

//--------------------------------------------------------------------------------------------------------------
/**
 * This class manages all active bots, propagating events to them and updating them.
 */
class CBotManager
{
public:
	CBotManager();
	virtual ~CBotManager();

	CBasePlayer *AllocateAndBindBotEntity( edict_t *ed );			///< allocate the appropriate entity for the bot and bind it to the given edict
	virtual CBasePlayer *AllocateBotEntity( void ) = 0;				///< factory method to allocate the appropriate entity for the bot 

	virtual void ClientDisconnect( CBaseEntity *entity ) = 0;
	virtual bool ClientCommand( CBasePlayer *player, const CCommand &args ) = 0;

	virtual void ServerActivate( void ) = 0;
	virtual void ServerDeactivate( void ) = 0;
	virtual bool ServerCommand( const char * pcmd ) = 0;

	virtual void RestartRound( void );							///< (EXTEND) invoked when a new round begins
	virtual void StartFrame( void );							///< (EXTEND) called each frame

	virtual unsigned int GetPlayerPriority( CBasePlayer *player ) const = 0;	///< return priority of player (0 = max pri)	
	
	enum { MAX_DBG_MSG_SIZE = 1024 };
	struct DebugMessage
	{
		char m_string[ MAX_DBG_MSG_SIZE ];
		IntervalTimer m_age;
	};

	// debug message history -------------------------------------------------------------------------------
	int GetDebugMessageCount( void ) const;						///< get number of debug messages in history
	const DebugMessage *GetDebugMessage( int which = 0 ) const;	///< return the debug message emitted by the bot (0 = most recent)
	void ClearDebugMessages( void );
	void AddDebugMessage( const char *msg );


private:
	enum { MAX_DBG_MSGS = 6 };
	DebugMessage m_debugMessage[ MAX_DBG_MSGS ];				///< debug message history
	int m_debugMessageCount;
	int m_currentDebugMessage;

	IntervalTimer m_frameTimer;									///< for measuring each frame's duration
};


inline CBasePlayer *CBotManager::AllocateAndBindBotEntity( edict_t *ed )
{
	CBasePlayer::s_PlayerEdict = ed;
	return AllocateBotEntity();
}

inline int CBotManager::GetDebugMessageCount( void ) const
{
	return m_debugMessageCount;
}

inline const CBotManager::DebugMessage *CBotManager::GetDebugMessage( int which ) const
{
	if (which >= m_debugMessageCount)
		return NULL;

	int i = m_currentDebugMessage - which;
	if (i < 0)
		i += MAX_DBG_MSGS;

	return &m_debugMessage[ i ];
}





// global singleton to create and control bots
extern CBotManager *TheBots;


#endif
