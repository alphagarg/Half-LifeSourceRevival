//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#include "cbase.h"
#include "cs_bot.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//--------------------------------------------------------------------------------------------------------------
/**
 * Begin the hunt
 */
void HuntState::OnEnter( CCSBot *me )
{
	// lurking death
	if (me->IsUsingCrowbar() && !me->IsHurrying())
		me->Walk();
	else
		me->Run();


	me->StandUp();
	me->SetDisposition( CCSBot::ENGAGE_AND_INVESTIGATE );
	me->SetTask( CCSBot::SEEK_AND_DESTROY );

	me->DestroyPath();
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Hunt down our enemies
 */
void HuntState::OnUpdate( CCSBot *me )
{
	// if we've been hunting for a long time, drop into Idle for a moment to
	// select something else to do
	const float huntingTooLongTime = 30.0f;
	if (gpGlobals->curtime - me->GetStateTimestamp() > huntingTooLongTime)
	{
		// stop hunting, there must not be many enemies left to hunt
		me->PrintIfWatched( "Giving up hunting.\n" );
		me->Idle();
		return;
	}

	// listen for enemy noises
	if (me->HeardInterestingNoise())
	{
		me->InvestigateNoise();
		return;
	}
		
	// look around
	me->UpdateLookAround();

	// if we have reached our destination area, pick a new one
	// if our path fails, pick a new one
	if (me->GetLastKnownArea() == m_huntArea || me->UpdatePathMovement() != CCSBot::PROGRESSING)
	{
		// pick a new hunt area
		m_huntArea = NULL;
			
		int areaCount = 0;
			
		// pick an area at random
		int which = RandomInt( 0, areaCount-1 );

		areaCount = 0;
		FOR_EACH_LL( TheNavAreaList, hit )
		{
			m_huntArea = TheNavAreaList[ hit ];

			if (which == areaCount)
				break;

			--which;
		}

		if (m_huntArea)
		{
			// create a new path to a far away area of the map
			me->ComputePath( m_huntArea->GetCenter() );
		}
	}
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Done hunting
 */
void HuntState::OnExit( CCSBot *me )
{
}
