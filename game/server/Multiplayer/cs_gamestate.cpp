//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Encapsulation of the current game state. Allows each bot imperfect knowledge.
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#include "cbase.h"
#include "KeyValues.h"

#include "cs_bot.h"
#include "cs_gamestate.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//--------------------------------------------------------------------------------------------------------------
CSGameState::CSGameState( CCSBot *owner )
{
}

void CSGameState::Reset( void )
{
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Update game state based on events we have received
 */
void CSGameState::OnRoundStart( IGameEvent *event )
{
	Reset();
}