//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#ifndef _GAME_STATE_H_
#define _GAME_STATE_H_


#include "Multiplayer/bot_util.h"


class CCSBot;

/**
 * This class represents the game state as known by a particular bot
 */
class CSGameState
{
public:
	CSGameState( CCSBot *owner );

	void Reset( void );

	// Event handling
	void OnRoundEnd( IGameEvent *event );
	void OnRoundStart( IGameEvent *event );
};

#endif // _GAME_STATE_
