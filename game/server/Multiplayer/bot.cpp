//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), Leon Hartwig, 2003

#include "cbase.h"

#include "Multiplayer/bot.h"
#include "Multiplayer/bot_util.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

/// @todo Remove this nasty hack - CreateFakeClient() calls CBot::Spawn, which needs the profile
const BotProfile *g_botInitProfile = NULL;

//
// NOTE: Because CBot had to be templatized, the code was moved into bot.h
//