//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#include "cbase.h"
#include "cs_bot.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//--------------------------------------------------------------------------------------------------------------
/**
 * Fire our active weapon towards our current enemy
 * NOTE: Aiming our weapon is handled in RunBotUpkeep()
 */
void CCSBot::FireWeaponAtEnemy( void )
{
	if (cv_bot_dont_shoot.GetBool())
	{
		return;
	}

	CBasePlayer *enemy = GetBotEnemy();
	if (enemy == NULL)
	{
		return;
	}

	Vector myOrigin = GetCentroid( this );

	if (gpGlobals->curtime > m_fireWeaponTimestamp &&
		GetTimeSinceAcquiredCurrentEnemy() >= GetProfile()->GetAttackDelay() &&
		!IsSurprised())
	{
		if (!IsActiveWeaponClipEmpty() && 
			//gpGlobals->curtime > m_reacquireTimestamp &&
			IsEnemyVisible())
		{
			// we have a clear shot - pull trigger if we are aiming at enemy
			Vector toAimSpot = m_aimSpot - EyePosition();
			float rangeToEnemy = toAimSpot.NormalizeInPlace();

			// get actual view direction vector
			Vector aimDir = GetViewVector();

			float onTarget = DotProduct( toAimSpot, aimDir );

			// because rifles' bullets spray, dont have to be very precise
			const float halfSize = (false) ? HalfHumanWidth : 2.0f * HalfHumanWidth;

			// aiming tolerance depends on how close the target is - closer targets subtend larger angles
			float aimTolerance = (float)cos( atan( halfSize / rangeToEnemy ) );

			if (onTarget > aimTolerance)
			{
				// if we are using a Crowbar, only swing it if we're close
				if (IsUsingCrowbar())
				{
					const float CrowbarRange = 75.0f;		// 50
					if (rangeToEnemy < CrowbarRange)
					{
						// since we've given ourselves away - run!
						ForceRun( 5.0f );

						// if our prey is facing away, backstab him!
						if (!IsPlayerFacingMe( enemy ))
						{
							SecondaryAttack();
						}
						else
						{
							// randomly choose primary and secondary attacks with Crowbar
							const float CrowbarStabChance = 33.3f;
							if (RandomFloat( 0, 100 ) < CrowbarStabChance)
								SecondaryAttack();
							else
								PrimaryAttack();
						}
					}
				}
				else
				{
					PrimaryAttack();
				}

				if (IsUsingPistol())
				{
					// high-skill bots fire their pistols quickly at close range
					const float closePistolRange = 999999.9f;
					if (GetProfile()->GetSkill() > 0.75f && rangeToEnemy < closePistolRange)
					{
						// fire as fast as possible
						m_fireWeaponTimestamp = 0.0f; 
					}
					else
					{
						// fire somewhat quickly
						m_fireWeaponTimestamp = RandomFloat( 0.15f, 0.4f );
					}
				}
				else	// not using a pistol
				{
					const float sprayRange = 400.0f;
					if (GetProfile()->GetSkill() < 0.5f || rangeToEnemy < sprayRange)
					{
						// spray 'n pray if enemy is close, or we're not that good, or we're using the big machinegun
						m_fireWeaponTimestamp = 0.0f;
					}
					else
					{
						// fire short bursts for accuracy
						m_fireWeaponTimestamp = RandomFloat( 0.15f, 0.25f );		// 0.15, 0.5
					}
				}

				// subtract system latency
				m_fireWeaponTimestamp -= g_BotUpdateInterval;

				m_fireWeaponTimestamp += gpGlobals->curtime;
			}
		}
	}
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Set the current aim offset using given accuracy (1.0 = perfect aim, 0.0f = terrible aim)
 */
void CCSBot::SetAimOffset( float accuracy )
{
	// if our accuracy is less than perfect, it will improve as we "focus in" while not rotating our view
	if (accuracy < 1.0f)
	{
		// if we moved our view, reset our "focus" mechanism
		if (IsViewMoving( 100.0f ))
			m_aimSpreadTimestamp = gpGlobals->curtime;

		// focusTime is the time it takes for a bot to "focus in" for very good aim, from 2 to 5 seconds
		const float focusTime = max( 5.0f * (1.0f - accuracy), 2.0f );
		float focusInterval = gpGlobals->curtime - m_aimSpreadTimestamp;

		float focusAccuracy = focusInterval / focusTime;

		// limit how much "focus" will help
		const float maxFocusAccuracy = 0.75f;
		if (focusAccuracy > maxFocusAccuracy)
			focusAccuracy = maxFocusAccuracy;

		accuracy = max( accuracy, focusAccuracy );
	}

	//PrintIfWatched( "Accuracy = %4.3f\n", accuracy );

	// aim error increases with distance, such that actual crosshair error stays about the same
	float range = (m_lastEnemyPosition - EyePosition()).Length();
	float maxOffset = (GetFOV()/GetDefaultFOV()) * 0.05f * range;		// 0.1
	float error = maxOffset * (1.0f - accuracy);

	m_aimOffsetGoal.x = RandomFloat( -error, error );
	m_aimOffsetGoal.y = RandomFloat( -error, error );
	m_aimOffsetGoal.z = RandomFloat( -error, error );

	// define time when aim offset will automatically be updated
	m_aimOffsetTimestamp = gpGlobals->curtime + RandomFloat( 0.25f, 1.0f ); // 0.25, 1.5f
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Wiggle aim error based on GetProfile()->GetSkill()
 */
void CCSBot::UpdateAimOffset( void )
{
	if (gpGlobals->curtime >= m_aimOffsetTimestamp)
	{
		SetAimOffset( GetProfile()->GetSkill() );
	}

	// move current offset towards goal offset
	Vector d = m_aimOffsetGoal - m_aimOffset;
	const float stiffness = 0.1f;
	m_aimOffset.x += stiffness * d.x;
	m_aimOffset.y += stiffness * d.y;
	m_aimOffset.z += stiffness * d.z;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if we are using a sniper rifle
 */
bool CCSBot::IsUsing( const char *wpnName ) const
{
	CBaseHL1CombatWeapon *weapon = GetActiveHL1Weapon();

	if ( weapon && V_stricmp(weapon->GetClassname(), wpnName ) == 0 )
		return true;

	return false;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if primary weapon doesn't exist or is totally out of ammo
 */
bool CCSBot::IsPrimaryWeaponEmpty( void ) const
{
	CBaseHL1MPCombatWeapon *weapon = static_cast<CBaseHL1MPCombatWeapon *>( Weapon_GetSlot( WEAPON_SLOT_RIFLE ) );

	if (weapon == NULL)
		return true;

	// check if gun has any ammo left
	if (weapon->HasAnyAmmo())
		return false;

	return true;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if pistol doesn't exist or is totally out of ammo
 */
bool CCSBot::IsPistolEmpty( void ) const
{
	CBaseHL1MPCombatWeapon *weapon = static_cast<CBaseHL1MPCombatWeapon *>( Weapon_GetSlot( WEAPON_SLOT_PISTOL ) );

	if (weapon == NULL)
		return true;

	// check if gun has any ammo left
	if (weapon->HasAnyAmmo())
		return false;

	return true;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Equip the given item
 */
bool CCSBot::DoEquip( CBaseHL1MPCombatWeapon *weapon )
{
	if (weapon == NULL)
		return false;

	// check if weapon has any ammo left
	if (!weapon->HasAnyAmmo())
		return false;

	// equip it
	SelectItem( weapon->GetClassname() );
	m_equipTimer.Start();

	return true;
}


// throttle how often equipping is allowed
const float minEquipInterval = 5.0f;


//--------------------------------------------------------------------------------------------------------------
/**
 * Equip the best weapon we are carrying that has ammo
 */
void CCSBot::EquipBestWeapon( bool mustEquip )
{
	// throttle how often equipping is allowed
	if (!mustEquip && m_equipTimer.GetElapsedTime() < minEquipInterval)
		return;

	CCSBotManager *ctrl = static_cast<CCSBotManager *>( TheBots );

	CBaseHL1MPCombatWeapon *primary = static_cast<CBaseHL1MPCombatWeapon *>( Weapon_GetSlot( WEAPON_SLOT_RIFLE ) );
	if (primary)
	{
		if (DoEquip( primary ))
			return;
	}

	if (ctrl->AllowPistols())
	{
		if (DoEquip( static_cast<CBaseHL1MPCombatWeapon *>( Weapon_GetSlot( WEAPON_SLOT_PISTOL ) ) ))
			return;
	}

	// always have a Crowbar
	EquipCrowbar();
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Equip our pistol
 */
void CCSBot::EquipPistol( void )
{
	// throttle how often equipping is allowed
	if (m_equipTimer.GetElapsedTime() < minEquipInterval)
		return;

	if (TheCSBots()->AllowPistols() && !IsUsingPistol())
	{
		CBaseHL1MPCombatWeapon *pistol = static_cast<CBaseHL1MPCombatWeapon *>( Weapon_GetSlot( WEAPON_SLOT_PISTOL ) );
		DoEquip( pistol );
	}
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Equip the Crowbar
 */
void CCSBot::EquipCrowbar( void )
{
	if (!IsUsingCrowbar())
	{
		SelectItem( "weapon_Crowbar" );
	}
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Returns true if we have Crowbar equipped
 */
bool CCSBot::IsUsingCrowbar( void ) const
{
	CBaseHL1MPCombatWeapon *weapon = GetActiveHL1Weapon();

	if (weapon && V_stricmp (weapon->GetClassname(), "weapon_crowbar") == 0)
		return true;

	return false;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Returns true if we have pistol equipped
 */
bool CCSBot::IsUsingPistol( void ) const
{
	CBaseHL1MPCombatWeapon *weapon = GetActiveHL1Weapon();

	if (weapon && V_stricmp (weapon->GetClassname(), "weapon_glock") == 0 || V_stricmp (weapon->GetClassname(), "weapon_357") == 0)
		return true;

	return false;
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Returns true if our weapon can attack
 */
bool CCSBot::CanActiveWeaponFire( void ) const
{
	return ( GetActiveWeapon() && GetActiveWeapon()->m_flNextPrimaryAttack <= gpGlobals->curtime );
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Reload our weapon if we must
 */
void CCSBot::ReloadCheck( void )
{
	const float safeReloadWaitTime = 3.0f;
	const float reloadAmmoRatio = 0.6f;

	// don't bother to reload if there are no enemies left
	if (GetEnemiesRemaining() == 0)
		return;

	if (IsActiveWeaponClipEmpty())
	{
		// high-skill players switch to pistol instead of reloading during combat
		if (GetProfile()->GetSkill() > 0.5f && IsAttacking())
		{
			if ( (!V_stricmp (GetActiveWeapon()->GetClassname(), "weapon_glock") == 0 || V_stricmp (GetActiveWeapon()->GetClassname(), "weapon_357") == 0) && !IsPistolEmpty())
			{
				// switch to pistol instead of reloading
				EquipPistol();
				return;
			}
		}
	}
	else if (GetTimeSinceLastSawEnemy() > safeReloadWaitTime && GetActiveWeaponAmmoRatio() <= reloadAmmoRatio)
	{
		// high-skill players use all their ammo and switch to pistol instead of reloading during combat
		if (GetProfile()->GetSkill() > 0.5f && IsAttacking())
			return;
	}
	else
	{
		// do not need to reload
		return;
	}

	Reload();
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Invoked when in contact with a CBaseCombatWeapon
 */
bool CCSBot::BumpWeapon( CBaseCombatWeapon *pWeapon )
{
	CBaseHL1MPCombatWeapon *droppedGun = dynamic_cast< CBaseHL1MPCombatWeapon* >( pWeapon );

	return BaseClass::BumpWeapon( droppedGun );
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Return line-of-sight distance to obstacle along weapon fire ray
 */
float CCSBot::ComputeWeaponSightRange( void )
{
	// compute the unit vector along our view
	Vector aimDir = GetViewVector();

	// trace the bullet's path
	trace_t result;
	UTIL_TraceLine( EyePosition(), EyePosition() + 10000.0f * aimDir, MASK_PLAYERSOLID, this, COLLISION_GROUP_NONE, &result );

	return (EyePosition() - result.endpos).Length();
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Return true if the given player just fired their weapon
 */
bool CCSBot::DidPlayerJustFireWeapon( const CHL1MP_Player *player ) const
{
	// if this player has just fired his weapon, we notice him
	CBaseHL1MPCombatWeapon *weapon = dynamic_cast< CBaseHL1MPCombatWeapon* >( player->GetActiveWeapon() );
	return (weapon && weapon->m_flNextPrimaryAttack > gpGlobals->curtime);
}

