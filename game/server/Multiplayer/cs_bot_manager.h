//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#ifndef CS_CONTROL_H
#define CS_CONTROL_H


#include "Multiplayer/bot_manager.h"
#include "nav_area.h"
#include "Multiplayer/bot_util.h"
#include "Multiplayer/bot_profile.h"
#include "hl1_shareddefs.h"
#include "hl1mp_player.h"

class CBasePlayerWeapon;

class CCSBotManager;

// accessor for CS-specific bots
inline CCSBotManager *TheCSBots( void )
{
	return reinterpret_cast< CCSBotManager * >( TheBots );
}

//--------------------------------------------------------------------------------------------------------------
class BotEventInterface : public IGameEventListener2
{
public:
	virtual const char *GetEventName( void ) const = 0;
};

//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
/**
 * Macro to set up an OnEventClass() in TheCSBots.
 */
#define DECLARE_BOTMANAGER_EVENT_LISTENER( BotManagerSingleton, EventClass, EventName ) \
	public: \
	virtual void On##EventClass( IGameEvent *data ); \
	private: \
	class EventClass##Event : public BotEventInterface \
	{ \
		bool m_enabled; \
	public: \
		EventClass##Event( void ) \
		{ \
			gameeventmanager->AddListener( this, #EventName, true ); \
			m_enabled = true; \
		} \
		~EventClass##Event( void ) \
		{ \
			if ( m_enabled ) gameeventmanager->RemoveListener( this ); \
		} \
		virtual const char *GetEventName( void ) const \
		{ \
			return #EventName; \
		} \
		void Enable( bool enable ) \
		{ \
			m_enabled = enable; \
			if ( enable ) \
				gameeventmanager->AddListener( this, #EventName, true ); \
			else \
				gameeventmanager->RemoveListener( this ); \
		} \
		bool IsEnabled( void ) const { return m_enabled; } \
		void FireGameEvent( IGameEvent *event ) \
		{ \
			BotManagerSingleton()->On##EventClass( event ); \
		} \
	}; \
	EventClass##Event m_##EventClass##Event;


//--------------------------------------------------------------------------------------------------------------
#define DECLARE_CSBOTMANAGER_EVENT_LISTENER( EventClass, EventName ) DECLARE_BOTMANAGER_EVENT_LISTENER( TheCSBots, EventClass, EventName )


//--------------------------------------------------------------------------------------------------------------
/**
 * Macro to propogate an event from the bot manager to all bots
 */
#define CCSBOTMANAGER_ITERATE_BOTS( Callback, arg1 ) \
	{ \
		for ( int idx = 1; idx <= gpGlobals->maxClients; ++idx ) \
		{ \
			CBasePlayer *player = UTIL_PlayerByIndex( idx ); \
			if (player == NULL) continue; \
			if (!player->IsBot()) continue; \
			CCSBot *bot = dynamic_cast< CCSBot * >(player); \
			if ( !bot ) continue; \
			bot->Callback( arg1 ); \
		} \
	}


//--------------------------------------------------------------------------------------------------------------
//
// The manager for Counter-Strike specific bots
//
class CCSBotManager : public CBotManager
{
public:
	CCSBotManager();

	virtual CBasePlayer *AllocateBotEntity( void );			///< factory method to allocate the appropriate entity for the bot

	virtual void ClientDisconnect( CBaseEntity *entity );
	virtual bool ClientCommand( CBasePlayer *player, const CCommand &args );

	virtual void ServerActivate( void );
	virtual void ServerDeactivate( void );
	virtual bool ServerCommand( const char *cmd );
	bool IsServerActive( void ) const { return m_serverActive; }

	virtual void RestartRound( void );						///< (EXTEND) invoked when a new round begins
	virtual void StartFrame( void );						///< (EXTEND) called each frame

	virtual unsigned int GetPlayerPriority( CBasePlayer *player ) const;	///< return priority of player (0 = max pri)
	
	// difficulty levels -----------------------------------------------------------------------------------------
	static BotDifficultyType GetDifficultyLevel( void )		
	{ 
		if (cv_bot_difficulty.GetFloat() < 0.9f)
			return BOT_EASY;
		if (cv_bot_difficulty.GetFloat() < 1.9f)
			return BOT_NORMAL;
		if (cv_bot_difficulty.GetFloat() < 2.9f)
			return BOT_HARD;

		return BOT_EXPERT;
	}

	/// returns a random spawn point
	CBaseEntity *GetRandomSpawn() const;
	
	float GetLastSeenEnemyTimestamp( void ) const	{ return m_lastSeenEnemyTimestamp; }	///< return the last time anyone has seen an enemy
	void SetLastSeenEnemyTimestamp( void ) 			{ m_lastSeenEnemyTimestamp = gpGlobals->curtime; }

	bool AllowPistols( void ) const					{ return cv_bot_allow_pistols.GetBool(); }
	bool AllowShotguns( void ) const				{ return cv_bot_allow_shotguns.GetBool(); }
	bool AllowSubMachineGuns( void ) const			{ return cv_bot_allow_sub_machine_guns.GetBool(); }
	bool AllowRifles( void ) const					{ return cv_bot_allow_rifles.GetBool(); }
	bool AllowMachineGuns( void ) const				{ return cv_bot_allow_machine_guns.GetBool(); }
	
	bool IsWeaponUseable( const CBaseHL1MPCombatWeapon *weapon ) const;	///< return true if the bot can use this weapon

	#define FROM_CONSOLE true
	bool BotAddCommand( bool isFromConsole = false, const char *profileName = NULL, BotDifficultyType difficulty = NUM_DIFFICULTY_LEVELS );	///< process the "bot_add" console command

private:
	enum SkillType { LOW, AVERAGE, HIGH, RANDOM };

	void MaintainBotQuota( void );

	static bool m_isMapDataLoaded;							///< true if we've attempted to load map data
	bool m_serverActive;									///< true between ServerActivate() and ServerDeactivate()

	CountdownTimer m_checkTransientAreasTimer;				///< when elapsed, all transient nav areas should be checked for blockage

	float m_lastSeenEnemyTimestamp;
	
	// Event Handlers --------------------------------------------------------------------------------------------
	DECLARE_CSBOTMANAGER_EVENT_LISTENER( PlayerFootstep,		player_footstep )
	DECLARE_CSBOTMANAGER_EVENT_LISTENER( PlayerDeath,			player_death )
	DECLARE_CSBOTMANAGER_EVENT_LISTENER( PlayerFallDamage,		player_falldamage )

	DECLARE_CSBOTMANAGER_EVENT_LISTENER( RoundEnd,				round_end )
	DECLARE_CSBOTMANAGER_EVENT_LISTENER( RoundStart,			round_start )
	DECLARE_CSBOTMANAGER_EVENT_LISTENER( RoundFreezeEnd,		round_freeze_end )

	DECLARE_CSBOTMANAGER_EVENT_LISTENER( DoorMoving,			door_moving )

	DECLARE_CSBOTMANAGER_EVENT_LISTENER( BreakProp,				break_prop )
	DECLARE_CSBOTMANAGER_EVENT_LISTENER( BreakBreakable,		break_breakable )

	DECLARE_CSBOTMANAGER_EVENT_LISTENER( WeaponFire,			weapon_fire )
	DECLARE_CSBOTMANAGER_EVENT_LISTENER( WeaponFireOnEmpty,		weapon_fire_on_empty )
	DECLARE_CSBOTMANAGER_EVENT_LISTENER( WeaponReload,			weapon_reload )
	DECLARE_CSBOTMANAGER_EVENT_LISTENER( WeaponZoom,			weapon_zoom )

	DECLARE_CSBOTMANAGER_EVENT_LISTENER( BulletImpact,			bullet_impact )

	DECLARE_CSBOTMANAGER_EVENT_LISTENER( NavBlocked,			nav_blocked )

	DECLARE_CSBOTMANAGER_EVENT_LISTENER( ServerShutdown,		server_shutdown )

	CUtlVector< BotEventInterface * > m_commonEventListeners;	// These event listeners fire often, and can be disabled for performance gains when no bots are present.
	bool m_eventListenersEnabled;
	void EnableEventListeners( bool enable );
};

inline CBasePlayer *CCSBotManager::AllocateBotEntity( void )
{
	return static_cast<CBasePlayer *>( CreateEntityByName( "cs_bot" ) );
}

#endif
