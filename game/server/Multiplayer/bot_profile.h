//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#ifndef _BOT_PROFILE_H_
#define _BOT_PROFILE_H_

#pragma warning( disable : 4786 )	// long STL names get truncated in browse info.

#include "Multiplayer/bot_constants.h"
#include "Multiplayer/bot_util.h"

enum
{
	FirstCustomSkin = 100,
	NumCustomSkins = 100,
	LastCustomSkin = FirstCustomSkin + NumCustomSkins - 1,
};

	
//--------------------------------------------------------------------------------------------------------------
/**
 * A BotProfile describes the "personality" of a given bot
 */
class BotProfile
{
public:
	BotProfile( void )
	{
		m_name = NULL;
		m_aggression = 0.0f;
		m_skill = 0.0f;	
		m_difficultyFlags = 0;
		m_reactionTime = 0.3f;
		m_attackDelay = 0.0f;
	}

	~BotProfile( void )
	{
		if ( m_name )
			delete [] m_name;
	}

	const char *GetName( void ) const					{ return m_name; }		///< return bot's name
	float GetAggression( void ) const					{ return m_aggression; }
	float GetSkill( void ) const						{ return m_skill; }
	
	bool IsDifficulty( BotDifficultyType diff ) const;	///< return true if this profile can be used for the given difficulty level
	float GetReactionTime( void ) const					{ return m_reactionTime; }
	float GetAttackDelay( void ) const					{ return m_attackDelay; }
	
	bool InheritsFrom( const char *name ) const;

private:	
	friend class BotProfileManager;						///< for loading profiles

	void Inherit( const BotProfile *parent, const BotProfile *baseline );	///< copy values from parent if they differ from baseline

	char *m_name;										///< the bot's name
	float m_aggression;									///< percentage: 0 = coward, 1 = berserker
	float m_skill;										///< percentage: 0 = terrible, 1 = expert
	
	unsigned char m_difficultyFlags;					///< bits set correspond to difficulty levels this is valid for
	float m_reactionTime;								//< our reaction time in seconds
	float m_attackDelay;								///< time in seconds from when we notice an enemy to when we open fire

	CUtlVector< const BotProfile * > m_templates;		///< List of templates we inherit from
};
typedef CUtlLinkedList<BotProfile *> BotProfileList;


inline bool BotProfile::IsDifficulty( BotDifficultyType diff ) const
{
	return (m_difficultyFlags & (1 << diff)) ? true : false;
}

/**
 * Copy in data from parent if it differs from the baseline
 */
inline void BotProfile::Inherit( const BotProfile *parent, const BotProfile *baseline )
{
	if (parent->m_aggression != baseline->m_aggression)
		m_aggression = parent->m_aggression;

	if (parent->m_skill != baseline->m_skill)
		m_skill = parent->m_skill;

	if (parent->m_difficultyFlags != baseline->m_difficultyFlags)
		m_difficultyFlags = parent->m_difficultyFlags;

	if (parent->m_reactionTime != baseline->m_reactionTime)
		m_reactionTime = parent->m_reactionTime;

	if (parent->m_attackDelay != baseline->m_attackDelay)
		m_attackDelay = parent->m_attackDelay;

	m_templates.AddToTail( parent );
}




//--------------------------------------------------------------------------------------------------------------
/**
 * The BotProfileManager defines the interface to accessing BotProfiles
 */
class BotProfileManager
{
public:
	BotProfileManager( void );
	~BotProfileManager( void );

	void Init( const char *filename, unsigned int *checksum = NULL );
	void Reset( void );

	/// given a name, return a profile
	const BotProfile *GetProfile( const char *name ) const
	{
		FOR_EACH_LL( m_profileList, it )
		{
			BotProfile *profile = m_profileList[ it ];
	
			if ( !stricmp( name, profile->GetName() ) )
				return profile;
		}

		return NULL;
	}

	/// given a template name and difficulty, return a profile
	const BotProfile *GetProfileMatchingTemplate( const char *profileName, BotDifficultyType difficulty ) const
	{
		FOR_EACH_LL( m_profileList, it )
		{
			BotProfile *profile = m_profileList[ it ];

			if ( !profile->InheritsFrom( profileName ) )
				continue;

			if ( !profile->IsDifficulty( difficulty ) )
				continue;

			if ( UTIL_IsNameTaken( profile->GetName() ) )
				continue;

			return profile;
		}

		return NULL;
	}

	const BotProfileList *GetProfileList( void ) const		{ return &m_profileList; }		///< return list of all profiles

	const BotProfile *GetRandomProfile( BotDifficultyType difficulty ) const;			///< return random unused profile that matches the given difficulty level

	const char * GetCustomSkin( int index );				///< Returns custom skin name at a particular index
	const char * GetCustomSkinModelname( int index );		///< Returns custom skin modelname at a particular index
	const char * GetCustomSkinFname( int index );			///< Returns custom skin filename at a particular index
	int GetCustomSkinIndex( const char *name, const char *filename = NULL );	///< Looks up a custom skin index by name
	
protected:
	BotProfileList m_profileList;							///< the list of all bot profiles
	BotProfileList m_templateList;							///< the list of all bot templates

	char *m_skins[ NumCustomSkins ];						///< Custom skin names
	char *m_skinModelnames[ NumCustomSkins ];				///< Custom skin modelnames
	char *m_skinFilenames[ NumCustomSkins ];				///< Custom skin filenames
	int m_nextSkin;											///< Next custom skin to allocate
};

/// the global singleton for accessing BotProfiles
extern BotProfileManager *TheBotProfiles;


#endif // _BOT_PROFILE_H_
