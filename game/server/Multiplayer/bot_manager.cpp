//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#include "cbase.h"

#include "Multiplayer/bot.h"
#include "Multiplayer/bot_manager.h"
#include "nav_area.h"
#include "Multiplayer/bot_util.h"

#include "cs_bot.h"

#include "tier0/vprof.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


float g_BotUpkeepInterval = 0.0f;
float g_BotUpdateInterval = 0.0f;


//--------------------------------------------------------------------------------------------------------------
CBotManager::CBotManager()
{
	InitBotTrig();
}


//--------------------------------------------------------------------------------------------------------------
CBotManager::~CBotManager()
{
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Invoked when the round is restarting
 */
void CBotManager::RestartRound( void )
{
	ClearDebugMessages();
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Invoked at the start of each frame
 */
void CBotManager::StartFrame( void )
{
	VPROF_BUDGET( "CBotManager::StartFrame", VPROF_BUDGETGROUP_NPCS );

	// set frame duration
	g_BotUpkeepInterval = m_frameTimer.GetElapsedTime();
	m_frameTimer.Start();

	g_BotUpdateInterval = (g_BotUpdateSkipCount+1) * g_BotUpkeepInterval;

	//
	// Process each active bot
	//
	for( int i = 1; i <= gpGlobals->maxClients; ++i )
	{
		CBasePlayer *player = static_cast<CBasePlayer *>( UTIL_PlayerByIndex( i ) );

		if (!player)
			continue;

		// Hack for now so the temp bot code works. The temp bots are very useful for debugging
		if (player->IsBot() && IsEntityValid( player ) )
		{
			// EVIL: Messes up vtables
			//CBot< CBasePlayer > *bot = static_cast< CBot< CBasePlayer > * >( player );
			CCSBot *bot = dynamic_cast< CCSBot * >( player );

			if ( bot )
			{
				bot->Upkeep();

				if (((gpGlobals->tickcount + bot->entindex()) % g_BotUpdateSkipCount) == 0)
				{
					bot->ResetCommand();
					bot->Update();
				}

				bot->UpdatePlayer();
			}
		}
	}
}


//--------------------------------------------------------------------------------------------------------------
void CBotManager::ClearDebugMessages( void )
{
	m_debugMessageCount = 0;
	m_currentDebugMessage = -1;
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Add a new debug message to the message history
 */
void CBotManager::AddDebugMessage( const char *msg )
{
	if (++m_currentDebugMessage >= MAX_DBG_MSGS)
	{
		m_currentDebugMessage = 0;
	}

	if (m_debugMessageCount < MAX_DBG_MSGS)
	{
		++m_debugMessageCount;
	}

	Q_strncpy( m_debugMessage[ m_currentDebugMessage ].m_string, msg, MAX_DBG_MSG_SIZE );
	m_debugMessage[ m_currentDebugMessage ].m_age.Start();
}
