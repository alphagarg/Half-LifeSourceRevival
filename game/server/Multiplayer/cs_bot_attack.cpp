//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

// Author: Michael S. Booth (mike@turtlerockstudios.com), 2003

#include "cbase.h"
#include "cs_bot.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//--------------------------------------------------------------------------------------------------------------
/**
 * Begin attacking
 */
void AttackState::OnEnter( CCSBot *me )
{
	CBasePlayer *enemy = me->GetBotEnemy();

	// store our posture when the attack began
	me->PushPostureContext();

	me->DestroyPath();

	// if we are using a Crowbar, try to sneak up on the enemy
	if (enemy && me->IsUsingCrowbar() && !me->IsPlayerFacingMe( enemy ))
		me->Walk();
	else
		me->Run();

	me->GetOffLadder();
	me->ResetStuckMonitor();

	m_repathTimer.Invalidate();
	m_haveSeenEnemy = me->IsEnemyVisible();
	m_nextDodgeStateTimestamp = 0.0f;
	m_firstDodge = true;
	m_reacquireTimestamp = 0.0f;

	m_pinnedDownTimestamp = gpGlobals->curtime + RandomFloat( 7.0f, 10.0f );

	if (me->IsUsingCrowbar())
	{
		// can't crouch and hold with a Crowbar
		m_crouchAndHold = false;
		me->StandUp();
	}

	m_scopeTimestamp = 0;
	m_didAmbushCheck = false;

	float skill = me->GetProfile()->GetSkill();

	// tendency to dodge is proportional to skill
	float dodgeChance = 80.0f * skill;

	m_shouldDodge = (RandomFloat( 0, 100 ) <= dodgeChance);
}

//--------------------------------------------------------------------------------------------------------------
/**
 * When we are done attacking, this is invoked
 */
void AttackState::StopAttacking( CCSBot *me )
{
	me->StopAttacking();
	me->StopAiming();
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Do dodge behavior
 */
void AttackState::Dodge( CCSBot *me )
{
	//
	// Dodge.
	// If crouching, stand still.
	//
	if (m_shouldDodge && !m_crouchAndHold)
	{
		CBasePlayer *enemy = me->GetBotEnemy();
		if (enemy == NULL)
		{
			return;
		}

		Vector toEnemy = enemy->GetAbsOrigin() - me->GetAbsOrigin();
		float range = toEnemy.Length();

		const float hysterisRange = 125.0f;

		float minRange = RandomFloat( 325.0f, 425.0f ) - hysterisRange;
		float maxRange = RandomFloat( 325.0f, 425.0f ) + hysterisRange;

		if (me->IsUsingCrowbar())
		{
			// dodge when far away if armed only with a Crowbar
			maxRange = 999999.9f;
		}

		// move towards (or away from) enemy if we are using a Crowbar, behind a corner, or we aren't very skilled
		if (me->GetProfile()->GetSkill() < 0.66f || !me->IsEnemyVisible())
		{
			if (range > maxRange)
				me->MoveForward();
			else if (range < minRange)
				me->MoveBackward();
		}

		// don't dodge if enemy is facing away
		const float dodgeRange = 2000.0f;
		if ((range > dodgeRange || !me->IsPlayerFacingMe( enemy )))
		{
			m_dodgeState = STEADY_ON;
			m_nextDodgeStateTimestamp = 0.0f;
		}
		else if (gpGlobals->curtime >= m_nextDodgeStateTimestamp)
		{
			int next;

			// high-skill bots keep moving and don't jump
			if (me->GetProfile()->GetSkill() > 0.5f)
			{
				// juke back and forth
				if (m_firstDodge)
				{
					next = (RandomInt( 0, 100 ) < 50) ? SLIDE_RIGHT : SLIDE_LEFT;
				}
				else
				{
					next = (m_dodgeState == SLIDE_LEFT) ? SLIDE_RIGHT : SLIDE_LEFT;
				}
			}
			else
			{
				// select next dodge state that is different that our current one
				do
				{
					// high-skill bots may jump when first engaging the enemy (if they are moving)
					const float jumpChance = 33.3f;
					if (m_firstDodge && me->GetProfile()->GetSkill() > 0.5f && RandomFloat( 0, 100 ) < jumpChance && !me->IsNotMoving())
						next = RandomInt( 0, NUM_ATTACK_STATES-1 );
					else
						next = RandomInt( 0, NUM_ATTACK_STATES-2 );
				}
				while( !m_firstDodge && next == m_dodgeState );
			}

			m_dodgeState = (DodgeStateType)next;
			m_nextDodgeStateTimestamp = gpGlobals->curtime + RandomFloat( 0.3f, 1.0f );
			m_firstDodge = false;
		}


		Vector forward, right;
		me->EyeVectors( &forward, &right );

		const float lookAheadRange = 30.0f;
		float ground;

		switch( m_dodgeState )
		{
			case STEADY_ON:
			{
				break;
			}

			case SLIDE_LEFT:
			{
				// don't move left if we will fall
				Vector pos = me->GetAbsOrigin() - (lookAheadRange * right);

				if (me->GetSimpleGroundHeightWithFloor( pos, &ground ))
				{
					if (me->GetAbsOrigin().z - ground < StepHeight)
					{
						me->StrafeLeft();
					}
				}
				break;
			}

			case SLIDE_RIGHT:
			{
				// don't move left if we will fall
				Vector pos = me->GetAbsOrigin() + (lookAheadRange * right);

				if (me->GetSimpleGroundHeightWithFloor( pos, &ground ))
				{
					if (me->GetAbsOrigin().z - ground < StepHeight)
					{
						me->StrafeRight();
					}
				}
				break;
			}

			case JUMP:
			{
				if (me->m_isEnemyVisible)
				{
					me->Jump();
				}
				break;
			}
		}
	}
}


//--------------------------------------------------------------------------------------------------------------
/**
 * Perform attack behavior
 */
void AttackState::OnUpdate( CCSBot *me )
{
	Msg("OnUpdate\n");
	// can't be stuck while attacking
	me->ResetStuckMonitor();
	
	CBasePlayer *enemy = me->GetBotEnemy();
	if (enemy == NULL)
	{
		Msg("StopAttacking\n");
		StopAttacking( me );
		me->StopAiming();
		return;
	}

	Vector myOrigin = GetCentroid( me );
	Vector enemyOrigin = GetCentroid( enemy );

	// keep track of whether we have seen our enemy at least once yet
	if (!m_haveSeenEnemy)
		m_haveSeenEnemy = me->IsEnemyVisible();

	//
	// Crowbar fighting
	// We need to pathfind right to the enemy to cut him
	//
	if (me->IsUsingCrowbar())
	{
		// can't crouch and hold with a Crowbar
		m_crouchAndHold = false;
		me->StandUp();

		// if we are using a Crowbar and our prey is looking towards us, run at him
		if (me->IsPlayerFacingMe( enemy ))
		{
			me->ForceRun( 5.0f );
			me->Hurry( 10.0f );
		}

		// slash our victim
		me->FireWeaponAtEnemy();

		// if toe to toe with our enemy, don't dodge, just slash
		const float slashRange = 70.0f;
		if ((enemy->GetAbsOrigin() - me->GetAbsOrigin()).IsLengthGreaterThan( slashRange ))
		{
			const float repathInterval = 0.5f;

			// if our victim has moved, repath
			bool repath = false;
			if (me->HasPath())
			{
				const float repathRange = 100.0f;		// 50
				if ((me->GetPathEndpoint() - enemy->GetAbsOrigin()).IsLengthGreaterThan( repathRange ))
				{
					repath = true;
				}
			}
			else
			{
				repath = true;
			}

			if (repath && m_repathTimer.IsElapsed())
			{
				Vector enemyPos = enemy->GetAbsOrigin() + Vector( 0, 0, HalfHumanHeight );
				me->ComputePath( enemyPos, FASTEST_ROUTE );
				m_repathTimer.Start( repathInterval );
			}

			// move towards victim
			if (me->UpdatePathMovement( NO_SPEED_CHANGE ) != CCSBot::PROGRESSING)
			{
				me->DestroyPath();
			}
		}

		return;
	}

	if (me->IsUsing("weapon_shotgun"))
	{
		// if we have a shotgun equipped and enemy is too far away, switch to pistol
		const float shotgunMaxRange = 1024.0f;
		if ((enemyOrigin - myOrigin).IsLengthGreaterThan( shotgunMaxRange ))
			me->EquipPistol();
	}

	// see if we "notice" that our prey is dead
	if (me->IsAwareOfEnemyDeath())
	{
		Msg("EnemyDead\n");
		StopAttacking( me );
		me->StopAiming();
		return;
	}

	float notSeenEnemyTime = gpGlobals->curtime - me->GetLastSawEnemyTimestamp();

	// if we haven't seen our enemy for a moment, continue on if we dont want to fight, or decide to ambush if we do
	if (!me->IsEnemyVisible())
	{
		// attend to nearby enemy gunfire
		if (notSeenEnemyTime > 0.5f && me->CanHearNearbyEnemyGunfire())
		{
			// give up the attack, since we didn't want it in the first place
			StopAttacking( me );
			me->StopAiming();

			const Vector *pos = me->GetNoisePosition();
			if (pos)
			{
				me->SetLookAt( "Nearby enemy gunfire", *pos, PRIORITY_HIGH, 0.0f );
				me->PrintIfWatched( "Checking nearby threatening enemy gunfire!\n" );
				return;
			}
		}
	}
	else
	{
		// we can see the enemy again - reset our ambush check
		m_didAmbushCheck = false;
	}


	// if we haven't seen our enemy for a long time, chase after them
	float chaseTime = 2.0f + 2.0f * (1.0f - me->GetProfile()->GetAggression());

	if (me->IsCrouching())	// if we are crouching, be a little patient
		chaseTime += 1.0f;

	// if we can't see the enemy, and have either seen him but currently lost sight of him, 
	// or haven't yet seen him, chase after him
	if (!me->IsEnemyVisible() && (notSeenEnemyTime > chaseTime || !m_haveSeenEnemy))
	{
		// move to last known position of enemy
		me->SetTask( CCSBot::MOVE_TO_LAST_KNOWN_ENEMY_POSITION, enemy );
		me->MoveTo( me->GetLastKnownEnemyPosition() );
		return;
	}


	// if we can't see our enemy at the moment, and were shot by
	// a different visible enemy, engage them instead
	const float hurtRecentlyTime = 3.0f;
	if (!me->IsEnemyVisible() &&
		me->GetTimeSinceAttacked() < hurtRecentlyTime &&
		me->GetAttacker() &&
		me->GetAttacker() != me->GetBotEnemy())
	{
		// if we can see them, attack, otherwise panic
		if (me->IsVisible( me->GetAttacker(), CHECK_FOV ))
		{
			me->Attack( me->GetAttacker() );
			me->PrintIfWatched( "Switching targets to retaliate against new attacker!\n" );
		}
		/*
		 * Rethink this
		else
		{
			me->Panic( me->GetAttacker() );
			me->PrintIfWatched( "Panicking from crossfire while attacking!\n" );
		}
		*/

		return;
	}

	me->FireWeaponAtEnemy();
	Dodge( me );
}

//--------------------------------------------------------------------------------------------------------------
/**
 * Finish attack
 */
void AttackState::OnExit( CCSBot *me )
{
	me->PrintIfWatched( "AttackState:OnExit()\n" );

	m_crouchAndHold = false;

	// clear any noises we heard during battle
	me->ForgetNoise();
	me->ResetStuckMonitor();

	// resume our original posture
	me->PopPostureContext();

	me->StopAiming();
}

