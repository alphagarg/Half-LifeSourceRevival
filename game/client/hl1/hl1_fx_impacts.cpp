//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Game-specific impact effect hooks
//
//=============================================================================//
#include "cbase.h"
#include "fx_impact.h"


//-----------------------------------------------------------------------------
// Purpose: Handle weapon impacts
//-----------------------------------------------------------------------------
void ImpactCallback( const CEffectData &data )
{
	trace_t tr;
	Vector vecOrigin, vecStart, vecShotDir;
	int iMaterial, iDamageType, iHitbox;
	short nSurfaceProp;
	C_BaseEntity *pEntity = ParseImpactData( data, &vecOrigin, &vecStart, &vecShotDir, nSurfaceProp, iMaterial, iDamageType, iHitbox );

	if ( !pEntity )
		return;

	// If we hit, perform our custom effects and play the sound
	if ( Impact( vecOrigin, vecStart, iMaterial, iDamageType, iHitbox, pEntity, tr ) )
	{
		// Check for custom effects based on the Decal index
		PerformCustomEffects( vecOrigin, tr, vecShotDir, iMaterial, 1.0 );
	}

	//Msg("Damage type is %i. Impacted material type is %i.\n", iDamageType, iMaterial);

	CLocalPlayerFilter filter;

	//Distance calculation. Unused but kept for third party use. --GARG
	//float hipotenuse = pow((vecOrigin - vecStart).x, 2) + pow((vecOrigin - vecStart).y, 2);
	//float result = sqrt((hipotenuse + pow((vecOrigin - vecStart).z, 2)));
	
	if (iDamageType == 4098) {
		if (iMaterial == 68 || iMaterial == 67 || iMaterial == 77)
			if (random->RandomFloat(0, 12) < 9)
				C_BaseEntity::EmitSound(filter, NULL, "Bullets.Crack", &vecOrigin); //Generic bullet crack. --GARG
	}
	//C_BaseEntity::EmitSound(filter, NULL, "Bullets.HeavyCrack", &vecOrigin); //Generic heavy bullet crack. --GARG
	
	PlayImpactSound( pEntity, tr, vecOrigin, nSurfaceProp );
}

DECLARE_CLIENT_EFFECT( "Impact", ImpactCallback );
